<?php
include_once("../SERVICE/ServicePerduTrouver.php");
include_once("../SERVICE/ServiceEspeceRace.php");
include_once("../SERVICE/ServiceAdoption.php");



if (isset($_POST["Action"])) {
  if ($_POST["Action"] == "AjoutPerduTrouver") {
    $AjoutPerduTrouver = new ServicePerduTrouver();
    $AjoutPerduTrouver->AjoutPerduTrouver($_POST);
  }
}


if (isset($_GET["link"])) {
  if ($_GET["link"] == "SupPerduTrouver") {
    $SupPerduTrouver = new ServicePerduTrouver();
    $SupPerduTrouver->SupPerduTrouver($_GET["id_perdu_trouve"]);
  }
}

?>

<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8" />
  <title>Perdu Trouvé</title>
  <link rel="stylesheet" href="../css/bootstrap.css" />
  <link rel="stylesheet" href="../css/style.css" />

  <link rel="icon" href="../img/patteblanche.png">

</head>

<body>
  <!-- header       -->

  <!-- fin header       -->
  <!-- debut page       -->
  <div class="container-fluid ">


    <div class="row col-lg-12 justify-content-center ml-1 ">
      <div class="col-lg-8 text-center ">
        <h1 class="Titre">Perdu ou Trouver ? </h1>
        <?php if (isset($_SESSION["email"])) { ?>
          <button class="btn Bouton-Admin-1 mt-3" type="submit">
            <a style="color: white;" href="Accueil.php?link=formPerduTrouve&Action=AjoutPerduTrouver">Créer une annonce</a>
          </button>
        <?php } ?>
      </div>
    

    <div class="col-lg-8 aside-adopt  text-center title-filtre ">
     
      <div class="row bloc-aside1 justify-content-around ">
        
          <!-- Type -->
          <div class="form-group row mt-2 justify-content-center ">
            <label for="Race" class="mx-auto col-form-label">Perdu/Trouver</label>
            <select class="custom-select col-10 mb-3 RecherchePerduTrouver" name="type_perdu_trouve" id="Type">
              <option value="">Perdu et Trouvé</option>
              <?php
              $SelectTypePerduTrouver = new ServicePerduTrouver();
              $data = $SelectTypePerduTrouver->SelectTypePerduTrouver();

              for ($i = 0; $i < count($data); $i++) {
                echo '<option>' . $data[$i]["type_perdu_trouve"] . '</option>';
              }

              ?>
            </select>
          </div>
          <!-- Espece -->
          <div class="form-group row mt-2 justify-content-center ">
            <label for="Espece" class="col-sm-3 col-form-label">Espece</label>
            <select class="custom-select col-10 mb-3 RecherchePerduTrouver" name="nom_espece" id="Espece">
              <option value="">Toutes les Espece </option>
              <?php
              $Espece = new ServiceEspeceRace();
              $data = $Espece->SelectEspece();

              for ($i = 0; $i < count($data); $i++) {
                echo '<option>' . $data[$i]["nom_espece"] . '</option>';
              }
              ?>
            </select>
          </div>
          <!-- code postal -->
          <div class="form-group row col-3 mt-2 justify-content-center ">
            <label for="CodePostal" class="col-10 col-form-label">Code Postal</label>
            <input type="text" class=" custom-select col-10 mb-3" name="code_postale_perdu_trouve" id="CodePostal">
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>




  <!-- bloc annonces adoption -->
  <div class="row justify-content-center ">
    <div class="col-lg-8 mb-3" id="AffichagePerduTrouver">
      <?php

      include_once('PerduTrouverAffichage.php');
      ?>

    </div>

    <!-- bloc recherche en aside en lg -->




  </div>


  </div>
  </div>
  <!-- debut page       -->
  <!-- footer       -->

  <!-- fin footer       -->
</body>
<script>
  window.onload = function() {

    $("#paginPerduTrouve li").remove();
    loadPaginationPerduTrouve();

  }
</script>

</html>