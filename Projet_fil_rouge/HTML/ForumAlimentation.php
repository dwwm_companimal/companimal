<!-- header       -->

<!-- fin header       -->

<!-- Titre ALIMENTATION -->

<div class="container text-center">
  <div class="row mt-2 ">
    <div class="col-4"></div>
    <div class="col-4 Titre">
      <h1>Alimentation</h1>
    </div>
    <div class="col-4 "></div>
  </div>
</div>

<!-- Créé TOPIC -->
<div class="container text-center backgroundforum">

  <div class="row mt-2 pt-3">
    <div class="col-3"></div>
    <div class="col-6">
      <?php if (isset($_SESSION["email"])) { ?>
        <a type="submit" class="btn Bouton-Admin-1 text-decoration-none text-white" href="Accueil.php?action=formForumAlim&categorie=2">Créer un Topic</a></button>
      <?php } ?>
    </div>
    <div class="col-3 "></div>
  </div>

  <!-- Barre de Recherche -->
  <div class="row mt-3">
    <div class="offset-2"></div>

    <!-- Bouton recherche -->
    <div class="col-12 aff-article4  pt-2 pb-2 mb-5">

      <div class="row justify-content-center">
        <label class=" mt-3 mr-3" for="site-search">Recherche sur le forum:</label>
        <input style="margin-bottom: 10px; text-align: center;" class="col-5 mt-2 form-control" type="text" placeholder="Que cherche-vous ?" aria-label="Search" name="nom_topic" id="NomTopicAlimentation">
      </div>
    </div>
    <div class="offset-1"></div>
  </div>


  <!-- Nom Topic et Date -->
  <?php

  include_once('..\SERVICE\ServiceForum.php');

  /* Ajout d'un Topic */

  if (isset($_POST["ValidationTopic"]) && $_GET['link'] == 'ajout') {
    $serviceAjoutTopic = new ServiceForum();
    $serviceAjoutTopic->ajoutTopic($_POST);
  }
  /* *********************** */

  /* Modification d'un Topic */

  if (isset($_POST["ModificationTopic"]) && $_GET['link'] == 'modif') {
    $serviceAjoutTopic = new ServiceForum();
    $serviceAjoutTopic->modificationTopic($_POST);
  }
  /* *********************** */

  /* Suppression d'un Topic */

  if (isset($_GET['link']) && $_GET['link'] == 'del') {
    $delTopic = new ServiceForum();
    $delTopic->deleteTopic($_GET['id_topic']);
  }
  /* *********************** */

  /* Affichage avec pagination */
  ?>

  <div class="row text-center backgroundforum" id="AffichageForumAlimentation">
    <?php include_once("ForumAlimentationAffichage.php"); ?>
  </div>


</div>

<script>
        window.onload=function() {

        $("#paginForumArticle li").remove();
        loadPaginationForumArticle();

        }
    </script>
<!-- footer       -->

<!-- fin footer       -->