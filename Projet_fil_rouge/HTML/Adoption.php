<?php
include_once("../SERVICE/ServiceAdoption.php");
include_once("../SERVICE/ServiceEspeceRace.php");
include_once("../SERVICE/ServiceRefuge.php");

?>


<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8" />
  <title>Adoption</title>
  <link rel="stylesheet" href="../css/bootstrap.css" />
  <link rel="stylesheet" href="../css/style.css" />

  <link rel="icon" href="../img/patteblanche.png">

  <script>
    $("#paginAdoption li").remove();
    loadPaginationAdoption();
  </script>

</head>

<body>
  <!-- header       -->

  <!-- fin header       -->
  <!-- debut page       -->
  <div class="container-fluid ">
    <div class="row col-lg-12 justify-content-center ml-1 ">
      <div class="col-lg-12 text-center ">
        <h1 class="Titre">Adoptez !</h1>
        <?php if (isset($_SESSION['role']) && $_SESSION['role'] != "Membre") { ?>
          <a class="btn Bouton-Admin-1" style="color: white;" href="Accueil.php?link=formAdoption&Action=AjoutAdoption">Créer une annonce</a>
        <?php } ?>
      </div>

      <div class="col-lg-8 aside-adopt  text-center title-filtre  ">
        
        <div class="row bloc-aside1 justify-content-around ">
          
            <!-- Espece -->
            <div class="form-group row  m-2 justify-content-center ">
              <label for="Espece" class="col-sm-3 col-form-label">Espece</label>
              <select class="custom-select col-10 mb-3 Recherche" name="nom_espece" id="Espece">
                <option value="">Toutes les Espece </option>
                <?php
                $Espece = new ServiceEspeceRace();
                $data = $Espece->SelectEspece();

                for ($i = 0; $i < count($data); $i++) {
                  echo '<option>' . $data[$i]["nom_espece"] . '</option>';
                }
                ?>
              </select>
            </div>
            <!-- Race 
                <div class="form-group row  justify-content-center ">
                  <label for="Race" class="col-sm-3 col-form-label">Race</label>
                    <select class="custom-select col-10 mb-3 Recherche" name="Nom_race" id="Race">
                      <option value="">Toute les Races</option>
                      <?php
                      /*$Race = new ServiceEspeceRace();
                        $data = $Race-> SelectRace();
                    
                        for($i=0; $i < count($data); $i++){
                            echo '<option>'.$data[$i]["Nom_race"].'</option>';
                        }*/

                      ?>
                    </select>
                </div>
                      -->
            <!-- Sexe -->
            <div class="form-group row m-2  justify-content-center ">
              <label for="Race" class="col-sm-3 col-form-label">Sexe</label>
              <select class="custom-select col-10 mb-3 Recherche" name="sexe_animal" id="Sexe">
                <option value="">Toute les sexe</option>
                <?php
                $SelectSexeAdoption = new ServiceAdoption();
                $data = $SelectSexeAdoption->SelectSexeAdoption();

                for ($i = 0; $i < count($data); $i++) {
                  echo '<option>' . $data[$i]["sexe_animal"] . '</option>';
                }

                ?>
              </select>
            </div>

            <!-- Age -->
            <div class="row m-2 justify-content-center">
              <label class="col-6 text-center" for="Age">Age</label>
              <input type="range" class="custom-range col-10 age-color" value="0" min="0" max="5" step="1" id="Age" nom="naissance_animal" oninput="result4.value=parseInt(Age.value)">
              <output class=" col-6 text-center" name="result4">"" Ans</output>
            </div>

            <!-- Espece -->
            <div class="row m-2 justify-content-center">
              <label for="Refuge " class=" col-6 col-form-label text-center">refuge</label>
              <select class="custom-select col-10 mb-3 Recherche" name="id_refuge" id="Refuge">
                <option value="">Tous les refuges</option>
                <?php
                $Refuge = new ServiceRefuge();
                $data = $Refuge->SelectAllRefuge();

                for ($i = 0; $i < count($data); $i++) {
                  echo '<option value="' . $data[$i]["id_refuge"] . '">' . $data[$i]["nom_refuge"] . '</option>';
                }
                ?>
              </select>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- bloc annonces adoption -->
  <div class="row justify-content-center ">
    <div class="col-lg-8 mb-3" id="Affichage">

      <?php
      /* Ajout Adoption */

      if (isset($_POST["Action"])) {
        if ($_POST["Action"] == "AjoutAdoption") {
          $AjoutAdoption = new ServiceAdoption();
          $AjoutAdoption->AjoutAdoption($_POST);
        }

        /*if($_POST["Action"] == "ModifierAdoption"){
              $ModifAdoption = new ServiceAdoption();
              $ModifAdoption-> ModifAdoption($_POST);
              
            }*/
      }


      if (isset($_GET["link"])) {
        if ($_GET["link"] == "SupAdoption") {
          $SupAdoption = new ServiceAdoption();
          $SupAdoption->SupAdoption($_GET["id_adoption"]);
        }
      }

      /* Affichage Adoption */
      include('AdoptionAffichage.php');
      ?>

    </div>
    <!-- bloc recherche en aside en lg -->


  </div>
  <!-- debut page       -->
  <!-- footer       -->

  <!-- fin footer       -->
</body>
<script>
  window.onload = function() {

    $("#paginAdoption li").remove();
    loadPaginationAdoption();

  }
</script>

</html>