<?php
include_once ('../SERVICE/ServicePetSitter.php');
include_once ('../DAO/BDDConnexionDAO.php');

?>


<!DOCTYPE html>
	<html>
		<head>
			<meta charset=``utf-8`` />
            <title>Fiche Pet'Sitter</title>
            <link rel="stylesheet" href="../css/bootstrap.css"/>
            <link rel="stylesheet" href="../css/style.css"/>
           
            <link rel="icon" href="../img/patteblanche.png">
            
        </head>
<body>


<?php 

/* Modification des employes */
if(isset($_POST["modif"])){
  $serviceEmp = new ServicePetSitter;
  $serviceEmp -> modify($_POST); 
}


 $db = mysqli_init();
 mysqli_real_connect($db, 'localhost',"root","","companimal");
 $id = $_GET['idPet'];
 $ligne = mysqli_query($db,"SELECT * FROM pet_sitter AS A inner join utilisateur AS B on A.id_utilisateur=B.id_utilisateur WHERE id_pet_sitter="."'$id'");
 $data = mysqli_fetch_all($ligne,MYSQLI_BOTH);


 
 
 ?>   



  <!-- Debut Body Fiche Pet Sitter-->
    
        <div class="container-fluid mb-5">
        
         
          <!-- Background Blanc-->
            <div class="row mb-5">
              <div class="col-8 offset-2 ">
                <div class="Background-Color-Fiche-Blanc mt-5 ">
                <div class="row ">
            <div class="col-lg-4 offset-lg-4 ">
              <h1 class="Titre"><?php echo  $data[0][1]; ?></h1>
              </div>
            </div>
                  <div class="Background-Color-Fiche-Texte">
                    <div class="row">
                      <div class="col-lg-5 m-2 ml-5">
                      <p class="mt-5" style="text-align: center"><a class="text-dark "><strong>Information Complémentaire :</strong></a></p>
                                <p class=""><?php
                                        echo $data[0][6];
                                        
                                    ?></p>   
                      </div>
                      
                     <div class="col-lg-5 mt-5 ml-5">
                     <div class="ml-5">
                              <div class="black ">
                                <p><a class="text-dark"><strong>Nom / Prénom :</strong></a>  <?php 
                                                echo "  ";
                                                echo $data[0][11];
                                                echo "  ";                     
                                                echo $data[0][12];                                        
                                            ?> </p> 
                                <p><a class="text-dark"><strong>Code Postal :</strong></a><?php
                                                echo "  ";
                                                echo $data[0][2];                                   
                                              ?>  </p>
                                <p><a class="text-dark"><strong>Type de Garde :</strong></a><?php 
                                                echo "  ";
                                                echo $data[0][3];                                   
                                          ?> </p>
                                <p><a class="text-dark"><strong>Type de Domicile :</strong></a><?php 
                                                echo "  ";
                                                echo $data[0][4];                                   
                                            ?> </p>
                                <p><a class="text-dark"><strong>Type d'Animal Gardé :</strong></a><?php
                                                echo "  ";
                                                echo $data[0][9];                                   
                                            ?> </p>
                                
                        </div>
                        <div class="Background-Color-Fiche-Image mb-2 ">
                        <img width="580px" src="data:image/jpg;base64,<?php echo base64_encode($data[0][7])?>"></img>
                        </div> 
                        <?php if((isset($_SESSION['role']) && $_SESSION['role']!="Membre")||(isset($_SESSION['iduser'])&& $data[0]['id_utilisateur']==$_SESSION['iduser'])){?>
                        <div class="row mt-3 justify-content-center">
                          <form action="accueil.php?action=modifpetsiter" method="post">
                              <input Class="btn mt-2 mr-2 Bouton-Admin-1" type="submit" value="modifier">
                              <input type="hidden" name="modifier" value="<?php echo $data[0][0]?> ">                    
                          </form>
                          <a value="del" class="btn btn Bouton-Admin-1"  href="Accueil.php?action=petsitter&id_pet_sitter=<?php echo $data[0]['id_pet_sitter'];?>&link=del" style="color:white; text-decoration:none;">Supprimer</a>
                        </div>
                        <?php }else{ ?>
                          <div class="row mt-3 justify-content-center">
                          <?php } ?>
                          
                        </div>
                    </div>
                  </div>
                  
                </div>
                
                <div class=" row justify-content-center">
                  <?php
                      echo '<a href="Accueil.php?link=contact&action=contactPetSitter&email_petsitter='.$data[0][17].'"><button type="button" class="btn btn-lg Bouton-Admin-2">Contactez-Moi: '.$data[0][17].'</button></a>'; 
                      ?>
                    <?php if($data[0][5]=="oui"){?>
                      <a class="btn-lg text-light Bouton-Admin-2">Tel : <?php 
                                                  echo "  0";
                                                  echo $data[0][16];                                   
                                                }  ?>  </a>

                          <?php if(isset($_GET['ret'])){
                            echo '<a href="Accueil.php?action=mesAnnonces"><button type="button" class="btn btn-lg Bouton-Admin-2">Retour</button></a>';
                          }else{                         
                            echo' <a href="Accueil.php?action=petsitter"><button type="button" class="btn btn-lg Bouton-Admin-2">Retour</button></a> ';
                          }?>                         
                  </div>
                </div>
              </div>
        </div>
      </div>
  <!-- Fin Body Fiche Pet'Sitter-->    
        </div>
</body>
</html>
