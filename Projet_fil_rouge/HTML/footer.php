<!-- footer     -->
<footer class="font-small foot " >

        <div class="container-fluid  text-center text-md-left  ">
          <div class="row justify-content-center ">
            <!-- barre haut footer     -->
            <div class="col-lg-4 col-sm-8 bdtop">
              <br>
            </div>
          </div>
          
          <div class="row text-center mt-2">
            <!-- partie gauche footer  -->
           
            <div class="col-md-12 col-lg-3 mt-5">              
              <h5 style="color:#d5af64;" class=" font-weight-bold text-uppercase mb-2">Newsletter</h5>
              <a class="text-left" for="idpersonne">S'abonner à la Newsletter</a>
              <form method="POST" action="Accueil.php" >
                <div class="row" >
                  <div class="col-md-2 col-lg-1"></div>
                    <input style="text-align:center;" class="col-md-6 col-lg-7 col-sm-8 mt-1 offset-1 form-control" id="EmailNws" size="20" maxlength="50" type="email" 
                    placeholder="Entrez votre email" name="EmailNws"/>
                    <input type="hidden" name="Action" value="NwsEmail">
                    <!--<a style="color:#EAE0D5;" class="col-md-3 col-lg-3 col-sm-7 mt-2" type="submit"><i class="fas fa-file-import fa-2x"></i></a>-->
                    <input class="btn Bouton-Admin-1" type="submit" value="Envoyer">
                 <div class="col-md-2 col-lg-1"></div>
                </div>
              </form>
              
               <a class="nouscont" href="Accueil.php?link=contact"><i class="pt-5 pb-2 fas fa-file-contract fa-2x"></i></a>
               <h5>Nous contacter </h5>
            </div>
           
              <!-- partie centrale footer -->
            
            <div class="col-md-12 col-lg-2  mx-auto my-md-4 my-0 mt- mb-1 ">
              <br>
              <br>
              <ul class="list-unstyled">
                <li>
                    <h4><u>Accueil</u></h4>
                    <a href="Accueil.php"><i class="pt-3 fas fa-home fa-2x"></i></a>
                </li>
                
            </div> 
            <div class="col-md-12 col-lg-1  mx-auto my-md-4 my-0 mt- mb-1 "> 
              <br>
              <br>
              <ul class="list-unstyled"> 
                <li>    
                    <h4 class=""><u>Services</u></h4>
                </li> 
                <li>    
                    <a href="Accueil.php?action=adoption">Adopter</a>
                </li>
                <li>  
                    <a href="Accueil.php?action=refuge">Refuges</a>
                </li>
                <li>  
                    <a href="Accueil.php?action=perduTrouve">Perdus/Trouvés</a>
                </li>
                <li>  
                    <a href="Accueil.php?action=petsitter">Pet'Sitter</a>
                </li>
              </ul>  
            </div>
            <div class="col-md-12 col-lg-2  mx-auto my-md-4 my-0 mt- mb-1 "> 
              <ul class="list-unstyled"> 
              <br>
              <br>  
                <li>  
                <h4 class=""><u>Forum</u></h4>
                </li>
                <li>  
                    <a href="Accueil.php?action=forumSante">Santé</a>
                </li>
                <li>  
                    <a href="Accueil.php?action=forumAlimentation">Alimentation</a>
                </li>
                <li>  
                    <a href="Accueil.php?action=forumDivertissement">Divertissement</a>
                </li>
                <li>  
                    <a href="Accueil.php?action=forumAutre">Autre</a>
                </li>
              </ul>
            </div>
            

            
            <!-- partie droite footer -->
            
            <div class="col-md-3 col-lg-3 mx-auto my-md-4 my-0 mt-4 mb-1">
              <br>
              <!--<i class="fas fa-envelope mr-3">-->
                <a href="https://www.lpa-nf.fr/faire-un-don/" target="_blank">
                  <img src="../img/LogoV3png.png" alt="" class="img-fluid w-50 ml-4">
                </a>
              </i>
              <br>
              <a href="https://www.lpa-nf.fr/faire-un-don/" target="_blank" class="btn mt-5 ml-3 " type="submit" >Je fais un DON</a>
            </div>
          </div>  
            
            <!-- barre bas footer -->
            <div class="row justify-content-center ">
              <div class="col-lg-6 col-sm-8 bdtop">
                
              </div>
            </div>
        
        <div class="col-12 footer-copyright text-center py-2"><a>CGU © 2020 Copyright: 
        <a style="color:#EAE0D5;font-size:20px;" href="Accueil.php"><u class=> companimal.fr</u></a>
        </div>
        

      </footer>