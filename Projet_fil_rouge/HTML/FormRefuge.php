<?php 

include_once('../SERVICE/ServiceRefuge.php');

if(isset($_GET["Action"]) && $_GET["Action"] == "ModifierRefuge"){
  
  $SelectAllRefuge = new ServiceRefuge();
  $data = $SelectAllRefuge->SelectRefugeId($_GET["id_refuge"]);
}
?>



<!DOCTYPE html>
<html>
<head>

            <title>Formulaire Refuge</title>
            <link rel="stylesheet" href="../css/bootstrap.css"/>
            <link rel="stylesheet" href="../css/style.css"/>
           
            <link rel="icon" href="../img/patteblanche.png">
           
</head>
            
  <body>
    <!-- Body Form Refuge-->
    <div class="Background-Form">
        <div class="container-fluid">
          <!-- Titre Refuge-->
            <div>
              <div class="row">
                <div class="mx-auto ">
                <h1 class="Titre"><?php
                        if(isset($_GET["Action"]) && $_GET["Action"] == "ModifierRefuge"){ 
                            echo "Modification du Refuge";
                            
                        }else{
                            echo "Ajout d'un Refuge";
                        } 
                        ?></h1>
                </div>
              </div>
            </div>

            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                  <!-- Formulaire Refuge-->
                    <form method="POST" enctype="multipart/form-data" id="formRefuge" action="<?php
                        if(isset($_GET["Action"]) && $_GET["Action"] == "ModifierRefuge"){ 
                            echo "Accueil.php?link=ModifierRefuge&id_refuge=".$data[0]['id_refuge']."&action=ficheRefuge";
                            
                        }else{
                            echo "Accueil.php?action=refuge";
                        } 
                        ?>">
                      <!-- Nom refuge-->
                        <div class="form-group row">
                          <div class="col-lg-3"></div>
                            <label for="NomRefuge" class="col-lg-2 col-form-label" style="font-weight:bold;">Nom du Refuge :</label>
                            <div class="col-lg-4 col-6">
                              <input type="text" class="form-control" id="NomRefuge" name="NomRefuge" required autofocus value="<?php 
                                if(isset($_GET["Action"]) && $_GET["Action"] == "ModifierRefuge"){
                                    echo $data[0]["nom_refuge"];}?>">
                            </div>
                        </div>
                        <!--Adresse Refuge-->
                        <div class="form-group row">
                          <div class="col-lg-3"></div>
                          <label for="AdresseRefuge" class="col-lg-2 col-form-label" style="font-weight:bold;">Adresse :</label>
                          <div class="col-lg-4 col-6">
                            <input type="text" class="form-control" id="AdresseRefuge" name="AdresseRefuge" required value="<?php 
                                if(isset($_GET["Action"]) && $_GET["Action"] == "ModifierRefuge"){
                                    echo $data[0]["adresse_refuge"];}?>">
                          </div>
                      </div>
                      <!--Ville-->
                      <div class="form-group row">
                        <div class="col-lg-3"></div>
                        <label for="VilleRefuge" class="col-lg-2 col-form-label" style="font-weight:bold;">Ville :</label>
                        <div class="col-lg-4 col-6">
                          <input type="text" class="form-control" id="VilleRefuge" name="VilleRefuge" required value="<?php 
                                if(isset($_GET["Action"]) && $_GET["Action"] == "ModifierRefuge"){
                                    echo $data[0]["ville_refuge"];}?>">
                        </div>
                    </div>
                    <!-- code Postal -->
                    <div class="form-group row">
                      <div class="col-lg-3"></div>
                      <label for="CodePostalRefuge" class="col-lg-2 col-form-label" style="font-weight:bold;">Code Postal :</label>
                      <div class="col-lg-3 col-6">
                        <input type="number" class="form-control" id="CodePostalRefuge" name="CodePostalRefuge" required value="<?php 
                                if(isset($_GET["Action"]) && $_GET["Action"] == "ModifierRefuge"){
                                    echo $data[0]["code_postal_refuge"];}?>"">
                      </div>
                  </div>
                  <!-- AdresseMailRefuge-->
                   <div class="form-group row">
                    <div class="col-lg-3"></div>
                      <label for="AdresseMailRefuge" class="col-lg-2 col-form-label" style="font-weight:bold;">Adresse Mail :</label>
                      <div class="col-lg-4 col-6">
                        <input type="text" class="form-control" id="AdresseMailRefuge" name="AdresseMailRefuge" required value="<?php 
                                if(isset($_GET["Action"]) && $_GET["Action"] == "ModifierRefuge"){
                                    echo $data[0]["email_refuge"];}?>">
                      </div>
                   </div>
                   <!--Tel refuge-->
                    <div class="form-group row">
                      <div class="col-lg-3"></div>
                      <label for="TelRefuge" class="col-lg-2 col-form-label" style="font-weight:bold;">Numéro de Téléphone :</label>
                      <div class="col-lg-3 col-6 tel">
                        <input type="number" class="form-control" id="TelRefuge" name="TelRefuge" required value="<?php 
                                if(isset($_GET["Action"]) && $_GET["Action"] == "ModifierRefuge"){
                                    echo $data[0]["telephone_refuge"];}?>">
                      </div>
                    </div>

                  <!-- Dowload Photo-->
                  <?php 
                              if(isset($_GET["Action"]) && $_GET["Action"] == "ModifierRefuge"){
                                
                              }else{
                              echo'  <div class="form-group row">';
                              echo'       <div class="col-lg-3"></div>';
                              echo'       <label class="col-lg-3 col-form-label"><strong>Télécharger une Photo :</strong></label>';
                              echo'       <div class="col-lg-4 col-6">';
                              echo'       <input type="file" name="userImage"/>';
                              echo'      </div>';
                              echo'    </div>'; 
                              }
                              ?>

                  <!--Description refuge-->
                    <div class="form-group row">
                            <div class="col-lg-3"></div>
                            <label for="DescriptionRefuge" class="col-lg-2 col-form-label" style="font-weight:bold;">Description :</label>
                            <div class="col-lg-5 col-10">
                                <textarea class="form-control" id="DescriptionRefuge" name="DescriptionRefuge" rows="6" required><?php 
                                if(isset($_GET["Action"]) && $_GET["Action"] == "ModifierRefuge"){
                                    echo $data[0]["description_refuge"];}?></textarea>
                            </div>
                        </div>

                        <input type="hidden" name="Action" value="<?php if(isset($_GET["Action"]) && $_GET["Action"] == "ModifierRefuge"){echo 'ModifierRefuge';}else{ echo 'AjoutRefuge';}?>">
                        <?php
                            if(isset($_GET["Action"]) && $_GET["Action"] == "ModifierRefuge"){

                                echo '<input type="hidden" name="id_refuge" value="'.$data[0]["id_refuge"].'">';
                            }
                        ?>
                    <!-- Bouton Validation-->
                    <div class="row">
                      <div class="col-lg-5"></div>
                      <div class="col-lg-4">
                        <button type="submit" class="btn Bouton-Admin-1">Valider</button>
                        <a href="Accueil.php?action=refuge">
                          <button type="button" class="btn Bouton-Admin-1">Retour</button>
                        </a>
                        </div>
                    </div>   
                </form>
                <!-- Fin Formulaire-->
            </div>
        </div>
    </div>
</div>
<!-- Fin Body Formulaire Refuge-->
</body>
</html>