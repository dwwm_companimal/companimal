<?php

  include_once('../SERVICE/ServiceRefuge.php');

  if(isset($_GET["link"])){
    if($_GET["link"] == "ModifierRefuge"){
      $ModifRefuge = new ServiceRefuge();
      $ModifRefuge-> ModifRefuge($_POST);
    }
  }

  $SelectRefugeId = new ServiceRefuge();
  $data = $SelectRefugeId->SelectRefugeId($_GET["id_refuge"]);

?>

<!DOCTYPE html>
	<html>
		<head>
			<meta charset=``utf-8`` />
            <title>Fiche Refuge</title>
          
            <link rel="stylesheet" href="../css/bootstrap.css"/>
            <link rel="stylesheet" href="../css/style.css"/>
           
            <link rel="icon" href="../img/patteblanche.png">
            
        </head>
<body>
  <!-- Debut Fiche Perdu Trouver-->
    <div class="Fiche">
        <div class="container-fluid">
          
            <div class="col-lg-8 offset-lg-2 ">
              
            
                    <!-- Background Blanc-->
            <div class="row">
              <div class="">
                <div class="Background-Color-Fiche-Blanc">
                <h1 class="Titre"><?php echo $data[0]["nom_refuge"]; ?></h1>
          <!-- Image Fiche Animal-->      
                  
          <!-- Texte Fiche Animal-->
                  <div class="Background-Color-Fiche-Texte">
                    <div class="row">
                   
                      <div class="col-lg-6 offset-lg-1 col-10 offset-1 ">
                        
                      </div>
                    
                    <div class="col-lg-6 p-5 Commentaire-Box mt-5 ">
                    <p style="text-align: center"><strong>Information sur le Refuge :</strong></p>
                        <p><?php echo $data[0]["description_refuge"]; ?></p>
                      </div>
                   
                   
                    <div class="col-lg-6 p-5">
                    <div class="col-lg-12  ">
                    <p><strong>Adresse du Refuge :</strong> <?php echo $data[0]["adresse_refuge"]; ?></p>
                        <p><strong>Ville du refuge :</strong> <?php echo $data[0]["ville_refuge"]; ?></p>
                        <p><strong>Code Postale :</strong> <?php echo $data[0]["code_postal_refuge"]; ?></p>
                      </div>
                      
                      <div class="Background-Color-Fiche-Image p-3">
                      <img width="580px" src="data:image/jpg;base64,<?php echo base64_encode($data[0]["photo_refuge"])?>"></img>
                      </div>
                      <!-- Bouton Admin Creation/Suppresion-->
                  <div class="row justify-content-center">
                  <?php if(isset($_SESSION['role']) && $_SESSION['role']!="Membre"){?>
                    <a href="Accueil.php?Action=ModifierRefuge&link=formRefuge&id_refuge=<?php echo $data[0]['id_refuge']; ?>"><button type="button" class="btn  Bouton-Admin-1">Modifier</button></a>
                    <a href="Accueil.php?action=refuge&link=SupRefuge&id_refuge=<?php echo $data[0]['id_refuge'] ?>"><button type="button" class="btn  Bouton-Admin-1">Supprimer</button></a>
                  <?php }?>
                  </div>
                    </div>
                  </div>
        
        <!-- Bouton ContactEz-Nous/information du Refuge-->
                 </div>
              
              <div class=" row justify-content-center">
        <?php echo '<a href="Accueil.php?link=contact&action=contactRefuge&email_refuge='.$data[0]["email_refuge"].'"><button type="button" class="btn btn-lg Bouton-Admin-2">Contactez-Nous :'.$data[0]["email_refuge"].'</button></a>'; ?>
          <button type="button" class="btn btn-lg Bouton-Admin-2">Numéro de téléphone : <?php echo $data[0]["telephone_refuge"]; ?></button>
          <a href="Accueil.php?action=refuge"><button type="button" class="btn btn-lg Bouton-Admin-2">Retour</button></a>
        </div>
        </div> 
            </div>
            
            </div>
         </div>
        </div>
      </div>
<!-- Fin Body Fiche Perdu Trouver-->   

</body>
</html>
