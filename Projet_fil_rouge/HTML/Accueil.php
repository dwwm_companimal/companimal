<!DOCTYPE html>
	<html>
		<head>
			<meta charset=``utf-8`` />
            <title>Accueil</title>
            <link rel="stylesheet" href="../css/bootstrap.css"/>
            <link rel="stylesheet" href="../css/style.css"/>
            <script src="https://kit.fontawesome.com/5f3d413dd1.js" crossorigin="anonymous"></script>
            <link href="https://fonts.googleapis.com/css?family=Indie+Flower|Acme|Oleo+Script:wght@700&display=swap" rel="stylesheet">
            <link rel="icon" href="../img/patteblanche.png">
            
    </head>
    <body>

    <?php
      include_once('../DAO/UtilisateurDAO.php');
      include_once('../SERVICE/ServiceUtilisateur.php');
      include_once('../SERVICE/ServiceNewsLetter.php');
      session_start();
      
      $userdao= new UtilisateurDAO;
        $ServiceUtilisateur = new ServiceUtilisateur;
    
      
    
      if(isset($_GET["action"]) && ($_GET["action"]=="deconnection")){
        session_destroy();
            header("location:accueil.php"); 

      }

      /* Ajout Email NewsLetter */
      if(isset($_POST["Action"]) && ($_POST["Action"]=="NwsEmail")){
        $NewsLetter = new ServiceNewsLetter();
        $data = $NewsLetter->SelectWhereAllMailNws($_POST);

        if(count($data)>0){
        }else{
        $NewsLetter = new ServiceNewsLetter();
        $NewsLetter->AjoutMailNws($_POST);
        }
      }

      /* Supprimer NewsLetter */
      if(isset($_GET["Desinscrire"])){

        $EmailNws = $_GET["Desinscrire"];

        $NewsLetter = new ServiceNewsLetter();
        $NewsLetter->DeletNws($EmailNws);
      }


      if(isset($_POST["edit"])){
        $ServiceUtilisateur -> modifUser($_POST);
      }

      
     
include("header.php");
?>
        
        <!-- fin header       -->
        <?php if(isset($_GET["action"])&& $_GET['action']  =="petsitter"){
          include('PetSitter.php');
        }elseif(isset($_GET["action"])&& $_GET['action']  =="gestionUsers"){
          include('gestion_utilisateurs.php');
        }elseif(isset($_GET["action"])&& $_GET['action']  =="modifpetsiter"){
          include('FormPetSitterModification.php');
        }elseif(isset($_GET["action"])&& $_GET['action']  =="ajoutPetsiter"){
          include('FormPetSitter.php');
        }elseif(isset($_GET["action"])&& $_GET['action']  =="monCompte"){
          include('FormInscription.php');
        }elseif(isset($_GET["action"])&& $_GET['action']  =="ajouter"){
          include('FormInscription.php');
        }elseif(isset($_GET["action"])&& $_GET['action']  =="fichePetsiter"){
          include('FichePetSitter.php');
        }elseif(isset($_GET["action"])&& $_GET['action']  =="fichearticle"){
          include('FicheArticle.php');
        }elseif(isset($_GET["action"])&& $_GET['action']  =="forumAlimentation"){
          include('ForumAlimentation.php');
        }elseif(isset($_GET["action"])&& $_GET['action']  =="forumDivertissement"){
          include('ForumDivertissement.php');
        }elseif(isset($_GET["action"])&& $_GET['action']  =="forumSante"){
          include('ForumSante.php');
        }elseif(isset($_GET["action"])&& $_GET['action']  =="forumAutre"){
          include('ForumAutre.php');
        }elseif(isset($_GET["action"])&& $_GET['action']  =="formForumAlim"){
          include('FormForumAlimentation.php');
        }elseif(isset($_GET["action"])&& $_GET['action']  =="formForumAlimModif"){
          include('FormForumAlimentationModification.php');
        }elseif(isset($_GET["action"])&& $_GET['action']  =="forumArticle"){
          include('ForumArticle.php');
        }elseif(isset($_GET["action"])&& $_GET['action']  =="formForumComm"){
          include('FormForumCommentaire.php');
        }elseif(isset($_GET["action"])&& $_GET['action']  =="refuge"){
          include('Refuge.php');
        }elseif(isset($_GET["action"])&& $_GET['action']  =="ficheRefuge"){
          include('FicheRefuge.php');
        }elseif(isset($_GET["link"])&& $_GET['link']  =="formRefuge"){
          include('FormRefuge.php');
        }elseif(isset($_GET["action"])&& $_GET['action']  =="adoption"){
          include('Adoption.php');
        }elseif(isset($_GET["link"])&& $_GET['link']  =="formAdoption"){
          include('FormAdoption.php');
        }elseif(isset($_GET["action"])&& $_GET['action']  =="ficheAdoption"){
          include('FicheAdoption.php');
        }elseif(isset($_GET["action"])&& $_GET['action']  =="formArticle"){
          include('FormArticle.php');
        }elseif(isset($_GET["action"])&& $_GET['action']  =="mesAnnonces"){
          include('MesAnnonces.php');
        }elseif(isset($_GET["action"])&& $_GET['action']  =="perduTrouve"){
          include('PerduTrouve.php');
        }elseif(isset($_GET["link"])&& $_GET['link']  =="formPerduTrouve"){
          include('FormPerduTrouver.php');
        }elseif(isset($_GET["Action"])&& $_GET['Action']  =="fichePerduTrouve"){
          include('FichePerduTrouver.php');
        }elseif(isset($_GET["link"])&& $_GET['link']  =="contact"){
          include('Contact.php');
        }elseif(isset($_GET["action"])&& $_GET['action']  =="newsLetter"){
          include('FormNewsLetter.php');
        }else{
          include('Articles.php');
        } 
        include('footer.php');
        ?>

     

           

		</body>
  <script src="../JS\jquery-3.4.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <script src="../JS/JsAccueil.js"></script>
  <script src="../JS/JsAdoption.js"></script>
  <script src="../JS/JsForum.js"></script>
  <script src="../JS/JsPerduTrouve.js"></script>
  <script src="../JS/JsPetSitter.js"></script>
  <script src="../JS/JsRefuge.js"></script>
  <script src="../JS/JsFormContact.js"></script>
  <script src="../JS/JsUtilisateur.js"></script>
</html>
