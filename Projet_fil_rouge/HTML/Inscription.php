<?php
include_once('../SERVICE/ServiceUtilisateur.php');

$ServiceUtilisateur = new ServiceUtilisateur;
if (isset($_POST["inscrire"]) &&  isset($_POST["email"]) && isset($_POST["motDePasse"])) {

    $answer = $ServiceUtilisateur->ajoutUser($_POST);
    if ($answer == "false") {
        $jse = json_encode(array("error" => array(
            "code" => 891,
            "message" => "Cet email est déjà enregistré"
        )), JSON_UNESCAPED_UNICODE);
        echo $jse; 
    } elseif ($answer == "true") {
        $jse = json_encode("success");
        echo $jse; 
    }
}
