<!-- Titre Nom du Topic -->
<?php

include_once('..\SERVICE\ServiceCommentaire.php');
include_once('..\SERVICE\ServiceForum.php');

$db = mysqli_init();
mysqli_real_connect($db, 'localhost', 'root', '', 'companimal');
$id_topic = $_GET['id_topic'];

/* Ajout d'un Commentaire */

if (isset($_POST["ValidationCommentaire"]) && $_GET['link'] == 'ajout') {
  $serviceAjoutTopic = new ServiceCommentaire;
  $serviceAjoutTopic->ajoutCommentaire($_POST);
}

/* ***************** */

/* Suppression d'un Commentaire */

if (isset($_GET['link']) && $_GET['link'] == 'delcommentaire') {
  $SupCommentaire = new ServiceCommentaire();
  $SupCommentaire->SupCommentaire($_GET['id_commentaire']);
}
/* ***************** */

/* Suppression d'un Topic */

if (isset($_GET['link']) && $_GET['link'] == 'del') {
  $delTopic = new ServiceForum();
  $delTopic->deleteTopic($_GET['id_topic']);
}
/* ***************** */

/* Recherche du Topic par ID pour affichage */

$ligne = mysqli_query($db, "SELECT *, DATE_FORMAT(date_topic, '%d/%m/%Y à %H\h%i') as date_t FROM topic WHERE id_topic=" . "'$id_topic'");
$topic = mysqli_fetch_all($ligne, MYSQLI_BOTH);

$idPosteur =  mysqli_query($db, "SELECT id_utilisateur FROM topic WHERE id_topic =" . "'$id_topic' ");
$id = mysqli_fetch_all($idPosteur, MYSQLI_BOTH);

/* ***************** */

/*  Recherche Nom Prénom Posteur */

$idU = $id[0][0];
$NomPosteur =  mysqli_query($db, "SELECT nom_utilisateur,prenom_utilisateur FROM utilisateur WHERE id_utilisateur=" . "'$idU'");
$NomPrenom = mysqli_fetch_all($NomPosteur, MYSQLI_BOTH);

/* ***************** */


?>

<div class="container-fluid container1 text-center">
  <div class="row   justify-content-center">

    <!-- Titre du Topic -->
    <div class="col-12 Titre text-center">
      <h1><?php echo $topic[0][2] ?></h1>
    </div>
  </div>

  <!-- Cadre Profil commentaire principal -->
  <div class="row col-10  mr-1 mb-4 pb-3 mx-auto">

    <!-- Nom et Prénom du Posteur Topic-->
    <div class="col-2 offset-1  Titre ">
      <h4><?php echo $NomPrenom[0][0];
          echo "  ";
          echo $NomPrenom[0][1] ?></h4>
        <p><?php
                echo $topic[0]['date_t'];
        ?></p>
      <?php if ((isset($_SESSION['role']) && $_SESSION['role'] != "Membre") || (isset($_SESSION['iduser']) && $topic[0]['id_utilisateur'] == $_SESSION['iduser'])) { ?>
        <a value="del" class="btn Bouton-Admin-1 btn-sm" href="<?php

                                                          if ($_GET['categorie'] == 1) {
                                                            echo "Accueil.php?action=forumSante";
                                                          } elseif ($_GET['categorie'] == 2) {
                                                            echo "Accueil.php?action=forumAlimentation";
                                                          } elseif ($_GET['categorie'] == 3) {
                                                            echo "Accueil.php?action=forumDivertissement";
                                                          } elseif ($_GET['categorie'] == 4) {
                                                            echo "Accueil.php?action=forumAutre";
                                                          }

                                                          ?>&link=del&id_topic=<?php echo $id_topic ?>&categorie=<?php echo $_GET['categorie'] ?>" style="color:white; text-decoration:none;">Supprimer</a>
      <?php } ?>
    </div>
    <!-- Contenu du Topic -->

    <div class="col-8 mt-4 shadow aff-article2 text-left p-4"><a style="color: black;"><?php echo $topic[0][4] ?></a></div>
    <div class="col-7 ml-5"></div>
    <div class="col-4 ml-3">
      <form method="post" action="Accueil.php?action=formForumComm&id_topic=<?php echo $id_topic ?>&categorie=<?php echo $_GET['categorie'] ?>">
        <?php if (isset($_SESSION["email"])) { ?>
          <button class="btn Bouton-Admin-1" type="submit"><a class="text-decoration-none text-white">Répondre</a></button>
        <?php } ?>

      </form>
    </div>
  </div>

  <!-- Ajouter un commentaire  -->
  <div>
    <?php include_once("ForumArticleAffichage.php");  ?>
  </div>

</div>

<script>
        window.onload=function() {

        $("#paginForumCommentaire li").remove();
        loadPaginationForumCommentaire();

        }
    </script>