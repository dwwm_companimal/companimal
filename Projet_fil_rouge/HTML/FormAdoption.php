<?php
include_once('../SERVICE/ServiceEspeceRace.php');
include_once('../SERVICE/ServiceRefuge.php');
include_once('../SERVICE/ServiceAdoption.php');


if (isset($_GET["Action"]) && $_GET["Action"] == "ModifierAdoption") {

    $SelectIdAdoption = new ServiceAdoption();
    $data = $SelectIdAdoption->SelectIdAdoption($_GET["id_adoption"]);
}
?>

<!DOCTYPE html>
<html>

<head>

    <title>Formulaire d'Adoption</title>
    <link rel="stylesheet" href="../css/bootstrap.css" />
    <link rel="stylesheet" href="../css/style.css" />

    <link rel="icon" href="../img/patteblanche.png">

</head>

<body>
    <!-- Debut Body Formulaire Adoption-->
    <div class="Background-Form">
        <div class="container-fluid">
            <!-- Titre Form Adoption-->
            <div>
                <div class="row">
                    <div class="col-lg-4 offset-lg-4 ">
                        <h1 class="Titre"><?php

                                            if (isset($_GET["Action"]) && $_GET["Action"] == "ModifierAdoption") {
                                                echo "Modifier l'adoption";
                                            } else {
                                                echo 'Ajouter une adoption';
                                            } ?></h1>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <!-- Formulaire d'Adoption-->
                    <form id="formAdoption" method="POST" enctype="multipart/form-data" action="<?php
                                                                                                if (isset($_GET["Action"]) && $_GET["Action"] == "ModifierAdoption") {
                                                                                                    echo "Accueil.php?action=ficheAdoption&id_adoption=" . $data[0]['id_adoption'] . "";
                                                                                                } else {
                                                                                                    echo "Accueil.php?action=adoption";
                                                                                                }
                                                                                                ?>">

                        <div class="form-group row">
                            <!--Nom de l'animal-->
                            <div class="col-lg-3"></div>
                            <label for="NomAnimal" class="col-lg-2 col-form-label" style="font-weight:bold;">Nom de l'Animal :</label>
                            <div class="col-lg-4 col-6">
                                <input type="text" class="form-control" id="NomAnimal" name="NomAnimal" required autofocus value="<?php
                                                                                                                                    if (isset($_GET["Action"]) && $_GET["Action"] == "ModifierAdoption") {
                                                                                                                                        echo $data[0]["nom_animal"];
                                                                                                                                    } ?>">
                            </div>
                        </div>
                        <!-- Espece-->
                        <div class="form-group row">
                            <div class="col-lg-3"></div>
                            <label for="EspeceAnimal" class="col-lg-2 col-form-label" style="font-weight:bold;" data-target="#RaceAnimal">Espéce :</label>
                            <div class="col-lg-4 col-3">
                                <select class="form-control" name="EspeceAnimal" id="EspeceAnimal" required>
                                    <?php
                                    if (isset($_GET["Action"]) && $_GET["Action"] == "ModifierAdoption") {
                                        echo '<option selected>' . $data[0]["nom_espece"] . '</option>';
                                    } else {
                                        echo '<option selected value="">Selectionner une Espéce</option>';
                                    }

                                    ?>

                                    <?php
                                    $Espece = new ServiceEspeceRace();
                                    $data = $Espece->SelectEspece();

                                    for ($i = 0; $i < count($data); $i++) {
                                        echo '<option>' . $data[$i]["nom_espece"] . '</option>';
                                    }

                                    ?>
                                </select>
                            </div>
                        </div>
                        <!-- Race-->
                        <div class="form-group row">
                            <div class="col-lg-3"></div>
                            <label for="RaceAnimal" class="col-lg-2 col-form-label" style="font-weight:bold;">Race :</label>
                            <div class="col-lg-4 col-6">
                                <select class="form-control" id="RaceAnimal" name="RaceAnimal" required>
                                    <?php
                                    if (isset($_GET["Action"]) && $_GET["Action"] == "ModifierAdoption") {

                                        $SelectIdAdoption = new ServiceAdoption();
                                        $data = $SelectIdAdoption->SelectIdAdoption($_GET["id_adoption"]);

                                        echo '<option selected>' . $data[0]["nom_race"] . '</option>';
                                    } else {
                                        echo "<option selected value=''>Selectionner d'abord une Espéce</option>";
                                    }

                                    ?>
                                    <?php

                                    include_once("../HTML/AdoptionAffichageRace.php");



                                    /*$Race = new ServiceEspeceRace();
                                    $data = $Race-> SelectRace();
                                
                                    for($i=0; $i < count($data); $i++){
                                        echo '<option>'.$data[$i]["Nom_race"].'</option>';
                                    }*/

                                    ?>
                                </select>
                            </div>
                        </div>
                        <!-- Dowload Photo-->
                        <?php
                        if (isset($_GET["Action"]) && $_GET["Action"] !== "ModifierAdoption") {

                            echo '  <div class="form-group row">';
                            echo '       <div class="col-lg-3 "></div>';
                            echo '       <label class="col-lg-2 col-form-label"><strong> Photo :</strong></label>';
                            echo '       <div class="col-lg-4 col-6">';
                            echo '       <input type="file"  name="userImage"/>';
                            echo '      </div>';
                            echo '    </div>';
                        }
                        ?>
                        <!--  Sexe -->
                        <div class="form-group row ">
                            <div class="col-lg-3"></div>
                            <label for="SexeAnimal" class="col-lg-2 col-form-label" style="font-weight:bold;">Sexe :</label>
                            <div class="col-lg-6 col-8 puces">
                                <?php
                                if (isset($_GET["Action"]) && $_GET["Action"] == "ModifierAdoption") {

                                    $SelectIdAdoption = new ServiceAdoption();
                                    $data = $SelectIdAdoption->SelectIdAdoption($_GET["id_adoption"]);

                                    if ($data[0]["sexe_animal"] == "Male") {
                                        echo '<div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="SexeAnimal" id="inlineRadio1" value="Male" checked>
                                    <label class="form-check-label" for="Male">Male</label>
                                    </div>

                                    <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="SexeAnimal" id="inlineRadio2" value="Femelle">
                                    <label class="form-check-label" for="Femelle">Femelle</label>
                                    </div>';
                                    } else {
                                        echo '<div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="SexeAnimal" id="inlineRadio1" value="Male">
                                    <label class="form-check-label" for="Male">Male</label>
                                    </div>

                                    <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="SexeAnimal" id="inlineRadio2" value="Femelle" checked>
                                    <label class="form-check-label" for="Femelle">Femelle</label>
                                    </div>';
                                    }
                                } else {
                                    echo '<div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="SexeAnimal" id="inlineRadio1" value="Male" Required>
                                    <label class="form-check-label" for="Male">Male</label>
                                  </div>

                                  <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="SexeAnimal" id="inlineRadio2" value="Femelle">
                                    <label class="form-check-label" for="Femelle">Femelle</label>
                                  </div>';
                                }
                                ?>
                            </div>
                        </div>

                        <!-- Age Animal-->
                        <div class="form-group row">
                            <div class="col-lg-3"></div>
                            <label for="AgeAnimal" class="col-lg-2 col-form-label" style="font-weight:bold;">Age :</label>
                            <div class="col-lg-3 col-3">
                                <input type="number" class="form-control" id="AgeAnimal" name="AgeAnimal" required value="<?php
                                                                                                                            if (isset($_GET["Action"]) && $_GET["Action"] == "ModifierAdoption") {
                                                                                                                                echo $data[0]["naissance_animal"];
                                                                                                                            } ?>">
                            </div>
                        </div>
                        <!--Refuge-->
                        <div class="form-group row">
                            <div class="col-lg-3"></div>
                            <label for="RefugeAnimal" class="col-lg-2 col-form-label" style="font-weight:bold;">Refuge :</label>
                            <div class="col-lg-4 col-6">
                                <select class="form-control" id="RefugeAnimal" name="RefugeAnimal" required>
                                    <?php
                                    if (isset($_GET["Action"]) && $_GET["Action"] == "ModifierAdoption") {

                                        $Refuge = new  ServiceRefuge();
                                        $data2 = $Refuge->SelectRefugeId($data[0]["id_refuge"]);

                                        echo '<option value="' . $data2[0]["id_refuge"] . '" selected>' . $data2[0]["nom_refuge"] . '</option>';
                                    } else {
                                        echo '<option selected value="">Selectionner un Refuge</option>';
                                    }
                                    ?>


                                    <?php
                                    $Refuge = new ServiceRefuge();
                                    $data = $Refuge->SelectAllRefuge();

                                    for ($i = 0; $i < count($data); $i++) {
                                        echo '<option value="' . $data[$i]["id_refuge"] . '">' . $data[$i]["nom_refuge"] . '</option>';
                                    }

                                    ?>
                                </select>
                            </div>
                        </div>
                        <!-- Description-->
                        <div class="form-group row">
                            <div class="col-lg-3"></div>
                            <label for="DescriptionAnimal" class="col-lg-2 col-form-label" style="font-weight:bold;">Description :</label>
                            <div class="col-lg-6 col-10">
                                <textarea class="form-control" id="DescriptionAnimal" name="DescriptionAnimal" rows="6" required><?php
                                                                                                                                    if (isset($_GET["Action"]) && $_GET["Action"] == "ModifierAdoption") {

                                                                                                                                        $SelectIdAdoption = new ServiceAdoption();
                                                                                                                                        $data = $SelectIdAdoption->SelectIdAdoption($_GET["id_adoption"]);

                                                                                                                                        echo $data[0]["description_animal"];
                                                                                                                                    } ?></textarea>
                            </div>
                        </div>

                        <!--Bouton d'action -->
                        <input type="hidden" name="Action" value="<?php if (isset($_GET["Action"]) && $_GET["Action"] == "ModifierAdoption") {
                                                                        echo 'ModifierAdoption';
                                                                    } else {
                                                                        echo 'AjoutAdoption';
                                                                    } ?>">


                        <?php
                        if (isset($_GET["Action"]) && $_GET["Action"] == "ModifierAdoption") {

                            echo '<input type="hidden" name="Id_Adoption" value="' . $data[0]["id_adoption"] . '">';
                        }
                        ?>
                        <!-- Bouton d'envoi-->
                        <div class="row">
                            <div class="col-lg-5"></div>
                            <div class="col-lg-4">
                                <button type="submit" class="btn Bouton-Admin-1">Valider</button>

                                <a href="Accueil.php?action=adoption">
                                    <button type="button" class="btn Bouton-Admin-1">Retour</button>
                                </a>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Fin Body Formulaire d'Adoption-->
</body>

</html>