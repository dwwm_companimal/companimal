<?php include_once('../SERVICE/ServiceEspeceRace.php'); ?>


<!DOCTYPE html>
<html>
<head>

            <title>Formulaire Pet'Sitter</title>
            <link rel="stylesheet" href="../css/bootstrap.css"/>
            <link rel="stylesheet" href="../css/style.css"/>
           
            <link rel="icon" href="../img/patteblanche.png">
            
</head>
            
  <body>
    <!-- Début body Pet Sitter-->
    <div class="Background-Form">
        <div class="container-fluid">
            <!-- Titre Pet'Sitter-->
            <div>
                <div class="row">
                  <div class="mx-auto ">
                  <h1 class="Titre">Ajout Pet'Sitter</h1>
                  </div>
                </div>
              </div>
            <!-- Fin Titre Pet'Sitter-->


            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <!-- Formulaire Pet'Sitter -->
                    <form method="post" enctype="multipart/form-data" action="accueil.php?action=petsitter" id="formPetSitter">
                        <!-- Nom de l'annonce-->                        
                        <div class="form-group row">
                            <div class="col-lg-3"></div>
                            <label for="NomAnnonceFormPetSitter" class="col-lg-3 col-form-label" style="font-weight:bold;">Nom de l'Annonce :</label>
                            <div class="col-lg-4 col-6">
                              <input type="text" class="form-control" name="NomAnnonceFormPetSitter" required autofocus>
                            </div>
                        </div>                        
                        <!-- Lieu -->
                        <div class="form-group row">
                            <div class="col-lg-3"></div>
                            <label for="CodePostalLieuPetSitter" class="col-lg-3 col-form-label" style="font-weight:bold;">Code Postal :</label>
                            <div class="col-lg-4 col-6">
                              <input type="number" class="form-control" name="CodePostalPetSitter" required>
                            </div>
                        </div>
                        <!-- Type de Garde-->
                        <div class="form-group row">
                            <div class="col-lg-3"></div>
                            <label for="GardePetSitter" class="col-lg-3 col-form-label" style="font-weight:bold;">Type de Garde : </label>
                            <div class="col-lg-4 col-6 puces">
                                <div class="form-check form-check-inline">
                                   
                                    <input class="form-check-input" type="radio" name="Garde" id="inlineRadio1" value="Chez moi" required>
                                    <label class="form-check-label" for="ChezMoi">Chez moi</label>
                                  </div>

                                  <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="Garde" id="inlineRadio2" value="Chez vous">
                                    <label class="form-check-label" for="ChezVous">Chez vous</label>
                                  </div>

                                  <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="Garde" id="inlineRadio2" value="Promenade">
                                    <label class="form-check-label" for="Promenade">Promenade</label>
                                  </div>
                            </div>
                        </div>
                        <!-- Type de domicile -->
                        <div class="form-group row">
                            <div class="col-lg-3"></div>
                            <label for="DomicilePetSitter" class="col-lg-3 col-form-label" ><strong>Afficher votre numéro ?</strong></label>
                            <div class="col-lg-4 col-6 puces">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="afftel" id="inlineRadio1" value="oui" required>
                                    <label class="form-check-label" for="Maison">Oui</label>
                                  </div>

                                  <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="afftel" id="inlineRadio2" value="non">
                                    <label class="form-check-label" for="Appartement">Non</label>
                                  </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-3"></div>
                            <label for="DomicilePetSitter" class="col-lg-3 col-form-label"><strong>Type de Domicile :</strong></label>
                            <div class="col-lg-4 col-6 puces">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" name="Domicile" type="radio" id="inlineRadio1" value="Maison" required>
                                    <label class="form-check-label" for="Maison">Maison</label>
                                </div>

                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="Domicile" id="inlineRadio2" value="Appartement">
                                    <label class="form-check-label" for="Appartement">Appartement</label>
                                </div>
                            </div>
                        </div>
                        <!--Télécharger une photo-->
                        <div class="form-group row">
                            <div class="col-lg-3"></div>
                            <label class="col-lg-3 col-form-label"><strong>Télécharger une Photo :</strong></label>
                            <div class="col-lg-4 col-6">
                            <input type="file" name="userImage"/>
                            </div>
                        </div>  
                        
                        <!-- Esepece Animal Gardé-->
                        <div class="form-group row">
                            <div class="col-lg-3"></div>
                            <label for="EspecePetSitter" class="col-lg-3 col-form-label" style="font-weight:bold;">Espéce d'Animal Gardé :</label>
                            <div class="col-lg-4 col-6">
                            <select class="form-control" name="EspecePetSitter" id="EspecePetSitter" required>
                                <option selected value="">Selectionnez une espéce</option>
                                <?php
                                    $Espece = new ServiceEspeceRace();
                                    $data = $Espece-> SelectEspece();
                                
                                    for($i=0; $i < count($data); $i++){
                                        echo '<option>'.$data[$i]["nom_espece"].'</option>';
                                    }

                                ?>
                                </select>
                            </div>
                        </div>
                        <!-- Information Complémentaire-->
                        <div class="form-group row">
                            <div class="col-lg-3"></div>
                            <label for="InfoCompPetSitter" class="col-lg-3 col-form-label" style="font-weight:bold;">Information Complémentaire : </label>
                            <div class="col-lg-4 col-6">
                                <textarea class="form-control" name="InfoCompPetSitter" rows="6" required></textarea>
                            </div>
                        </div>
                        <!-- Bouton envoie Formulaire Pet'sitter-->
                        <div class="row">
                            <div class="col-lg-6"></div>
                            <div class="col-lg-2">
                                <button type="submit" name="creerPet" value="creer" class="btn Bouton-Admin-1">Valider</button>
                                <a href="Accueil.php?action=petsitter"><button type="button" class="btn Bouton-Admin-1 ">Retour</button></a>
                                </a>
                            </div>
                        </div>   
                    </form>
                    <!-- Fin Formulaire-->
                </div>
            </div>
        </div>
    </div>
    <!-- Fin Body Pet'Sitter-->
</body>
</html>