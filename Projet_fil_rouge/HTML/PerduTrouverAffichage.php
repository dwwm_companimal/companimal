<?php   

    include_once("../SERVICE/ServicePerduTrouver.php");
 
    if( 
        (isset($_POST["nom_espece"]) && $_POST["nom_espece"] != "")
    OR 
       ( isset($_POST["type_perdu_trouve"]) && $_POST["type_perdu_trouve"] != "")
    OR 
       ( isset($_POST["code_postale_perdu_trouve"]) && $_POST["code_postale_perdu_trouve"] != "")
    ){

        $SelectPerduTrouveRecherche = new ServicePerduTrouver();
        $data = $SelectPerduTrouveRecherche->SelectPerduTrouveRecherche($_POST);

    }else{

        $SelectAllPerduTrouver = new ServicePerduTrouver();
        $data = $SelectAllPerduTrouver->SelectAllPerduTrouver();
    

    }

        if(count($data)>0){

            echo '<div class="row aff-article justify-content-around mt-3">';

            for($i = 0; $i < count($data); $i++){

                echo '<div class="col-lg-3 col-md-5 col-sm-8  aside-adopt m-3 contentDisplayPerduTrouve ">
                    <div class="row mx-auto bloc-aside justify-content-center ">
                        <h5 class=" col-12 pt-2 text-center">'.$data[$i]['type_perdu_trouve'].' '.$data[$i]['nom_animal_perdu_trouve'].'</h5>
                        <a href="Accueil.php?Action=fichePerduTrouve&id_perdu_trouve='.$data[$i]['id_perdu_trouve']. '"><img class="img-article" src="data:image/jpg;base64,'.base64_encode($data[$i]['photo_perdu']).'" alt=""></a> 
                        <div class="col-12 adop_dessous_carte"><h5 class="pt-2 justify-content-center"></h5></div>    
                    </div> 
                </div>';
            }
            echo  '</div>';
        }else{
            echo "<div style='text-align:center;'>
                    <p> Nous n'avons trouvé aucun résultat </p>
                  </div>";
          }
?>
            <div class="row">
              <div class="mx-auto mt-2" >
                  <nav aria-label="Page navigation example" class="text-center">
                    <ul id="paginPerduTrouve" class="pagination justify-content-center">
                    </ul>
                  </nav>
                </div>
              </div>

  