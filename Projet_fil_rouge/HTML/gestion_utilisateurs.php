<?php
include_once("../DAO/UtilisateurDAO.php");
include_once("../SERVICE/ServiceUtilisateur.php");
?>

<!DOCTYPE html>
<html>

<head>
  <meta charset=``utf-8`` />
  <title>Header Footer</title>
  <link rel="stylesheet" href="../css/bootstrap.css" />
  <link rel="stylesheet" href="../css/style.css" />

  <link rel="icon" href="../img/patteblanche.png">

</head>

<body>
  <!-- header       -->

  <!-- fin header       -->





  <div class="container mb-3  ">
    <div class="row">
      <div class="mx-auto ">
        <h1 class="Titre">Gestion des Utilisateurs</h1>
      </div>
    </div>
    <div class="container  text-center ">
      <div class="row col-12   justify-content-center mt-3 ">
        <?php
        $Userdao = new UtilisateurDAO;
        $ServiceUtilisateur = new ServiceUtilisateur();
        if (isset($_POST['changerRole'])) {
          $ServiceUtilisateur->modifRole($_POST);
        }





        echo '<table class="table bg-nv ml-4" style="width:100%;">';


        $data = $Userdao->selectAll();

        $i = 0;

        for ($i = 0; $i <= count($data) - 1; $i++) {
          $col0 = $data[0];
          echo '<tr>';

          echo '<td>';
          echo $data[$i]['id_utilisateur'];
          echo '</td>';
          echo '<td>';
          echo $data[$i]['email'];
          echo '</td>';
          echo '<td>';
          echo $data[$i]['nom_utilisateur'] . ' ' . $data[$i]['prenom_utilisateur'];
          echo '</td>';

          echo '<td>';
          echo $data[$i]['nom_role'];
          echo '</td>';



          echo '<td class=" "  style="width:70%;">';
          if (isset($_SESSION['role']) && $_SESSION['role'] != 'Membre') {
            if ($data[$i]['nom_role'] == 'Membre' or ($data[$i]['nom_role'] == 'Admin' && $_SESSION['role'] == 'SuperAdmin')) {
              echo '<div class="row" >';
              echo '<form action="Accueil.php?action=gestionUsers" method="post" class=col-4>';
              echo '<input type="hidden" name="editUser" value="yes"> ';
              echo '<input type="hidden" name="usernameedit" value="' . $data[$i]['id_utilisateur'] . '">';
              echo '<a  class="btn btn-sm Bouton-Admin-2 btnRole"  id="gg"   value="Changer rôle">changer role</a>';
              echo '</form>';
            }
          }
          echo ' <form action="Accueil.php?action=gestionUsers" method="post"  class="col-6 formHid d-none">
              <div class=" form-row justify-content-center">
                <input type="hidden" class="form-control  is-valid" id="inputid" name="idUser" value="' . $data[$i]['id_utilisateur'] . '" required>
                <div class="col-8 mt-2">
                  <select class="form-control" name="role">
                    <option value="Membre">Membre</option>
                    <option value="Admin">Admin</option>';
          if ($_SESSION["role"] == "SuperAdmin") {
            echo ' <option value="SuperAdmin">SuperAdmin</option>';
          }
          echo '</select>
                </div>
                <div class="col-3">
                  <button type="submit" class="btn Bouton-Admin-2 btn-sm" name="changerRole" value="modifier">valider</button>
                </div>
              </div>
            </form>
            </div>';
          echo '</td>';


          echo '<td class=" ">';
          if (isset($_SESSION['role']) && $_SESSION['role'] != 'Membre') {
            if ($data[$i]['nom_role'] == 'Membre' or ($data[$i]['nom_role'] == 'Admin' && $_SESSION['role'] == 'SuperAdmin')) {
              echo '<form  class="suppUser">';
              echo '<input type="hidden" name="delUser" value="yes"> ';
              echo '<input type="hidden" name="usernameDel" value="' . $data[$i]['id_utilisateur'] . '">';
              echo '<input class="btn  btn-sm Bouton-Admin-2 " type="submit"  value="supprimer">';
              echo '</form>';
            }
          }
          echo '</td>';
          echo '</tr>';
        }
        echo '</table>';
        ?>
      </div>



      <div class="row">
        <div class="col-lg-9"></div>
        <div class="col-lg-3">
          <a href="Accueil.php">
            <button type="button" class="btn Bouton-Admin-1">Retour</button>
          </a>
        </div>
      </div>

    </div>
  </div>






</body>

</html>