<?php
include_once('..\SERVICE\ServiceArticle.php');
include_once('..\DAO\ArticleDAO.php');
include_once("../SERVICE/ServiceAdoption.php");
include_once("../DAO/PerduTrouverDAO.php");
?>


<!-- debut page       -->
<?php
/* Ajout Article */

if (isset($_POST["creerArticle"])) {

  $AjoutArticle = new ServiceArticle();
  $AjoutArticle->AjoutArticle($_POST);
}

include('caroussel.php');
?>


<div class="container-fluid text-center container1 ">
  <div class="row ">

    <div class="row col-lg-8 offset-1 ">
      <div class=" mx-auto">
        <h1 class="Titre">Quoi de neuf</h1>
        <?php if (isset($_SESSION['role']) && $_SESSION['role'] != "Membre") { ?>
          <a class="btn Bouton-Admin-1 mt-3" href="Accueil.php?action=formArticle" type="submit">Ajouter un article</a>
        <?php } ?>
      </div>
    </div>

    <!-- affichage des articles       -->


    <?php

    $SelectAllArticle = new ServiceArticle();
    $data = $SelectAllArticle->SelectAllArticle();

    $NbrArticleParPage = 5;
    $NbrArticleTotal = count($data);

    $NombreDePageTotale = ceil($NbrArticleTotal / $NbrArticleParPage);

    if (isset($_GET['page']) and !empty($_GET['page']) and $_GET['page'] > 0) {
      $_GET['page'] = intval($_GET["page"]);
      $pageEnCour = $_GET["page"];
    } else {
      $pageEnCour = 1;
    }

    $debut = ($pageEnCour - 1) * $NbrArticleParPage;

    $SelectArticle = new ServiceArticle();
    $data = $SelectArticle->SelectArticle($debut, $NbrArticleParPage);


    $SelectArticle = new ServiceArticle();
    $data = $SelectArticle->SelectArticle($debut, $NbrArticleParPage);



    echo '<div class="col-lg-8 offset-lg-1  m-sm-4 ">
                  ';
    if (count($data) > 0) {
      for ($i = 0; $i < count($data); $i++) {

        // Nombre de caractère
        $max = 100;
        $chaine = $data[$i]["contenu_article"];
        if (strlen($chaine) >= $max) {
          // Met la portion de chaine dans $chaine
          $chaine = substr($chaine, 0, $max);
          // position du dernier espace
          $espace = strrpos($chaine, " ");
          // test si il ya un espace
          if ($espace)
            // si ya 1 espace, coupe de nouveau la chaine
            $chaine = substr($chaine, 0, $espace);
          // Ajoute ... à la chaine
          $chaine .= '...';
        }



        echo  '<div class="row col-11 offset-1 justify-content-center  mb-2  ">
                  <div class="col-lg-4 mb-3">
                    <a href="Accueil.php?action=fichearticle&idarticle=' . $data[$i]["id_article"] . '"><img class="shadow img-article2" width="280px" src="data:image/jpg;base64,' . base64_encode($data[$i]['image_article']) . '" alt=""></a>
                  </div>
                  
                  <div class="col-lg-7 text-left Titre2 aff-article2 shadow mb-3">
                    <h4>' . $data[$i]["titre_article"] . '</h4>
                    <p>' . $chaine . '</p>
                  </div>';
        if (isset($_SESSION['role']) && $_SESSION['role'] != "Membre") {
          echo '<div class="col-lg-11 align-self-center ">
                    <div class="row justify-content-end"> 
                     
                  <form action="Accueil.php?action=formArticle" method="post">
                        <input type="hidden" name="editArticle" value="yes">                                                       
                        <input type="hidden" name="articleidedit" value="' . $data[$i]['id_article'] . '">
                        <input  class="btn Bouton-Admin-1 "   type="submit"  value="Modifier">
                      </form>
                      <form  class="suppArticle">
                        <input type="hidden" name="delArticle" value="yes">                                                       
                        <input type="hidden" name="articleiddel" value="' . $data[$i]['id_article'] . '">
                        <input  class="btn Bouton-Admin-1  ml-2 "   type="submit"  value="Supprimer">
                      </form>
                    </div>
                  </div>';
        }
        echo ' </div>';
      }
    }

    ?>

    <!-- aside       -->

    <?php $SelectAdoption = new ServiceAdoption();
    $adopt = $SelectAdoption->SelectAllAdoption();
    $SelectAllPerduTrouver = new PerduTrouverDAO();
    $perdu = $SelectAllPerduTrouver->SelectAllPerdu();
    $trouve = $SelectAllPerduTrouver->SelectAllTrouve();

    ?>
  </div>
  <div class="col-lg-2 col-sm-12 aside-adopt ml-lg-3 mt-4">
    <?php if (count($adopt) > 0) { ?>
      <div class="row mx-auto bloc-aside justify-content-center shadow ">
        <h5 class="col-12 pt-2 text-center"><?php echo $adopt[0]['nom_animal']; ?></h5>
        <a href="Accueil.php?retour=accueil&action=ficheAdoption&id_adoption=<?php echo $adopt[0]['id_adoption']; ?>"><img class="img-article" width="240px" src="data:image/jpg;base64,<?php echo base64_encode($adopt[0]['photo_animal']) ?>" alt=""></a>
        <button class="btn btn-100 Bouton-Admin-1 " type="submit">
          <h5>Adopter</h5>
        </button>
      </div>
    <?php } else { ?>
      <div class="row mx-auto bloc-aside justify-content-center mt-5 shadow">
        <h5 class="col-12 pt-2 text-center">Medor</h5>
        <a href="Accueil.php?action=adoption"><img class="img-article" width="240px" src="../img/chien_adopt.jpg" alt=""></a>
        <button class="btn btn-100 Bouton-Admin-1 " type="submit">
          <h5>Adopter</h5>
        </button>
      </div>
    <?php } ?>



    <?php if (count($perdu) > 0) { ?>
      <div class="row mx-auto bloc-aside justify-content-center mt-5 shadow">
        <h5 class="col-12 pt-2 text-center"><?php echo $perdu[0]['nom_animal_perdu_trouve']; ?></h5>
        <a href="Accueil.php?retour=accueil&Action=fichePerduTrouve&id_perdu_trouve=<?php echo $perdu[0]['id_perdu_trouve']; ?>"><img class="img-article" width="240px" src="data:image/jpg;base64,<?php echo base64_encode($perdu[0]['photo_perdu']) ?>" alt=""></a>
        <button class="btn btn-100 Bouton-Admin-1 " type="submit">
          <h5>Perdu</h5>
        </button>
      </div>
    <?php } else { ?>
      <div class="row mx-auto bloc-aside justify-content-center mt-5 shadow">
        <h5 class="col-12 pt-2 text-center">Caramel</h5>
        <a href="Accueil.php?action=perduTrouve"><img class="img-article" width="240px" src="../img/chat_perdu.jpg" alt=""></a>
        <button class="btn btn-100 Bouton-Admin-1 " type="submit">
          <h5>Perdu</h5>
        </button>
      </div>
    <?php } ?>




    <?php if (count($trouve) > 0) { ?>
      <div class="row mx-auto bloc-aside justify-content-center mt-5 shadow">
        <h5 class="col-12 pt-2 text-center"><?php echo $trouve[0]['nom_animal_perdu_trouve'];if($trouve[0]['nom_animal_perdu_trouve'] == "") {
                                              echo $trouve[0]['nom_espece'];}?></h5>
        <a href="Accueil.php?retour=accueil&Action=fichePerduTrouve&id_perdu_trouve=<?php echo $trouve[0]['id_perdu_trouve']; ?>"><img class="img-article" width="240px" src="data:image/jpg;base64,<?php echo base64_encode($trouve[0]['photo_perdu']) ?>" alt=""></a>
        <button class="btn btn-100 Bouton-Admin-1 " type="submit">
          <h5>Trouvé</h5>
        </button>
      </div>
    <?php } else { ?>
      <div class="row mx-auto bloc-aside justify-content-center mt-5 shadow">
        <h5 class="col-12 pt-2 text-center">Panpan</h5>
        <a href="Accueil.php?action=perduTrouve"><img class="img-article" width="240px" src="../img/lapin_trouve.jpg" alt=""></a>
        <button class="btn btn-100 Bouton-Admin-1 " type="submit">
          <h5>Trouvé</h5>
        </button>
      </div>
    <?php } ?>



  </div>
  <!-- Pagination     -->
  <div class="row col-9">
    <div class="mx-auto mt-2">
      <nav aria-label="Page navigation example" class="text-center">
        <ul class="pagination ">
          <?php
          for ($i = 1; $i <= $NombreDePageTotale; $i++) {
            echo '<li class="page-item"><a class="page-link" href="accueil.php?page=' . $i . '">' . $i . '</a></li>';
          }
          ?>
        </ul>
      </nav>
    </div>
  </div>








</div>
</div>

</div>