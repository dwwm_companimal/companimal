<?php
include_once('../SERVICE/ServiceUtilisateur.php');
session_start();

if (isset($_POST["updatePassword"]) &&  isset($_POST["oldPassword"]) && isset($_POST["password1"]) && isset($_POST["password2"])) {
    $ServiceUtilisateur = new ServiceUtilisateur;
    $answer = $ServiceUtilisateur->modifMDP($_POST);
    $jse = 0;
    if ($answer == "false3") {
        $jse = json_encode(array("error" => array(
            "code" => 889,
            "message" => "Ancien mot de passe incorrect"
        )), JSON_UNESCAPED_UNICODE);
        echo $jse;  
    } elseif ($answer == "true") {
        $jse = json_encode("success");
        echo $jse; 
    }
}