<?php
  include_once("../SERVICE/ServiceRefuge.php");

  
?>


<!DOCTYPE html>
	<html>
		<head>
			<meta charset="utf-8" />
            <title>Refuge</title>
            <link rel="stylesheet" href="../css/bootstrap.css"/>
            <link rel="stylesheet" href="../css/style.css"/>
           
            <link rel="icon" href="../img/patteblanche.png">
            
    </head>
    <body>
      <!-- header       -->
    
      <!-- fin header       -->
      <!-- debut page       -->
      <div class="container-fluid ">
        <div class="row">
          <div class="mx-auto">
            <h1 class="Titre">Nos Refuge Partenaire</h1>
          </div>
        </div>
        <!-- bloc annonces adoption -->
        <div class="row justify-content-center ">

          <div class="col-lg-8 mb-3">
            <div class="row justify-content-center ">
              <div class="mx-auto ">
              <?php if(isset($_SESSION['role']) && $_SESSION['role']!="Membre"){?>
                <a href="Accueil.php?link=formRefuge&Action=AjoutRefuge"><button type="button" class="btn Bouton-Admin-1">Ajouter un Refuge</button></a>
              <?php } ?>
              </div>
            </div>
             
            <?php

            if(isset($_POST["Action"])){
                if($_POST["Action"] == "AjoutRefuge"){
                    $AjoutRefuge= new ServiceRefuge();
                    $AjoutRefuge-> AjoutRefuge($_POST);
                }
            }

            if(isset($_GET["link"])){
                if($_GET["link"] == "SupRefuge"){
                  $SupRefuge = new ServiceRefuge();
                  $SupRefuge-> SupRefuge($_GET["id_refuge"]);
                }
              }


              include_once('../HTML/RefugeAffichage.php');
            ?>

        </div>       
          </div>
       
            
          <div class="row ">
            <div class="mx-auto" >
                <nav aria-label="Page navigation example" class="text-center">
                <ul class="pagination ">
                    <?php
                    for($i=1;$i<=$NombreDePageTotale;$i++){
                      echo '<li class="page-item"><a class="page-link" href="accueil.php?action=refuge&page='.$i.'">'.$i.'</a></li>';
                    }
                    ?>
                    </ul>
                  </ul>
                </nav>
              </div>
            </div>
      </div>
            
      <!-- debut page       -->
      <!-- footer       -->
     
     <!-- fin footer       -->
		</body>
	</html>
