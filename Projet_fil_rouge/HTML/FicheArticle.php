<?php
include_once('..\SERVICE\ServiceArticle.php');
include_once('..\DAO\ArticleDAO.php');

if(isset($_POST["modifierArticle"])){
           
  $ModifierArticle = new ServiceArticle();
  $ModifierArticle-> ModifArticle($_POST);

}

$SelectArticle = new ServiceArticle();
$data = $SelectArticle->SelectIdArticle($_GET["idarticle"]);


?>

<!DOCTYPE html>
	<html>
		<head>
			<meta charset=``utf-8`` />
            <title>Fiche Article</title>
            <link rel="stylesheet" href="../css/bootstrap.css"/>
            <link rel="stylesheet" href="../css/style.css"/>
            
            <link rel="icon" href="../img/patteblanche.png">
            
    </head>
    <body>
      <!-- header       -->
     
      <!-- fin header       -->
      <div>
        <div class="container mt-3 mb-3">
          <!-- Background Blanc-->
            <div class="row justify-content-center " >
              <div class="col-12 ">
              <div class="row justify-content-center">
                <div class="Background-Color-Fiche-Blanc col-12">
          <!-- Fin Background Blanc-->
          <!-- Image Fiche Animal-->      
                  <div class="Background-Color-Fiche-Image col-8 offset-2 mb-4">
                  <img width="580px" src="data:image/jpg;base64,<?php echo base64_encode($data[0]['image_article'])?>"></img>
                  </div>
          <!-- Fin Image Fiche Animal-->
          <!-- Texte Fiche Animal-->
                  <div class="Background-Color-Fiche-Texte" style="border-radius: 10px;">
                      <h3 style="text-align:center; padding-top: 20px;"><?php echo $data[0]['titre_article'];?></h3>
                    
                    
                    <div class="row">
                      <div class="col-lg-10  offset-1 Commentaire-Box mb-5 mt-5 text-center">
                        <p><?php echo $data[0]['contenu_article'];?></p>
                      </div>
                    </div>
                  </div>
        <!-- Fin Texte Fiche Animal -->

        <!-- Bouton Admin Creation/Suppresion-->
                  <div class=" row justify-content-end">
                 <?php if(isset($_SESSION['role']) && $_SESSION['role']!="Membre"){?>
                    <form action="Accueil.php?action=formArticle" method="post">
                      <input type="hidden" name="editArticle" value="yes">                                                       
                      <input type="hidden" name="articleidedit" value="<?php echo $data[0]['id_article'];?>">
                      <input  class="btn btn-lg Bouton-Admin-2 "  id="gg" type="submit"  value="Modifier">
                    </form>
                    <form  class="suppArticle">
                      <input type="hidden" name="delArticle" value="yes">                                                       
                      <input type="hidden" name="articleiddel" value="<?php echo $data[0]['id_article'];?>">
                      <input  class="btn btn-lg Bouton-Admin-2 "  id="gg" type="submit"  value="Supprimer">
                    </form>
                 <?php } ?>
                    <a class="btn btn-lg Bouton-Admin-2" href="Accueil.php">Retour</a>
                  </div>
        <!-- Fin Bouton Admin -->

        
        <!-- Fin Bouton Contact/Refuge-->
                </div>
              </div>
            </div>
            </div>
        </div>
    </div>

      <!-- footer       -->
      
     <!-- fin footer       -->
		</body>
	</html>
