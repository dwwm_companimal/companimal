<?php
include_once('..\SERVICE\ServicePetSitter.php');
include_once('..\DAO\PetSitterDAO.php');
include_once('../SERVICE/ServiceEspeceRace.php');




/* Ajout d'une annonce */
if (isset($_POST["creerPet"])) {
    $AjoutPetSitter = new ServicePetSitter();
    $AjoutPetSitter->ajoutPetsitter($_POST);
}

/* Suppression d'une annonce */
if (isset($_GET['link']) && $_GET['link'] = 'del' && isset($_GET['id_pet_sitter'])) {
    $SupPetSitter = new ServicePetSitter();
    $SupPetSitter->SupPetSitter($_GET['id_pet_sitter']);
}
?>


<!DOCTYPE html>
<html>

<head>
    <meta charset=``utf-8`` />
    <title>Pet'sitter</title>
    <link rel="stylesheet" href="../css/bootstrap.css" />
    <link rel="stylesheet" href="../css/style.css" />


</head>

<body>
    <div class="container-fluid container1 text-center">
        <div class="row col-lg-11  ml-5">
            <div class="mx-auto">
                <h1 class="Titre">Pet'Sitter</h1>
                <?php if (isset($_SESSION["email"])) { ?>

                    <a class="btn Bouton-Admin-1 mb-3 " href="Accueil.php?action=ajoutPetsiter" type="submit">Ajouter un article</a>
                <?php } ?>
            </div>
        </div>
        <!-- Recherche en sm -->
        <div class=" col-lg-8 aside-adopt  text-center title-filtre mx-auto ">
            

            <div class="row bloc-aside1 justify-content-around mb-3">
                
                    <div class="form-group row mt-2 justify-content-center ">
                        <label for="Espece" class="col-sm- col-form-label">Type de Garde</label>
                        <select class="custom-select col-10 mb-3 RecherchePetSitter" name="type_garde_pet_sitter" id="TypeGarde">
                            <option value="">Toutes les type de Garde </option>
                            <?php

                            $SelectGardePetSitter = new ServicePetSitter();
                            $data = $SelectGardePetSitter->SelectGardePetSitter();

                            for ($i = 0; $i < count($data); $i++) {
                                echo '<option>' . $data[$i]["type_garde_pet_sitter"] . '</option>';
                            }
                            ?>
                        </select>
                    </div>

                    <div class="form-group row mt-2 justify-content-center ">
                        <label for="Espece" class="col-sm- col-form-label">Type de Domicile</label>
                        <select class="custom-select col-10 mb-3 RecherchePetSitter" name="type_domicile_pet_sitter" id="TypeDomicile">
                            <option value="">Toutes les type de Domicile </option>
                            <?php

                            $SelectDomicilePetSitter = new ServicePetSitter();
                            $data = $SelectDomicilePetSitter->SelectDomicilePetSitter();

                            for ($i = 0; $i < count($data); $i++) {
                                echo '<option>' . $data[$i]["type_domicile_pet_sitter"] . '</option>';
                            }
                            ?>
                        </select>
                    </div>

                    <div class="form-group row  justify-content-center ">
                        <label for="Espece" class="col-sm-3 col-form-label">Espece</label>
                        <select class="custom-select col-10 mb-3 RecherchePetSitter" name="nom_espece" id="Espece">
                            <option value="">Toutes les Espece </option>
                            <?php
                            $Espece = new ServiceEspeceRace();
                            $data = $Espece->SelectEspece();

                            for ($i = 0; $i < count($data); $i++) {
                                echo '<option>' . $data[$i]["nom_espece"] . '</option>';
                            }
                            ?>
                        </select>
                    </div>

                    <div class="form-group row mt-2 justify-content-center ">
                        <label for="CodePostalPetSitter" class="col-sm-5 col-form-label">Code Postal</label>
                        <input type="text" class="custom-select col-10 mb-3" name="distance_pet_sitter" id="CodePostalPetSitter">
                    </div>

                </div>
            </div>
       
        <!-- Fin Filtre -->

        <!-- Titre-->


        <!-- Fin Titre -->

        <!-- Affichage-->

        <div class="row justify-content-center  ">
            <!-- affichage des articles petsitter -->

            <div class="col-lg-7   justify-content-center" id="AffichagePetSitter">

                <?php
                include_once('../HTML/PetSitterAffichage.php');
                ?>


            </div>
        </div>

        <!-- Fin Affichage -->

        <!-- Filtre -->



    </div>
    </div>

    <script>
        window.onload = function() {

            $("#paginPetSitter li").remove();
            loadPaginationPetSitter();

        }
    </script>
    <!-- Pagination     -->