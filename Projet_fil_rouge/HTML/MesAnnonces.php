<?php

include_once('..\DAO\UtilisateurDAO.php');
include_once('..\DAO\PetSitterDAO.php');
include_once('..\DAO\ForumDAO.php');
include_once('../SERVICE/ServicePerduTrouver.php');

/* Suppression d'une annonce */
if (isset($_GET['link'])&& $_GET['link']='delPet' && isset($_GET['id_pet_sitter'])){
  $servicePetSiter = new PetSitterDAO;
  $servicePetSiter -> SupPetSitter($_GET['id_pet_sitter']);
} 

if (isset($_GET['link'])&& $_GET['link']='delPerduTrouve' && isset($_GET['id_perdu_trouve'])){
    $SupPerduTrouver = new ServicePerduTrouver();
    $SupPerduTrouver-> SupPerduTrouver($_GET["id_perdu_trouve"]);
  }

  /* Suppression d'un Topic */
  if (isset($_GET['link'])&& $_GET['link']='delTopic' && isset($_GET['id_topic'])){
  $delTopic = new ForumDAO;
  $delTopic -> deleteTopic($_GET['id_topic']);
}





$utilisateurDAO = new UtilisateurDAO;
$allPet = $utilisateurDAO -> selectAllPetSitter($_SESSION['iduser']);
$allPerdu = $utilisateurDAO -> selectAllperdu($_SESSION['iduser']);
$allTrouve = $utilisateurDAO -> selectAllTrouve($_SESSION['iduser']);
$allTopic = $utilisateurDAO -> selectAllTopic($_SESSION['iduser']);
//$allComm = $utilisateurDAO -> selectAllCommentaire($_SESSION['iduser']);


?>

<!DOCTYPE html>
	<html>
		<head>
			<meta charset=``utf-8`` />
            <title>Header Footer</title>
            <link rel="stylesheet" href="../css/bootstrap.css"/>
            <link rel="stylesheet" href="../css/style.css"/>
            
            <link rel="icon" href="../img/patteblanche.png">
           
    </head>
    <body>
      <!-- header       -->
         
      <!-- fin header       -->
      <h2 class="grdTitre text-center mt-2" >Mes annonces</h2>
      <div class="container mt-3 mb-3 Background-Color-Fiche-Blanc ">
          
          
 <!-- Mes annonces perdu-->           
            <div class="row">
                <div class="col-12 mb-3 titresection  text-center">
                  <h2 class="">Animaux perdus</h2>
                </div>
            </div>
              <div class="row">
                <?php
                 
                if(count($allPerdu)>0){
                  echo '<table class="table table-sm bg-nv ml-3 mr-3">';
                  for($i=0; $i<=count($allPerdu)-1; $i++){ 
                    echo' <tr >';
                    echo'   <td class="align-middle " style="width:5%;">';
                    echo'   </td>';
                    echo'   <td class="align-middle " style="width:70%;">';
                    echo      '<a  href="Accueil.php?ret=annonces&Action=fichePerduTrouve&id_perdu_trouve='.$allPerdu[$i]['id_perdu_trouve'].'"><h3>'.$allPerdu[$i]["titre_perdu_trouve"].'</h3></a>';
                    echo'   </td>';
                    echo'   <td>';
                    echo      '<a class="btn Bouton-Admin-2" href="Accueil.php?link=formPerduTrouve&Action=ModifierPerduTrouver&id_perdu_trouve='.$allPerdu[0]["id_perdu_trouve"].'" style="color:white; text-decoration:none;">Modifier</a>';
                    echo'   </td>';
                    echo'   <td>';
                    echo      '<a value="del" class="btn  Bouton-Admin-2"  href="Accueil.php?action=mesAnnonces&id_perdu_trouve='.$allPerdu[$i]['id_perdu_trouve'].'&link=delPerduTrouve" style="color:white; text-decoration:none;">Supprimer</a>';
                    echo'   </td>';
                    echo' </tr>';
                            
                  }
                  echo '</table>'; 
                }else{
                  echo '<div class="col-12 text-center"><h5>Vous n\'avez pas d\'annonces Animaux perdus</h5></div>';
                }
                
                 ?>
              </div>

<!-- Mes annonces trouve-->           
            <div class="row">
                <div class="col-12 mb-3 titresection  text-center">
                  <h2 class="">Animaux trouvés</h2>
                </div>
            </div>
              <div class="row">
                <?php
                 
                if(count($allTrouve)>0){
                  echo '<table class="table table-sm bg-nv ml-3 mr-3">';
                  for($i=0; $i<=count($allTrouve)-1; $i++){ 
                    echo' <tr >';
                    echo'   <td class="align-middle " style="width:5%;">';
                    echo'   </td>';
                    echo'   <td class="align-middle " style="width:70%;">';
                    echo      '<a href="Accueil.php?ret=annonces&Action=fichePerduTrouve&id_perdu_trouve='.$allTrouve[$i]['id_perdu_trouve'].'"><h3>'.$allTrouve[$i]["titre_perdu_trouve"].'</h3></a>';
                    echo'   </td>';
                    echo'   <td>';
                    echo '    <a href="Accueil.php?link=formPerduTrouve&Action=ModifierPerduTrouver&id_perdu_trouve='.$allTrouve[0]["id_perdu_trouve"].'"><button type="button" class="btn Bouton-Admin-2">Modifier</button></a>';
                    echo'   </td>';
                    echo'   <td>';
                    echo '    <a value="del" class="btn btn Bouton-Admin-2"  href="Accueil.php?action=mesAnnonces&id_perdu_trouve='.$allTrouve[$i]['id_perdu_trouve'].'&link=delPerduTrouve" style="color:white; text-decoration:none;">Supprimer</a></div>';
                    echo'   </td>';
                    echo' </tr>';
                            
                  }
                  echo '</table>'; 
                }else{
                  echo '<div class="col-12 text-center"><h5>Vous n\'avez pas d\'annonces Animaux trouvés</h5></div>';
                }
                
                 ?>
              </div>

<!-- Mes annonces petSitter-->           
            <div class="row">
                <div class="col-12 mb-3 titresection  text-center">
                  <h2 class="">Pet'Sitter</h2>
                </div>
            </div>
              <div class="row">
                <?php
                 
                if(count($allPet)>0){
                  echo '<table class="table table-sm bg-nv ml-3 mr-3">';
                  for($i=0; $i<=count($allPet)-1; $i++){ 
                    echo' <tr >';
                    echo'   <td class="align-middle " style="width:5%;">';
                    echo'   </td>';
                    echo'   <td class="align-middle " style="width:70%;">';
                    echo      '<a href="Accueil.php?ret=annonces&action=fichePetsiter&idPet='.$allPet[$i]['id_pet_sitter'].'"><h3>'.$allPet[$i]["titre_pet_sitter"].'</h3></a>';
                    echo'   </td>';
                    echo'   <td>';
                    echo '    <form action="Accueil.php?action=modifpetsiter" method="post">';
                    echo '      <input Class="btn Bouton-Admin-2 " type="submit" value="modifier"> ' ;                                                       
                    echo '      <input type="hidden" name="modifier" value="'.$allPet[$i]['id_pet_sitter'].'">';
                    echo '    </form>';
                    echo'   </td>';
                    echo'   <td>';
                    echo '    <a value="del" class="btn btn Bouton-Admin-2"  href="Accueil.php?action=mesAnnonces&id_pet_sitter='.$allPet[$i]['id_pet_sitter'].'&link=delPet" style="color:white; text-decoration:none;">Supprimer</a></div>';
                    echo'   </td>';
                    
                    echo' </tr>';
                            
                  }
                  echo '</table>'; 
                }else{
                  echo '<div class="col-12 text-center"><h5>Vous n\'avez pas d\'annonces Pet\'Sitter</h5></div>';
                }
                
                 ?>
              </div>

<!-- Mes annonces petSitter-->           
            <div class="row">
                <div class="col-12 mb-3 titresection  text-center">
                  <h2 class="">Topics</h2>
                </div>
            </div>
              <div class="row">
                <?php
                 
                if(count($allTopic)>0){
                  echo '<table class="table table-sm bg-nv ml-3 mr-3">';
                  for($i=0; $i<=count($allTopic)-1; $i++){ 
                    echo' <tr >';
                    echo'   <td class="align-middle " style="width:5%;">';
                    echo'   </td>';
                    echo'   <td class="align-middle " style="width:70%;">';
                    echo      '<a href="Accueil.php?action=forumArticle&id_topic='.$allTopic[$i]["id_topic"].'&categorie=2"><h3>'. $allTopic[$i]["nom_topic"].'</h3></a>'; 
       
                    echo'   </td>';
                    echo'   <td>';
                    echo'    <form action="Accueil.php?action=formForumAlimModif&idtopic='.$allTopic[$i]["id_topic"].'&categorie=2" method="post">';
                    echo'      <input Class="btn Bouton-Admin-2" type="submit" value="modifier"> ' ;                                                   
                    echo'      <input type="hidden" name="modifier" value="'.$allTopic[$i]["id_topic"].'">';
                    echo'    </form>';
                    echo'   </td>';
                    echo'   <td>';
                    echo'      <a value="del" class="btn Bouton-Admin-2"  href="Accueil.php?action=mesAnnonces&link=delTopic&id_topic='.$allTopic[$i]['id_topic'].'" style="color:white; text-decoration:none;">Supprimer</a></div>';
                    echo'   </td>';
                    
                    echo' </tr>';
                            
                  }
                  echo '</table>'; 
                }else{
                  echo '<div class="col-12 text-center"><h5>Vous n\'avez pas de topic</h5></div>';
                }
                
                 ?>
              </div>






















            </div>
		</body>
	</html>
