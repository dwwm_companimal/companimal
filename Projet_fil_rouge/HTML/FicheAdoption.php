<?php
  include_once("../SERVICE/ServiceAdoption.php");
  include_once("../SERVICE/ServiceRefuge.php");

 /* $SelectIdAdoption = new ServiceAdoption();
  $data = $SelectIdAdoption->SelectIdAdoption($_GET["id_adoption"]);*/

if(isset($_POST["Action"])){
  if($_POST["Action"] == "ModifierAdoption"){
    $ModifAdoption = new ServiceAdoption();
    $ModifAdoption-> ModifAdoption($_POST);
  }
}

$SelectIdAdoption = new ServiceAdoption();
$data = $SelectIdAdoption->SelectIdAdoption($_GET["id_adoption"]);
?>

<!DOCTYPE html>
	<html>
		<head>
			<meta charset="utf-8"/>
            <title>Fiche d'Adoption</title>
          
            <link rel="stylesheet" href="../css/bootstrap.css"/>
            <link rel="stylesheet" href="../css/style.css"/>
           
            <link rel="icon" href="../img/patteblanche.png">
           
        </head>
<body>
<!-- Fiche Adoption-->
    <div class="Fiche">
        <div class="container-fluid">
          
          <!-- Background Blanc-->
            <div class="row">
              <div class="col-8 offset-2">
                <div class="Background-Color-Fiche-Blanc">
                <div class="row ">
            <div class="col-lg-4 offset-lg-4 ">
              <h1 class="Titre"><?php echo  $data[0]["nom_animal"]; ?></h1>
              </div>
            </div>
                  
          <!-- Fin Image Fiche Animal-->
                  <div class="Background-Color-Fiche-Texte">
                      
                    <div class="row">
                      <div class="col-lg-6 mt-3">
                      
                      <br>
                      <br>
                    <p style="text-align: center"><strong>Commentaire:</strong> </p>
                    <div class="row bg-comm ">
                      <div class="col-lg-8 offset-lg-2 col-10 offset-1">
                        <p><?php echo  $data[0]["description_animal"]; ?></p>
                      </div>
                    </div>
                      </div>
                        <div class="Background-Color-Fiche-Image col-lg-5  ml-3  ">
                        <div class="row mt-5">
                        <div class="col-lg-5  col-5  ml-5 ">
                          <p><strong>Espéce:</strong> <?php echo  $data[0]["nom_espece"]; ?></p>
                          <p><strong>Race:</strong> <?php echo  $data[0]["nom_race"]; ?></p>
                       

                        <?php 
                        $Refuge = new  ServiceRefuge();
                        $data2 = $Refuge->SelectRefugeId($data[0]["id_refuge"]);

                        ?>
                        
                    
                        </div>
                        <div class="col-lg-4 col-4  ml-5">
                          <p><strong>Sexe:</strong> <?php echo  $data[0]["sexe_animal"]; ?></p>
                          <p><strong>Age:</strong> <?php echo  $data[0]["naissance_animal"]; ?> ans</p>
                        </div>
                      </div>
                      <div class="text-center">
                        <p><strong>Refuge:</strong> <?php echo  $data2[0]["nom_refuge"]; ?></p>
                      </div>
                      <img class="mb-3" width="580px" src="data:image/jpg;base64,<?php echo base64_encode($data[0]["photo_animal"])?>"></img>
                          <!-- Bouton Admin Creation/Suppresion-->
                          <div class="row justify-content-center ">
                          <?php if(isset($_SESSION['role']) && $_SESSION['role']!="Membre"){?>
                            <a href="Accueil.php?link=formAdoption&Action=ModifierAdoption&id_adoption=<?php echo $data[0]['id_adoption'] ?>"><button type="button" class="btn Bouton-Admin-1">Modifier</button>
                            <a href="Accueil.php?action=adoption&link=SupAdoption&id_adoption=<?php echo $data[0]['id_adoption'] ?>"><button type="button" class="btn Bouton-Admin-1">Supprimer</button></a>
                          <?php } ?>
                          </div>
                        </div>
                      </div>
                   
                     </div>
        
        <!-- Bouton ContactEz-Nous/information du Refuge-->
                  <div class=" row justify-content-center">
                    <?php
                        echo '<a href="Accueil.php?link=contact&action=contactAdoption&email_refuge='.$data2[0]["email_refuge"].'"><button type="button" class="btn btn-lg Bouton-Admin-2">Contactez-Nous: '.$data2[0]["email_refuge"].'</button></a>'; 
                        echo '<button  class="btn btn-lg Bouton-Admin-2">Numéro du Refuge: '. $data2[0]["telephone_refuge"] .'</button>';
                      if(isset($_GET['retour'])){
                        echo '<a href="Accueil.php"><button type="button" class="btn btn-lg Bouton-Admin-2">Retour</button></a>';
                      }else{
                        echo '<a href="Accueil.php?action=adoption"><button type="button" class="btn btn-lg Bouton-Admin-2">Retour</button></a>';
                      }?> 
                       
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
<!-- Fin Body FicheAdoption-->     
</body>
</html>
