<?php

if (!isset($_SESSION)) {
    session_start();
}
/* Recherche ID commentaire */

$data =  mysqli_query($db, "SELECT *, DATE_FORMAT(date_commentaire, '%d/%m/%Y à %H\h%i') as date_c FROM commentaire as a inner join utilisateur as b on a.id_utilisateur=b.id_utilisateur WHERE id_topic =" . $id_topic . " order by id_commentaire desc");
$data = mysqli_fetch_all($data, MYSQLI_BOTH);


/* ***************** */
?>


<!-- Cadre commentaire  -->
<?php 
    for ($j = 0; $j <= count($data) - 1; $j++) { ?>
        <div class="row col-6 offset-3 contentDisplayForumCommentaire">

        <!-- Nom et Prénom du Posteur Commentaire-->
        <div class="col-3 mt-4 Titre ">
            <h5><?php echo $data[$j]['nom_utilisateur'];
                echo "  ";
                echo $data[$j]['prenom_utilisateur'] ?></h5>
            <p><?php
                echo $data[$j]['date_c'];
            ?></p>
            <?php if ((isset($_SESSION['role']) && $_SESSION['role'] != "Membre") || (isset($_SESSION['iduser']) && $data[$j]['id_utilisateur'] == $_SESSION['iduser'])) { ?>
                <a value="delcommentaire" class="btn Bouton-Admin-1 btn-sm mt-2 mb-3" href="Accueil.php?action=forumArticle&link=delcommentaire&id_commentaire=<?php echo $data[$j][0] ?>&id_topic=<?php echo $id_topic ?>&categorie=<?php echo $_GET['categorie'] ?>" style="color:white; text-decoration:none;">Supprimer</a>
            <?php }  ?>
        </div>

        <!-- Contenu du Commentaire -->
        <div class="col-8 mt-3 aff-article2 shadow p-4 mb-3"><a style="color: black;"><?php echo $data[$j][2] ?></a></div>
        <div class="col-1"></div>
        <div class="col-8"></div>
        <div class="ml-5 col-3">

        </div>
    </div>
    <?php } ?>

<div class="row mt-5 ">
    
    <a class="col-1 btn Bouton-Admin-1 btn-sm mt-2 mx-auto " href=<?php

                                                                    if ($_GET['categorie'] == 1) {
                                                                        echo "Accueil.php?action=forumSante";
                                                                    } elseif ($_GET['categorie'] == 2) {
                                                                        echo "Accueil.php?action=forumAlimentation";
                                                                    } elseif ($_GET['categorie'] == 3) {
                                                                        echo "Accueil.php?action=forumDivertissement";
                                                                    } elseif ($_GET['categorie'] == 4) {
                                                                        echo "Accueil.php?action=forumAutre";
                                                                    }
                                                                    ?>>Retour</a>
</div>

<!-- Ne rien ecrire en dessous Barre de navigation des pages -->
<div class="row">
    <div class="mx-auto mt-2">
        <nav aria-label="Page navigation example" class="text-center">
            <ul id="paginForumCommentaire" class="pagination justify-content-center">
            </ul>  
        </nav>
    </div>
</div>