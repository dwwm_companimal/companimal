<?php
include_once('../SERVICE/ServicePerduTrouver.php');

if(isset($_POST["Action"])){
  if($_POST["Action"] == "ModifierPerduTrouver"){
    $ModifierPerduTrouver = new ServicePerduTrouver();
    $ModifierPerduTrouver-> ModifPerduTrouver($_POST);
  }
}

$SelectIdPerduTrouver = new ServicePerduTrouver();
$data = $SelectIdPerduTrouver->SelectIdPerduTrouver($_GET["id_perdu_trouve"]);

?>

<!DOCTYPE html>
	<html>
		<head>
			<meta charset="utf-8"/>
            <title>Fiche Perdu/Trouver</title>
          
            <link rel="stylesheet" href="../css/bootstrap.css"/>
            <link rel="stylesheet" href="../css/style.css"/>
           
            <link rel="icon" href="../img/patteblanche.png">
           
        </head>
<body>
<!-- Fiche Adoption-->
    <div class="Fiche">
        <div class="container-fluid">
        
          <!-- Background Blanc-->
            <div class="row">
              <div class="col-8 offset-2">
                <div class="Background-Color-Fiche-Blanc">
                <div class="row ">
            <div class="col-lg-4 offset-lg-4 ">
              <h1 class="Titre"><?php echo  $data[0]["type_perdu_trouve"]; ?></h1>
              </div>
            </div>
          <!-- Image Fiche Animal-->      
                  
          <!-- Fin Image Fiche Animal-->
                  <div class="Background-Color-Fiche-Texte">
                      
                    <div class="row ">
                      <div class="col-lg-7 mt-3">
                      <div class="row mt-5">
                        
                      </div>
                      
                    <p style="text-align: center"><strong>Commentaire:</strong> </p>
                    <div class="row bg-comm ">
                      <div class="col-lg-8 offset-lg-2 col-10 offset-1">
                        <p><?php echo  $data[0]["description_perdu_trouver"]; ?></p>
                      </div>
                    </div>
                      </div>
                        <div class="Background-Color-Fiche-Image col-lg-4 mt-3 mr-3 ">
                        <div class="col-lg-12 col-5  ">
                          <p><strong>Nom de l'Animal:</strong> <?php echo  $data[0]["nom_animal_perdu_trouve"]; ?></p>
                          <p><strong>Espéce:</strong> <?php echo  $data[0]["nom_espece"]; ?></p>                    
                          <p><strong>Ville:</strong> <?php echo  $data[0]["lieu_perdu_trouve"]; ?></p>
                          <p><strong>Code Postal:</strong> <?php echo  $data[0]["code_postale_perdu_trouve"]; ?></p>
                          <p><strong>Date: </strong><?php setlocale(LC_TIME, "fr_FR", "French"); echo strftime("%d %B %G", strtotime($data[0]["date_perdu_trouve"])); ?></p>
                        </div>
                        <img class="mb-3" width="580px" src="data:image/jpg;base64,<?php echo base64_encode($data[0]["photo_perdu"])?>"></img>
                          <!-- Bouton Admin Creation/Suppresion-->
                          <?php if((isset($_SESSION['role']) && $_SESSION['role']!="Membre")||(isset($_SESSION['iduser'])&& $data[0]['id_utilisateur']==$_SESSION['iduser'])){?>
                          <div class="row justify-content-center ">
                            <a href="Accueil.php?link=formPerduTrouve&Action=ModifierPerduTrouver&id_perdu_trouve=<?php echo $data[0]["id_perdu_trouve"]; ?>"><button type="button" class="btn Bouton-Admin-1">Modifier</button></a>
                            <a href="Accueil.php?action=perduTrouve&link=SupPerduTrouver&id_perdu_trouve=<?php echo $data[0]["id_perdu_trouve"]; ?>"><button type="button" class="btn Bouton-Admin-1">Supprimer</button></a>    
                          </div>
                          <?php }else{ ?>
                            <div class="row justify-content-center">
                            </div>
                          <?php } ?>
                        </div>
                      </div>
                   
                     </div>
        
        <!-- Bouton ContactEz-Nous/information du Refuge-->
                  <div class=" row justify-content-center">
                    <?php

                        $SelectIdUtilisateurPerduTrouver = new ServicePerduTrouver();
                        $data2 = $SelectIdUtilisateurPerduTrouver->SelectIdUtilisateurPerduTrouver($data[0]["id_utilisateur"]);

                        echo '<a href="Accueil.php?link=contact&action=contactPerduTrouve&email_perdu_trouve='.$data2[0]['email'].'"><button type="button" class="btn btn-lg Bouton-Admin-2">Contactez-Moi: '.$data2[0]['email'].'</button></a>'; 
                        echo '<button  class="btn btn-lg Bouton-Admin-2">Numéro de téléphone: '.$data2[0]['telephone_utilisateur'].'</button>';
                        if(isset($_GET['ret'])){
                        echo '<a href="Accueil.php?action=mesAnnonces"><button type="button" class="btn btn-lg Bouton-Admin-2">Retour</button></a>';
                        }elseif(isset($_GET['retour'])){
                          echo '<a href="Accueil.php"><button type="button" class="btn btn-lg Bouton-Admin-2">Retour</button></a>';
                        }else{
                          echo '<a href="Accueil.php?action=perduTrouve"><button type="button" class="btn btn-lg Bouton-Admin-2">Retour</button></a>';
                        }
                      ?> 
                       
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
<!-- Fin Body FicheAdoption-->     
</body>
</html>
