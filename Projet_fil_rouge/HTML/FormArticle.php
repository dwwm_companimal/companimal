<!DOCTYPE html>
	<html>
		<head>
			<meta charset=``utf-8`` />
            <title>Création article</title>
            <link rel="stylesheet" href="../css/bootstrap.css"/>
            <link rel="stylesheet" href="../css/style.css"/>
            <link rel="icon" href="../img/patteblanche.png">
            
    </head>
    <body>

    <?php 
      
      include_once("../SERVICE/ServiceArticle.php");
      include_once("../DAO/ArticleDAO.php");
      if(isset($_POST["editArticle"])){
        $ArticleDAO = new ArticleDAO();
        $data = $ArticleDAO -> SelectIdArticle($_POST['articleidedit']);
      }
    ?>
      <!-- header       -->
      
      <!-- fin header       -->
      
      <div class="container">
        <div class="row mt-3 ">
          <!-- titre-->
            <div class="mx-auto">
            <?php if(isset($_POST["editArticle"])){
              echo '<h1 class="Titre">Modification de l\'article</h1>';
            }else{
            echo '<h1 class="Titre">Création d\'un article</h1>';}?>
            </div>
        </div> 
        <!-- formulaire creation--> 
        <form  enctype="multipart/form-data" action=" <?php if(isset($_POST["editArticle"])){
                    echo "Accueil.php?action=fichearticle&idarticle=".$data[0]['id_article']."";
                  }else{
                    echo "Accueil.php";
                  }
                  ?>" method="POST" id="formArticle">
            
            <!-- Nom de l'article -->
            <div class="form-group row mt-2">
              <input type="hidden" class="form-control " id="idArticle" name="idArticle" value="<?php if(isset($_POST["editArticle"])){
                    echo $data[0]['id_article'];
                  }?>" required>
                <div class="col-lg-3"></div>
                <label for="TitreArticle" class="col-lg-2 col-form-label" style="font-weight:bold;">Nom de l'article :</label>
                <div class="col-lg-5 col-6">
                  <input type="text" class="form-control " id="TitreArticle" name="TitreArticle" autofocus value="<?php if(isset($_POST["editArticle"])){
                    echo $data[0]['titre_article'];
                  }?>" required>
                </div>
            </div>

             <!-- Image de l'article -->
             <?php if(!isset($_POST["editArticle"])){
              echo'     <div class="form-group row mt-2">';
              echo'     <div class="col-lg-3"></div>';
              echo'       <label for="userImage" class="col-lg-2 col-form-label" style="font-weight:bold;">Image d\'illustration : </label>';
              echo'     <div class="col-lg-5 col-6">';
              echo'       <input type="file" name="userImage" id="articleImage"/>';
              echo'     </div>';
              echo'   </div>';
                  }?>
            
            
             <!-- Détails de l'article -->
            <div class="form-group row mt-2">
              <div class="col-lg-3"></div>
              <label for="ContenuArticle" class="col-lg-2 col-form-label" style="font-weight:bold;">Détail de l'article :</label>
              <div class="col-lg-5 col-6">
                <textarea type="text" class="form-control " id="ContenuArticle" name="ContenuArticle" required rows="5"><?php if(isset($_POST["editArticle"])){
                    echo $data[0]['contenu_article'];}?> </textarea>
              </div>
             </div>  
            

            <div class="row">
              <div class="col-lg-5"></div>
              <div class="col-lg-4">
            <?php if(isset($_POST["editArticle"])){
                    echo' <button class="btn Bouton-Admin-1" name="modifierArticle" value="yes" type="submit">Valider</button>';
                  }else{
                    echo' <button class="btn Bouton-Admin-1" name="creerArticle" type="submit">Valider</button>';
                  }
                  ?>

                  <a href="Accueil.php">
                    <button type="button" class="btn Bouton-Admin-1">Retour</button>
                  </a>
              </div>
            </div>
        </form>    
      </div>

      <!-- footer       -->
   
     <!-- fin footer       -->
		</body>
	</html>
