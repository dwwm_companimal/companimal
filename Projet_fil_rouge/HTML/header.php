       <!-- header       -->
       <!-- Titre et logo-->

       <div class="container-fluid  ">
         <div class="row bg-color-white justify-content-center ">
           <div class="col-lg-6 col-md-12 text-center">
             <img src="../img/Logov3.svg" alt="" class=" img-fluid w-25 p-3 ">
           </div>
           <div class="col-lg-12 col-sm-10 text-center align-self-center">
             <h4 class="slogant">Ne me louez pas, adoptez moi!</h4>
           </div>
         </div>
       </div>
       <!-- Navbar-->
       <div class="container-fluid sticky-top">
         <div class="navfont row justify-content-center ">
           <nav class="navbar navbar-expand-lg  bg-nv  container-fluid gg  ">
             <a class="offset-xl-2 offset-md-1 navbar-brand pr-4 pt-3" href="Accueil.php">
               <h5>Comp'Animal</h5>
             </a>
             <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#responsive">
               <span class="navbar-toggler-icon"></span>
             </button>

             <div class="collapse navbar-collapse bg-nv" id="responsive">
               <ul class="navbar-nav ">
                 <li class="nav-item ">
                   <a class="nav-link" href="Accueil.php">Accueil</a>
                 </li>
                 <li class="nav-item dropdown">
                   <a class="nav-link dropdown-toggle " data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Adoption</a>
                   <div class="bg-nv dropdown-menu">
                     <a class="bg-nv dropdown-item" href="Accueil.php?action=adoption"><i class="fas fa-house-user pr-3"></i></i>Adoption</a>
                     <a class="bg-nv dropdown-item" href="Accueil.php?action=refuge"><i class="fas fa-clinic-medical pr-3"></i>Refuges</a>
                   </div>
                 </li>
                 <li class="nav-item">
                   <a class="nav-link" href="Accueil.php?action=perduTrouve">Animaux perdus et trouvés</a>
                 </li>
                 <li class="nav-item">
                   <a class="nav-link" href="Accueil.php?action=petsitter">Pet'Sitter</a>
                 </li>
                 <li class="nav-item dropdown">
                   <a class="nav-link dropdown-toggle " data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Forum</a>
                   <div class="bg-nv dropdown-menu p-1 ">
                     <a class="bg-nv dropdown-item" href="Accueil.php?action=forumSante"><i class="fas fa-heartbeat pr-3"></i> Santé</a>
                     <a class="bg-nv dropdown-item" href="Accueil.php?action=forumAlimentation"><i class="fas fa-drumstick-bite pr-3"></i>Alimentation</a>
                     <a class="bg-nv dropdown-item" href="Accueil.php?action=forumDivertissement"><i class="fas fa-baseball-ball pr-3"></i>Divertissement</a>
                     <a class="bg-nv dropdown-item" href="Accueil.php?action=forumAutre"><i class="far fa-question-circle pr-3"></i>Autre</a>
                   </div>
                 </li>
               </ul>
               <ul class="offset-xl-1 navbar-nav">
                 <?php
                  if (!isset($_SESSION['email'])) {
                    echo '
                <li class="nav-item dropdown " > 
                  <a class="nav-link dropdown-toggle  " data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Se Connecter</a>
                    <div class="bg-nv p-3  text-center dropdown-menu  " style="width:250px">
                      <form class"p-4" id="formLogin">            
                        <label for="idpersonne">Identifiant</label>
                        <input id="email2" size="30" maxlength="50" type="email" 
                              name="email" class="form-control "/>
                              <span class="feedback2"></span>
                        <label for="passpersonne " class=" mt-2">Mot de passe</label>
                        <input id="passpersonne" size="30" maxlength="30" type="password" 
                                name="motDePasse" class="form-control"/> 
                                <span class="feedback4"></span>
                                <input size="30" maxlength="30" type="hidden" 
                                name="login" class="form-control"/> 
                        <button class="btn mt-2  " type="submit" >Connecter</button>
                      </form>
                      <form id="MDPOublie" >
                        <label class="mt-3 ptpol" for="idpersonne">Mot de passe oublié ?</label>
                        <input id="email3" size="30" maxlength="30" type="email" 
                              placeholder="     Entrez votre email ici" name="mailOublier" class="form-control"/>
                              <span  class="feedback3 "></span>
                        <div class="row col-12 justify-content-center ">
                          <button class="btn mt-2 ml-4 " type="submit">Envoyer</button>
                        <div>
                      </form>                           
                    </div>
                </li>
                
                <li class="nav-item">
                  <a class="nav-link" href="Accueil.php?action=ajouter">S\'inscrire</a>
                </li>
                ';
                  }
                  if (isset($_SESSION['email'])) {
                    echo

                      '<li class="nav-item">
                  <a class="nav-link" href="accueil.php?action=deconnection">Déconnection</a>
                </li>
                <li class="nav-item dropdown" > 
                  <a class="nav-link dropdown-toggle " data-toggle="dropdown" href="#"   role="button" aria-haspopup="true" aria-expanded="false">Compte</a>
                    <div class="bg-nv dropdown-menu" >
                        <a class="bg-nv dropdown-item" href="Accueil.php?action=monCompte">Mes infos</a>
                        <a type="button" class="bg-nv dropdown-item" data-toggle="modal" data-target="#exampleModal">Changer mon mot de passe</a>
                        <a class="bg-nv dropdown-item" href="Accueil.php?action=mesAnnonces">Mes annonces</a> ';
                    if ($_SESSION["role"] != "Membre") {
                      echo
                        '<a class="bg-nv dropdown-item" href="accueil.php?action=gestionUsers">Gestion utilisateurs</a>
                        <a class="bg-nv dropdown-item" href="accueil.php?action=newsLetter">NewsLetter</a> ';
                    }
                    '</div>
                </li>';
                  }
                  ?>
               </ul>
             </div>
           </nav>
         </div>
       </div>





       <!-- Modal -->
       <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog" role="document">
           <div class="modal-content text-center">
             <div class="modal-header  ">
               <h5 class="modal-title col-12  " id="exampleModalLabel">Changer de mot de passe </h5>

             </div>
             <div class="modal-body ">
               <div class="list-group list-group-flush">
                 <form id="modifMDP">
                   <div class="list-group-item">

                     <input type="password" class="form-control" name="oldPassword" aria-describedby="emailHelp" placeholder="Ancien mot de passe">
                     <span class="feedback5"></span>
                   </div>
                   <div class="list-group-item ">

                     <input type="password" class="form-control" name="password1" id="password1" placeholder="Nouveau mot de passe">
                   </div>
                   <div class="list-group-item ">

                     <input type="password" class="form-control" name="password2" placeholder="Confirmez le mot de passe">
                     <input type="hidden" class="form-control" name="updatePassword" >
                   </div>
                   <button type="submit" class="btn Bouton-Admin-1 mt-4">Valider</button>
                   <button type="button" class="btn Bouton-Admin-1 mt-4 " data-dismiss="modal">Fermer</button>
                 </form>
               </div>
             </div>
           </div>
         </div>
       </div>