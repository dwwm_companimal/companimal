<?php
    include_once('../SERVICE/ServiceRefuge.php');

    if(isset($_POST["Action"]) && $_POST["Action"] = "ContactEmail"){                  
        $to_email = $_POST["DestinataireContact"];
        $subject = $_POST["SujetContact"];
        $body = $_POST["MessageContact"];
        $headers = "From: Companimal.@gmail.com";
        
        mail($to_email, $subject, $body, $headers);

        $to_email = $_POST["EmailContact"];
        $subject = "Confirmation d'envoie de Mail";
        $body = "Vôtre E-Mail à bien était envoyé";
        $headers = "From: Companimal.@gmail.com";
          
        mail($to_email, $subject, $body, $headers);
    }
                           
                                 
?>

<!DOCTYPE html>
<html>
<head>

            <title>Contact</title>
            <link rel="stylesheet" href="../css/bootstrap.css"/>
            <link rel="stylesheet" href="../css/style.css"/>
           
            <link rel="icon" href="../img/patteblanche.png">
            
</head>
            
  <body>
      <!-- Body Contact -->
    <div class="Background-Form">
        <div class="container-fluid">
            <!-- Titre Formulaire -->
            <div>
              <h1 class="Titre">Contactez-Nous</h1>
            </div>
            <!-- Fin Titre Formulaire-->

            <!-- Formulaire -->
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <form method="POST" id="formContact" action="Accueil.php?link=contact">
                        <!-- EMail Contact -->
                        <div class="form-group row">
                        <div class="col-lg-2"></div>
                            <label for="EmailUtilisateur" class="col-lg-3 col-form-label text-right">Vôtre Adresse E-Mail :</label>
                            <div class="col-lg-5 col-8">
                                <?php
                                    if(isset($_SESSION['iduser'])){
                                        echo '<input type="text" class="form-control" id="EmailUtilisateur" name="EmailContact"  style="pointer-events:none"  value="'.$_SESSION['email'].'"required>';
                                    }else{
                                        echo '<input type="text" class="form-control" id="EmailUtilisateur" name="EmailContact" required>';
                                    }
                                ?>
                            </div>
                        </div>
                        <!-- Sujet Contact-->
                        <div class="form-group row">
                        <div class="col-lg-2"></div>
                            <label for="DestinataireContact" class="col-lg-3 col-form-label text-right">Destinataire :</label>
                            <div class="col-lg-5 col-8">
                                <?php
                                if(isset($_GET["action"]) && $_GET["action"] == "contactRefuge"){
                                    echo '<input type="text" class="form-control" id="DestinataireContact" name="DestinataireContact" style="pointer-events:none" required value="'.$_GET["email_refuge"].'">';
                                }elseif(isset($_GET["action"]) && $_GET["action"] == "contactAdoption"){
                                    echo '<input type="text" class="form-control" id="DestinataireContact" name="DestinataireContact" style="pointer-events:none" required value="'.$_GET["email_refuge"].'">';
                                }elseif(isset($_GET["action"]) && $_GET["action"] == "contactPerduTrouve"){
                                    echo '<input type="text" class="form-control" id="DestinataireContact" name="DestinataireContact" style="pointer-events:none" required value="'.$_GET["email_perdu_trouve"].'">';
                                }elseif(isset($_GET["action"]) && $_GET["action"] == "contactPetSitter"){
                                    echo '<input type="text" class="form-control" id="DestinataireContact" name="DestinataireContact" style="pointer-events:none" required value="'.$_GET["email_petsitter"].'">';
                                }else{
                                    echo '<select class="form-control" id="DestinataireContact" name="DestinataireContact" required>';

                                    $Refuge = new ServiceRefuge();
                                    $data = $Refuge-> SelectAllRefuge();
                                
                                    for($i=0; $i < count($data); $i++){
                                        echo '<option value="'.$data[$i]["email_refuge"].'">'.$data[$i]["email_refuge"].'</option>';
                                    }
                                }
                                    echo '</select>';
                              ?>
                            </div>
                        </div>
                        <!-- Sujet Contact-->
                        <div class="form-group row">
                        <div class="col-lg-2"></div>
                            <label for="SujetContact" class="col-lg-3 col-form-label text-right">Sujet :</label>
                            <div class="col-lg-5 col-8">
                                <?php
                                    if(isset($_GET["action"]) && $_GET["action"] == "contactPerduTrouve"){
                                        echo '<select class="form-control" id="SujetContact" name="SujetContact" required>';
                                            echo "<option selected>J'ai retrouver vôtre Animal</option>";
                                            echo "<option>J'ai aperçu vôtre Animal</option>";
                                            echo "<option>Vôtre Animal ce trouve dans un Refuge</option>";
                                        echo '</select>';
                                    }elseif(isset($_GET["action"]) && $_GET["action"] == "contactPetSitter"){
                                        echo '<select class="form-control" id="SujetContact" name="SujetContact" required>';
                                        echo "<option selected>Je souhaiterais louer vos service</option>";
                                        echo "<option>Information suplémentaire</option>";
                                    echo '</select>';
                                    }else{
                                        echo '<select class="form-control" id="SujetContact" name="SujetContact" required>';
                                            echo "<option selected>Qu'elle est vôtre questions ?</option>
                                            <option>Comment Aider le refuge ?</option>
                                            <option>Comment Adopter ?</option>
                                            <option>Qu'elle Alimentation Choisir pour mon Animal ?</option>
                                            <option>Autre</option>
                                        </select>";
                                    }
                                ?>
                            </div>
                        </div>
                        <!-- Message Contact -->
                        <div class="form-group row">
                        <div class="col-lg-2"></div>
                            <label for="MessageContact" class="col-lg-3 col-form-label text-right">Votre Message :</label>
                            <div class="col-lg-5 col-8">
                                <textarea class="form-control" id="MessageContact" name="MessageContact" rows="6" required></textarea>
                            </div>
                        </div>
                        <!-- Bouton envoi formulaire contact-->
                        <div class="row">
                            <div class="col-lg-5"></div>
                            <div class="col-lg-4">

                                <input type="hidden" name="Action" value="ContactEmail">
                                <button type="submit" class="btn Bouton-Admin-1">Envoyer</button>


                                <a href="<?php 
                                    if(isset($_GET["action"]) && $_GET["action"] == "contactRefuge"){
                                        echo "Accueil.php?action=refuge";
                                    }elseif(isset($_GET["action"]) && $_GET["action"] == "contactAdoption"){
                                        echo "Accueil.php?action=adoption";
                                    }elseif(isset($_GET["action"]) && $_GET["action"] == "contactPerduTrouve"){
                                        echo "Accueil.php?action=perduTrouve";
                                    }elseif(isset($_GET["action"]) && $_GET["action"] == "contactPetSitter"){
                                        echo 'Accueil.php?action=petsitter';
                                    }else{
                                        echo 'Accueil.php';
                                    }
                                    ?>">
                                    <button type="button" class="btn Bouton-Admin-1">Retour</button>
                                </a>
                            </div>
                        </div>
                    </form> 
                <!-- Fin Formulaire-->   
                </div>
            </div>
        </div>
    </div>
    <!-- Fin Body Contact-->
</body>

</html>