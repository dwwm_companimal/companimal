<!DOCTYPE html>
	<html>
		<head>
			<meta charset=``utf-8`` />
            <title>Inscription</title>
            <link rel="stylesheet" href="../css/bootstrap.css"/>
            <link rel="stylesheet" href="../css/style.css"/>
            <link rel="icon" href="../img/patteblanche.png">
          
    </head>
    <body>
    <?php 
      
      include_once("../SERVICE/ServiceUtilisateur.php");

        if(isset($_SESSION['email'])){
        include_once("../DAO/UtilisateurDAO.php");
        $email = $_SESSION['email'];
        $UserDAO = new UtilisateurDAO();
        $data = $UserDAO -> selectWhereEgale($email);
        }
    ?> 
    <!-- header       -->
   
      <!-- fin header       -->


      <div class="container">
        <div>
          <div class="row">
            <div class="mx-auto mt-3 mb-3 ">
            <?php if(isset($_GET["action"]) && ($_GET["action"]=="monCompte")){
              echo '<h1 class="Titre">Mes Informations</h1>';
            }else{
              echo '<h1 class="Titre">Inscription</h1>';}?>
            </div>
          </div>
        </div>
        <!-- formulaire inscrition-->
        <div class="row ">
          <div class="col-lg-8 offset-lg-2">

        <form  action="accueil.php" method="post" id="formInscription">     
            <div class="form-group row">
            <input type="hidden" class="form-control " id="inputid" name="idUser"  value="
                  <?php if(isset($_GET["action"]) && ($_GET["action"]=="monCompte")){
                    echo $data[0]['id_utilisateur'];
                  }?>" required>
              <div class="col-lg-2"></div>

                <label  for="nom" class="col-lg-2 col-form-label" style="font-weight:bold;"  >Nom : </label>
                <div class="col-lg-6 col-7">
                  <input type="text" class="form-control nom" id="nom" name="nom" autofocus value="<?php
                   if(isset($_GET["action"]) && ($_GET["action"]=="monCompte")){echo $data[0]['nom_utilisateur'];
                  }?>" required>
                </div>
            </div>


            <div class="form-group row">
              <div class="col-lg-2"></div>
                <label for="prenom"  class="col-lg-2 col-form-label" style="font-weight:bold;">Prénom : </label>
                <div class="col-lg-6 col-7">
                  <input type="text" class="form-control " id="prenom" name="prenom" value="<?php
                   if(isset($_GET["action"]) && ($_GET["action"]=="monCompte")){
                    echo $data[0]['prenom_utilisateur'];
                  }?>" required>
                </div>
            </div>

            <div class="form-group row">
              <div class="col-lg-2"></div>
                <label for="adresse" class="col-lg-2 col-form-label" style="font-weight:bold;">Adresse :</label>
                <div class="col-lg-6 col-8">
                  <input type="text" class="form-control " id="adresse" name="adresse" value="<?php
                   if(isset($_GET["action"]) && ($_GET["action"]=="monCompte")){
                    echo $data[0]['adresse_utilisateur'];
                  }?>" >
                </div>
            </div>

            <div class="form-group row">
                  <div class="col-lg-2"></div>
                <label for="ville" class="col-lg-2 col-form-label" style="font-weight:bold;">Ville :</label>
                <div class="col-lg-6 col-8">
                  <input type="text" class="form-control " id="ville" name="ville" value="<?php
                  if(isset($_GET["action"]) && ($_GET["action"]=="monCompte")){
                    echo $data[0]['ville_utilisateur'];
                  }?>" >
                </div>
            </div>

            <div class="form-group row">
              <div class="col-lg-2"></div>
                <label for="codePostal" class="col-lg-3 col-form-label" style="font-weight:bold;">Code postal :</label>
                <div class="col-lg-5 col-6">
                  <input type="text" class="form-control " id="codePostal" name="codePostal" value="<?php
                   if(isset($_GET["action"]) && ($_GET["action"]=="monCompte")){
                    echo $data[0]['code_postal_utilisateur'];
                  }?>" >
                </div>
            </div>


            <div class="form-group row ">
              <div class="col-lg-2"></div>
                <label for="tel" class="col-lg-3 col-form-label" style="font-weight:bold;">Téléphone :</label>
                <div class="col-lg-5 col-6">
                  <input type="text" class="form-control " id="tel" name="tel" value="<?php
                   if(isset($_GET["action"]) && ($_GET["action"]=="monCompte")){
                    echo $data[0]['telephone_utilisateur'];
                  }?>" required>
                </div>
            </div>

            <div class="form-group row">
              <div class="col-lg-2"></div>
                <label for="email" class="col-lg-3 col-form-label" style="font-weight:bold;">Adresse Email :</label>
                <div class="col-lg-5 col-7">
                  <input type="email" class="form-control " id="email" name="email" value="<?php
                   if(isset($_GET["action"]) && ($_GET["action"]=="monCompte")){
                    echo $data[0]['email'];
                  }?>" required>
                  <span class="feedback"></span>

                </div>
            </div>

             <?php if(!isset($_SESSION["email"])){
                echo     
            '<div class="form-group row">
              <div class="col-lg-2"></div>
                <label for="motDePasse" class="col-lg-3 col-form-label" style="font-weight:bold;">Mot de passe : </label>
                <div class="col-lg-5 col-6">
                  <input type="password" class="form-control " id="motDePasse" name="motDePasse" value="" required>
                </div>
            </div>

            <div class="form-group row">
              <div class="col-lg-2"></div>
                <label for="motDePasse2" class="col-lg-3 col-form-label" style="font-weight:bold;">Confirmer mot de passe : </label>
                <div class="col-lg-5 col-7">
                  <input type="password" class="form-control " id="motDePasse2" name="motDePasse2" value="" required>
                </div>
            </div>';}
            if(isset($_GET["erreur"]) && ($_GET["erreur"]=="mdpdif")){
               echo'<div class=" col-6 offset-3 mb-3 text-center text-danger"> Les 2 mots de passes sont différents</div>';
             }
             if(isset($_GET["erreur"]) && ($_GET["erreur"]=="existe")){
              echo'<div class=" col-6 offset-3 mb-3 text-center text-danger"> Le compte existe déja</div>';
            }
             
             ?>

            <div class="row">
              <div class="col-lg-5"></div>
              <?php if(!isset($_SESSION["email"])){
                echo
                  '<div class="col">
               <input type="hidden" name="inscrire" >
                <button class="btn Bouton-Admin-1" type="submit" ">Valider</button>
                <button class="btn Bouton-Admin-1"><a href="Accueil.php">Retour</a></button>
              </div>';}?>
              <?php if(isset($_SESSION["email"])){
                echo
              '<div class="col">
                <button class="btn Bouton-Admin-1 " type="submit" name="edit">Modifier</button>
                <button class="btn Bouton-Admin-1"><a href="Accueil.php">Retour</a></button>
              </div>';}?>
              
            </div>

        </form>
        
        <div>
      </div>    
    </div>
  </div>
</div>
      

      <!-- footer       -->
     
     <!-- fin footer       -->
		</body>
	</html>
