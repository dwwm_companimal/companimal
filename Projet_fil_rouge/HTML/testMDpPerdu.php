<?php
include_once('../SERVICE/ServiceUtilisateur.php');
$ServiceUtilisateur = new ServiceUtilisateur;

	if (isset($_POST["mailOublier"])) {
		$data = $ServiceUtilisateur->SelectAll();
		$rep = false;
		for ($i = 0; $i <= count($data) - 1; $i++) {
			
			if ($data[$i]["email"] == $_POST["mailOublier"]) {
				$rep = true;
			}
		}

		if ($rep == true) {
			$newMDP = $ServiceUtilisateur->MDPEmail($_POST["mailOublier"]);

			$to_email = $_POST["mailOublier"];
			$subject = "Mot de passe tomporaire :";
			$body ="Voici votre mot de passe temporaire : ". $newMDP." Pensez à le modifier!";
			$headers = "From: Companimal.@gmail.com";

			mail($to_email, $subject, $body, $headers);
			$jse = json_encode("success");
			echo $jse;

		} else {
			$jse = json_encode(array("error" => array(
				"code" => 890,
				"message" => "Email non enregistré"
			)), JSON_UNESCAPED_UNICODE);
			echo $jse;
		}
	}