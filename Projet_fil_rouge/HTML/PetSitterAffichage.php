
<?php

    include_once('../SERVICE/ServicePetSitter.php');
        if(!isset($_SESSION)){
            session_start();
        }

        if(
            ( isset($_POST["type_garde_pet_sitter"]) && $_POST["type_garde_pet_sitter"] != "" )
            OR 
            ( isset($_POST["distance_pet_sitter"]) && $_POST["distance_pet_sitter"] != "" )
            OR 
            ( isset($_POST["type_domicile_pet_sitter"]) && $_POST["type_domicile_pet_sitter"] != "" )
            OR 
            ( isset($_POST["nom_espece"]) && $_POST["nom_espece"] != "" )
        ){

            $SelectPetSitterRecherche = new ServicePetSitter();
            $data = $SelectPetSitterRecherche->SelectPetSitterRecherche($_POST);

        }else{
            
            $SelectAllPetSitter = new ServicePetSitter();
            $data = $SelectAllPetSitter->SelectAllPetSitter();
            
        }



        if(count($data)>0){

            for($i=0; $i<=count($data)-1; $i++){
                
            echo '<div class="row justify-content-center mb-3 contentDisplayPetSitter">
                    <div class="col-lg-4">
                            
                            <a href="Accueil.php?action=fichePetsiter&idPet=' . $data[$i]['id_pet_sitter'] . '"><img class="img-article2 shadow" width="280px" src="data:image/jpg;base64,' . base64_encode($data[$i]['photo_pet_sitter']) . '" alt=""></a>
                        </div>

                        <div class="col-lg-8 text-left Titre2 aff-article2 shadow ">
                            <h4>' . $data[$i]['titre_pet_sitter'] . '</h4>
                        <p>Localité:  ' . $data[$i]["distance_pet_sitter"] . '    </br>      Type de garde:  ' . $data[$i]["type_garde_pet_sitter"] . ' </p>
                                
                        </div>';
                    

                        if((isset($_SESSION['role']) && $_SESSION['role']!="Membre")||(isset($_SESSION['iduser'])&& $data[$i]['id_utilisateur']==$_SESSION['iduser'])){ 
                        echo '<div class="col-lg-12 align-self-center mr-5">
                                <div class="row justify-content-end">  
                                    <form action="Accueil.php?action=modifpetsiter" method="post">
                                        <input Class="btn Bouton-Admin-1 " type="submit" value="modifier">                                                        
                                        <input type="hidden" name="modifier" value="'.$data[$i]['id_pet_sitter'].'">
                                        <a value="del" class="btn btn Bouton-Admin-1"  href="Accueil.php?action=petsitter&id_pet_sitter='.$data[$i]['id_pet_sitter'].'&link=del" style="color:white; text-decoration:none;">Supprimer</a>
                                    </form>
                                </div>
                            </div>';
                        }

                    echo'     
                </div>';
                }
            }else{
                echo "<div style='text-align:center;'>
                        <p> Nous n'avons trouvé aucun résultat </p>
                      </div>";
              }
    ?>
       

        <div class="row">
            <div class="mx-auto mt-2" >
                <nav aria-label="Page navigation example" class="text-center">
                    <ul id="paginPetSitter" class="pagination justify-content-center">
                    </ul>
                </nav>
            </div>
        
