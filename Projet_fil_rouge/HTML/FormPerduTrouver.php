<?php

    include_once('../SERVICE/ServiceEspeceRace.php');
    include_once('../SERVICE/ServicePerduTrouver.php');

    if(isset($_GET["Action"]) && $_GET["Action"] == "ModifierPerduTrouver"){

        $SelectIdPerduTrouver = new ServicePerduTrouver();
        $data = $SelectIdPerduTrouver->SelectIdPerduTrouver($_GET["id_perdu_trouve"]);
    }
?>

<!DOCTYPE html>
	<html>
		<head>
			<meta charset=``utf-8`` />
            <title>Formulaire Perdu Trouver</title>
            <link rel="stylesheet" href="../css/bootstrap.css"/>
            <link rel="stylesheet" href="../css/style.css"/>
           
            <link rel="icon" href="../img/patteblanche.png">
           
    </head>
    <body>
        <!-- Debut Body Formulaire Perdu Trouver-->
       <div class="Background-Form">
        <div class="container-fluid">
            <!-- Titre Formulaire Perdu Trouver-->
            <div>
                <div class="row">
                  <div class="mx-auto ">
                  <h1 class="Titre"><?php 
                  
                  if(isset($_GET["Action"]) && $_GET["Action"] == "ModifierPerduTrouver"){
                      echo 'Modifier Perdu/Trouvé';
                  }else{
                    echo 'Ajout Perdu/Trouvé';
                  }?></h1>
                  </div>
                </div>
              </div>



            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <!-- Formulaire-->
                    <form method="POST" id="formPerduTrouve" enctype="multipart/form-data" action="<?php
                        if(isset($_GET["Action"]) && $_GET["Action"] == "ModifierPerduTrouver"){ 
                            echo "Accueil.php?Action=fichePerduTrouve&id_perdu_trouve=".$data[0]["id_perdu_trouve"]."";
                            
                        }else{
                            echo "Accueil.php?action=perduTrouve";
                        } 
                        ?>">
                        <div class="form-group row">
                            <div class="col-lg-3"></div>
                            <!-- Tyoe d'Annonce-->
                            <label for="TypeAnnoncePT" class="col-lg-2 col-form-label" style="font-weight:bold;">Type d'Annonce : </label>
                            <div class="col-lg-4 col-6 puces">
                                <!-- Radio Perdu Trouver-->
                            <?php
                            if(isset($_GET["Action"]) && $_GET["Action"] == "ModifierPerduTrouver"){
                                if($data[0]["type_perdu_trouve"] == "Perdu"){
                                    echo '<div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="TypeAnnoncePT" id="Perdu" value="Perdu" Required checked>
                                        <label class="form-check-label" for="Perdu">Perdu</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="TypeAnnoncePT" id="Trouver" value="Trouve">
                                        <label class="form-check-label" for="Trouver">Trouve</label>
                                    </div>';
                                }else{
                                    echo '<div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="TypeAnnoncePT" id="Perdu" value="Perdu" Required>
                                        <label class="form-check-label" for="Perdu">Perdu</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="TypeAnnoncePT" id="Trouver" value="Trouve" checked>
                                        <label class="form-check-label" for="Trouver">Trouve</label>
                                    </div>';
                                }
                            }else{
                                echo '<div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="TypeAnnoncePT" id="Perdu" value="Perdu" Required>
                                    <label class="form-check-label" for="Perdu">Perdu</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="TypeAnnoncePT" id="Trouver" value="Trouve">
                                    <label class="form-check-label" for="Trouver">Trouve</label>
                                </div>';
                                }
                            ?>   
                            </div>
                        </div>
                        <!-- Nom de l'annonce-->
                        <div class="form-group row">
                            <div class="col-lg-3"></div>
                            <label for="NomAnnoncePT" class="col-lg-2 col-form-label" style="font-weight:bold;">Nom de l'Annonce :</label>
                            <div class="col-lg-4 col-6">
                              <input type="text" class="form-control" id="NomAnnoncePT" name="NomAnnoncePT" Required autofocus
                              value="<?php 
                              if(isset($_GET["Action"]) && $_GET["Action"] == "ModifierPerduTrouver"){
                                echo $data[0]["titre_perdu_trouve"];
                              }
                              ?>">
                            </div>
                        </div>
                        <!-- Nom de l'Animal-->
                        <div class="form-group row">
                            <div class="col-lg-3"></div>
                            <label for="NomAnimalPT" class="col-lg-2 col-form-label" style="font-weight:bold;">Nom de l'Animal :</label>
                            <div class="col-lg-4 col-6">
                              <input type="text" class="form-control" id="NomAnimalPT" name="NomAnimalPT"
                              value="<?php 
                              if(isset($_GET["Action"]) && $_GET["Action"] == "ModifierPerduTrouver"){
                                echo $data[0]["nom_animal_perdu_trouve"];
                              }
                              ?>">
                            </div>
                        </div>
                        <!-- Liste déroulante Espece-->
                        <div class="form-group row">
                            <div class="col-lg-3"></div>
                            <label for="EspeceAnimalPT" class="col-lg-2 col-form-label" style="font-weight:bold;">Espéce :</label>
                            <div class="col-lg-4 col-6">
                                <select class="form-control" id="EspeceAnimalPT" name="EspeceAnimalPT" Required>
                                <?php
                                    if(isset($_GET["Action"]) && $_GET["Action"] == "ModifierPerduTrouver"){
                                       echo '<option selected>'.$data[0]["nom_espece"].'</option>';
                                    }else{
                                        echo '<option selected value="">Selectionner une Espece</option>';
                                    }
                                
                                
                                    $Espece = new ServiceEspeceRace();
                                    $data = $Espece-> SelectEspece();
                                
                                    for($i=0; $i < count($data); $i++){
                                        echo '<option>'.$data[$i]["nom_espece"].'</option>';
                                    }

                                ?>
                                </select>
                            </div>
                        </div>
                        <!-- Télécharger Photo-->
                        <?php 
                              if(isset($_GET["Action"]) && $_GET["Action"] !== "ModifierPerduTrouver"){

                              echo'  <div class="form-group row">';
                              echo'       <div class="col-lg-3"></div>';
                              echo'       <label class="col-lg-3 col-form-label"><strong>Télécharger une Photo :</strong></label>';
                              echo'       <div class="col-lg-4 col-6">';
                              echo'       <input type="file" name="userImage"/>';
                              echo'      </div>';
                              echo'    </div>'; 
                              }
                              ?>
                        
                                             
                        <!-- Lieu-->
                        <div class="form-group row">
                            <div class="col-lg-3"></div>
                            <label for="LieuPT" class="col-lg-2 col-form-label" style="font-weight:bold;">Ville :</label>
                            <div class="col-lg-4 col-6">
                              <input type="text" class="form-control" id="LieuPT" name="LieuPT" Required
                              value="<?php 
                              if(isset($_GET["Action"]) && $_GET["Action"] == "ModifierPerduTrouver"){

                                $SelectIdPerduTrouver = new ServicePerduTrouver();
                                $data = $SelectIdPerduTrouver->SelectIdPerduTrouver($_GET["id_perdu_trouve"]);

                                echo $data[0]["lieu_perdu_trouve"];
                              }
                              ?>">
                            </div>
                        </div>
                        <!-- Lieu-->
                        <div class="form-group row">
                            <div class="col-lg-3"></div>
                            <label for="CodePostalPT" class="col-lg-2 col-form-label" style="font-weight:bold;">Code Postale :</label>
                            <div class="col-lg-4 col-6">
                              <input type="number" class="form-control" id="CodePostalPT" name="CodePostalPT" Required
                              value="<?php 
                              if(isset($_GET["Action"]) && $_GET["Action"] == "ModifierPerduTrouver"){
                                echo $data[0]["code_postale_perdu_trouve"];
                              }
                              ?>">
                            </div>
                        </div>
                        <!-- Desciption-->
                        <div class="form-group row">
                            <div class="col-lg-3"></div>
                            <label for="DescriptionPT" class="col-lg-2 col-form-label" style="font-weight:bold;">Description :</label>
                            <div class="col-lg-4 col-6">
                                <textarea class="form-control" id="DescriptionPT" name="DescriptionPT" rows="6"><?php 
                              if(isset($_GET["Action"]) && $_GET["Action"] == "ModifierPerduTrouver"){
                                echo $data[0]["description_perdu_trouver"];
                              }
                              ?></textarea>
                            </div>
                        </div>

                        <?php
                            if(isset($_GET["Action"]) && $_GET["Action"] == "ModifierPerduTrouver"){

                                echo '<input type="hidden" name="id_perdu_trouve" value="'.$data[0]["id_perdu_trouve"].'">';
                            }
                        ?>
                        <!-- bouton de validation -->
                        <div class="row">
                            <div class="col-lg-5"></div>
                            <div class="col-lg-4">
                                <input type="hidden" name="Action" value="<?php if(isset($_GET["Action"]) && $_GET["Action"] == "ModifierPerduTrouver"){echo 'ModifierPerduTrouver';}else{ echo 'AjoutPerduTrouver';}?>">
                                <button type="submit" class="btn Bouton-Admin-1">Valider</button>
                                <a href="Accueil.php?action=perduTrouve"><button type="button" class="btn Bouton-Admin-1 ">Retour</button></a>
                                </a>
                            </div>
                        </div>
                    </form>
                    <!-- Fin Formulaire-->
                </div>
            </div>
        </div>
    </div>
    <!-- Fin Body Formulaire Perdu Trouver-->
</body>
	</html>
