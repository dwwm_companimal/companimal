<?php

    include_once("../DAO/PerduTrouverDAO.php");
    include_once("../MODEL/PerduTrouve.php");

    class ServicePerduTrouver{

        private $PerduTrouver;

        public function __construct(){

            $this->PerduTrouver = new PerduTrouverDAO();
        }

        /* Permet l'ajout d'un fiche */

        public function AjoutPerduTrouver($post){

            $PerduTrouver = new PerduTrouve();

            $PerduTrouver->setTypePerduTrouve($post["TypeAnnoncePT"]);
            $PerduTrouver->setTitrePerduTrouve($post["NomAnnoncePT"]);
            $PerduTrouver->setNomEspece($post["EspeceAnimalPT"]);
            $PerduTrouver->setNomAnimalPerduTrouve($post["NomAnimalPT"]);
            $PerduTrouver->setLieuPerduTrouve($post["LieuPT"]);
            $PerduTrouver->setCodePostalPerduTrouve($post["CodePostalPT"]);
            $PerduTrouver->setDescriptionPerduTrouver($post["DescriptionPT"]);

            $this->PerduTrouver->AjoutPerduTrouver($PerduTrouver);
        }

        /* Permet de modifier une fiche */

        public function ModifPerduTrouver($post){

            $PerduTrouver = new PerduTrouve();

            $PerduTrouver->setTypePerduTrouve($post["TypeAnnoncePT"]);
            $PerduTrouver->setTitrePerduTrouve($post["NomAnnoncePT"]);
            $PerduTrouver->setNomEspece($post["EspeceAnimalPT"]);
            $PerduTrouver->setNomAnimalPerduTrouve($post["NomAnimalPT"]);
            $PerduTrouver->setLieuPerduTrouve($post["LieuPT"]);
            $PerduTrouver->setCodePostalPerduTrouve($post["CodePostalPT"]);
            $PerduTrouver->setDescriptionPerduTrouver($post["DescriptionPT"]);
            $PerduTrouver->setIdPerduTrouve($post["id_perdu_trouve"]);

            $this->PerduTrouver->ModifPerduTrouver($PerduTrouver);
        }

        /* Permet de recupere tous les information PerduTrouver */

        public function SelectAllPerduTrouver(){

            $data = $this->PerduTrouver->SelectAllPerduTrouver();
            return $data;
        }

        /* Permet de recupere les Différent Type PerduTrouve */

        public function SelectTypePerduTrouver(){

            $data = $this->PerduTrouver->SelectTypePerduTrouver();
            return $data;
        }

        /* Select toute les information en fonction de son Id_perdu_trouve */

        public function SelectIdPerduTrouver($get){

            $data = $this->PerduTrouver->SelectIdPerduTrouver($get);
            return $data;
        }

            /* Permet de supprimer une fiche */

        public function SupPerduTrouver($get){

            $data = $this->PerduTrouver->SupPerduTrouver($get);
        }

        /* Recupere le telephone et l'email de l'utilisateur  */
        
        public function SelectIdUtilisateurPerduTrouver($get){

            $data2 = $this->PerduTrouver->SelectIdUtilisateurPerduTrouver($get);
            return $data2;
        }

        /* Permet d'avoir le nombre d'element par recherche */

        public function SelectPerduTrouveRecherche($post){

            $count=0;
            foreach($post as $key=>$val){
                if($val != ""){
                    $count++;
                    $Tab[$key] = $post[$key];  
                }
            }

            if($count == 0){
                $data = $this->PerduTrouver->SelectAllPerduTrouver();
                return $data;
            }else{
                $data = $this->PerduTrouver->SelectPerduTrouveRecherche($Tab);
                return $data; 
            }
        }

    }

        
?>