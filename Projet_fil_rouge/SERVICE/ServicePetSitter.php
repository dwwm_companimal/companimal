<?php
include_once ('..\DAO\PetSitterDAO.php');
include_once ('..\MODEL\PetSitter.php');
include_once ('..\DAO\BDDConnexionDAO.php');

Class ServicePetsitter extends BDDConnexionDAO{

    private $PetSitter;

    public function __construct(){

        $this->PetSitter = new PetSitterDao();
    }

            /* Ajout */

    Public function ajoutPetsitter($data){

        $PetSitter = new PetSitter();

        $PetSitter-> setTitrePetSitter($data['NomAnnonceFormPetSitter']);
        $PetSitter-> setDistancePetSitter($data['CodePostalPetSitter']);
        $PetSitter-> setTypeGardePetSitter($data['Garde']);
        $PetSitter-> setTypeDomicilePetSitter($data['Domicile']);
        $PetSitter-> setPresentationTelephone($data['afftel']);
        $PetSitter-> setDescriptionPetSitter($data['InfoCompPetSitter']);
        $PetSitter-> setIdEspece($data['EspecePetSitter']);

        $this->PetSitter->ajoutAnnoncePetSitter($PetSitter);
    }

                /* Modifier */

    public function modify($data){

        $PetSitter = new PetSitter();

        $PetSitter -> setIdPetSitter($data['idpet']);
        $PetSitter-> setTitrePetSitter($data['NomAnnonceFormPetSitter']);
        $PetSitter-> setDistancePetSitter($data['CodePostalPetSitter']);
        $PetSitter-> setTypeGardePetSitter($data['Garde']);
        $PetSitter-> setTypeDomicilePetSitter($data['Domicile']);
        $PetSitter-> setPresentationTelephone($data['afftel']);
        $PetSitter-> setDescriptionPetSitter($data['InfoCompPetSitter']);
        $PetSitter-> setIdEspece($data['EspecePetSitter']);

        $this->PetSitter->modifyAnnonce($PetSitter);   
    }

    /* Select Tout les information Pet Sitter */

    public function SelectAllPetSitter(){

        $data = $this->PetSitter->SelectAllPetSitter();
        return $data;
    }

            /* Sup PetSitter */

    public function SupPetSitter($get){

        $this->PetSitter->SupPetSitter($get);
    }

            /* Select Garde PetSitter */

    public function SelectGardePetSitter(){

        $data = $this->PetSitter->SelectGardePetSitter();
        return $data;
    }

    /* Select Domicile PetSitter */

    public function SelectDomicilePetSitter(){

        $data = $this->PetSitter->SelectDomicilePetSitter();
        return $data;
    }


    /* Select Recherche Pet Sitter si POST */

    public function SelectPetSitterRecherche($post){

        $count=0;

        foreach($post as $key=>$val){
            if($val != ""){
                $count++;
                $Tab[$key] = $post[$key];  
            }
        }

        if($count == 0){
            $data = $this->PetSitter->SelectAllPetSitter();
            return $data;
        }else{
            $data = $this->PetSitter->SelectPetSitterRecherche($Tab);
            return $data;
        }
    }


}

?>