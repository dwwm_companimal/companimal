<?php

    include_once('../DAO/AdoptionDAO.php');
    include_once('../MODEL/Adoption.php');

    class ServiceAdoption{

        private $Adoption;

        public function __construct(){

            $this->Adoption = new AdoptionDao();
        }

        public function AjoutAdoption($post){

            $Adoption = new Adoption();

            $Adoption->SetNom_animal($post["NomAnimal"]);
            $Adoption->SetNom_espece($post["EspeceAnimal"]);  
            $Adoption->SetNom_race($post["RaceAnimal"]);
            $Adoption->SetSexe_animal($post["SexeAnimal"]);
            $Adoption->SetNaissance_animal($post["AgeAnimal"]);
            $Adoption->SetDescription_animal($post["DescriptionAnimal"]);       
            $Adoption->setId_refuge($post["RefugeAnimal"]);

            $this->Adoption->AjoutAdoption($Adoption);
        }

        public function ModifAdoption($post){

            $Adoption = new Adoption();

            $Adoption->SetNom_animal($post["NomAnimal"]);
            $Adoption->SetNom_espece($post["EspeceAnimal"]);  
            $Adoption->SetNom_race($post["RaceAnimal"]);
            $Adoption->SetSexe_animal($post["SexeAnimal"]);
            $Adoption->SetNaissance_animal($post["AgeAnimal"]);
            $Adoption->SetDescription_animal($post["DescriptionAnimal"]);       
            $Adoption->setId_refuge($post["RefugeAnimal"]);
            $Adoption->setId_adoption($post["Id_Adoption"]);

            $this->Adoption->ModifAdoption($Adoption);
        }

        public function SupAdoption($get){

            $this->Adoption->SupAdoption($get);
        }


        public function SelectAllAdoption(){

            $data = $this->Adoption->SelectAllAdoption();
            return $data;
        }

        public function SelectIdAdoption($get){

            $data = $this->Adoption->SelectIdAdoption($get);
            return $data;
        }

        public function SelectSexeAdoption(){

            $data = $this->Adoption->SelectSexeAdoption();
            return $data;
        }

        /* Permet d'avoir le nombre d'element par recherche */

        public function SelectAdoptionRecherche($post){

            $count=0;
            
            foreach($post as $key=>$val){
                if($val != ""){
                    $count++;
                    $Tab[$key] = $post[$key];  
                }
            }

            if($count == 0){
                $data = $this->Adoption->SelectAllAdoption();
                return $data;
            }else{
                $data = $this->Adoption->SelectAdoptionRecherche($Tab);
                return $data;         
            }
        }

    }

?>