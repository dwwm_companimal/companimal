<?php

    include_once('../DAO/NewsLetterDAO.php');
    include_once('../MODEL/NewsLetter.php');

    class ServiceNewsLetter{

        private $NewsLetter;

        public function __construct(){

            $this->NewsLetter = new NewsLetterDAO();

        }

        public function SelectAllMailNws(){

            $data = $this->NewsLetter->SelectAllMailNws();
            return $data;
        }

        public function SelectWhereAllMailNws($post){

            $data = $this->NewsLetter->SelectWhereAllMailNws($post["EmailNws"]);
            return $data;
        }

        public function AjoutMailNws($post){

           $NewsLetter = new NewsLetter();

           $NewsLetter->setEmail($post["EmailNws"]);

           $this->NewsLetter->AjoutMailNws($NewsLetter);
        }

        public function DeletNws($DeletNws){

            $this->NewsLetter->DeletNws($DeletNws);
        }


    }