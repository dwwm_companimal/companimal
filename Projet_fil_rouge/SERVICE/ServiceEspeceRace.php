<?php

include_once("../DAO/EspeceRaceDao.php");
include_once("../MODEL/EspeceRace.php");


    class ServiceEspeceRace{

        private $EspeceRace;

        public function __construct(){ 

            $this->EspeceRace = new EspeceRaceDAO();
        }

        public function SelectEspece(){

            $data = $this->EspeceRace->SelectEspeceDAO();
            return $data;
        }

        public function SelectRace(){

            $data = $this->EspeceRace->SelectRaceDAO();
            return $data;
        }

        public function SelectRaceAdoptionForm($post){

            $data = $this->EspeceRace->SelectRaceAdoptionForm($post);
            return $data;
        }
    }
?>