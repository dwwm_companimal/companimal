<?php

include_once('../MODEL/Utilisateur.php');
include_once('../DAO/UtilisateurDAO.php');
include_once('../DAO/MysqliQueryExceptionDAO.php');
include_once('../DAO/MysqliExceptionDAO.php');
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);


class ServiceUtilisateur {
      
    public function ajoutUser($tab){

        $User     = new Utilisateur();   
        $UserDAO = new UtilisateurDAO();
            
        $User  -> setNomUtilisateur($tab['nom']);
        $User  -> setPrenomUtilisateur($tab['prenom']); 
        $User  -> setAdresseUtilisateur($tab['adresse']); 
        $User  -> setCodePostalUtilisateur($tab['codePostal']); 
        $User  -> setVilleUtilisateur($tab['ville']); 
        $User  -> setTelephoneUtilisateur($tab['tel']); 
        $User  -> setEmail($tab['email']); 
        $User  -> setmotDePasse(password_hash($tab['motDePasse'], PASSWORD_DEFAULT));
        
        $data = $UserDAO-> selectWhereEgale($tab['email']);

        if(count($data)>0){
           return "false"; 
        }else{
            $UserDAO -> add($User);
            return "true";
        }
        
    }
    
    public function modifUser($tab){

        $User     = new Utilisateur();
        $UserDAO = new UtilisateurDAO();

        $User  -> setIDUtilisateur($tab['idUser']);
        $User  -> setNomUtilisateur($tab['nom']);
        $User  -> setPrenomUtilisateur($tab['prenom']); 
        $User  -> setAdresseUtilisateur($tab['adresse']); 
        $User  -> setCodePostalUtilisateur($tab['codePostal']); 
        $User  -> setVilleUtilisateur($tab['ville']); 
        $User  -> setTelephoneUtilisateur($tab['tel']); 
        $User  -> setEmail($tab['email']); 
              
        $UserDAO -> edit($User);
    }

    public function modifMDP($tab){

        $User     = new Utilisateur();
        $UserDAO = new UtilisateurDAO();

        $User  -> setIDUtilisateur($_SESSION['iduser']);
        $User  -> setMotDePasse(password_hash($tab['password1'], PASSWORD_DEFAULT));   
              
        $data = $UserDAO-> selectWhereEgaleId($_SESSION['iduser']);
        $temp = $data[0]['mot_de_passe'];
        $password = $tab['oldPassword'];

          if(password_verify($password,$temp)){
            $UserDAO -> editMDP($User);
            return "true";
        }else if(!password_verify($password,$temp)){   
            return "false3"; 
        }  
    }

    public function modifRole($tab){

        $User     = new Utilisateur();
        $UserDAO = new UtilisateurDAO();

        $User  -> setIDUtilisateur($tab['idUser']);
        $User  -> setNomRole($tab['role']);
     
        $UserDAO -> editRole($User);    
    }

    
    public function selectAll(){

        $UserDAO = new UtilisateurDAO();

        $data = $UserDAO -> SelectAll();

        return $data;
    }

    public function MDPEmail($post){

        $User     = new Utilisateur();
        $UserDAO = new UtilisateurDAO();

        $newMDP = $UserDAO -> MDPEmail($post);

         return $newMDP;
    }

    public function suppUser($tab){

        $UserDAO = new UtilisateurDAO();
        try{
        $UserDAO -> del($tab["usernameDel"]);

        }catch(MysqliExceptionDAO $msd){
            throw $msd;
        }catch(MysqliQueryExceptionDAO $msqd){
            throw $msqd;
        }
    }

    public function connexion($tab){

        $UserDAO = new UtilisateurDAO();
        $password = $tab['motDePasse'];
        $tabnc = $UserDAO -> selectWhereEgale($tab['email']);
        $temp = $tabnc[0]['mot_de_passe'];
        
        if(password_verify($password,$temp)){
            $_SESSION['iduser'] = $tabnc[0]['id_utilisateur'];
            $_SESSION['role'] = $tabnc[0]['nom_role'];
            $_SESSION['email'] = $tabnc[0]['email']; 
            $_SESSION['telephone'] = $tabnc[0]['telephone_utilisateur']; 
            $_SESSION['nom'] = $tabnc[0]['nom_utilisateur'];
            $_SESSION['prenom'] = $tabnc[0]['prenom_utilisateur'];
           
            return "true";

        }

    }   
}

?>