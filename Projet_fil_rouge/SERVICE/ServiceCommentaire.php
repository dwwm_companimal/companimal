<?php
include_once('..\DAO\CommentaireDAO.php');
include_once('..\DAO\BDDConnexionDAO.php');
include_once('..\MODEL\Commentaire.php');

Class ServiceCommentaire{

    private $Commentaire;

    public function __construct(){

        $this->Commentaire = new CommentaireDAO();
    }

    Public function ajoutCommentaire($post){

        $Commentaire = new Commentaire();

        $Commentaire->setContenuCommentaire($post['contenu_commentaire']);
        
        $this->Commentaire->ajoutCommentaire($Commentaire);
    }

    
    Public function SupCommentaire($get){
        
        $this->Commentaire->SupCommentaire($get);
    }

}

?>