<?php

    include_once('../DAO/ArticleDAO.php');
    include_once('../MODEL/Article.php');

    class ServiceArticle{
    
        public function AjoutArticle($post){

            $Article = new Article();
            $articleDAO = new ArticleDAO();

            $Article->setTitreArticle($post["TitreArticle"]);
            $Article->setContenuArticle($post["ContenuArticle"]);
                
            $articleDAO->AjoutArticle($Article);
        }

        public function ModifArticle($post){

            $Article = new Article();
            $articleDAO = new ArticleDAO();

            $Article->setTitreArticle($post["TitreArticle"]);
            $Article->setContenuArticle($post["ContenuArticle"]);
            $Article->setIdArticle($post["idArticle"]);

            $articleDAO->ModifArticle($Article);
        }

        public function SupArticle($get){

            $articleDAO = new ArticleDAO();
            try{
            $articleDAO->SupArticle($get);
            }catch(MysqliExceptionDAO $msd){
                throw $msd;
            }catch(MysqliQueryExceptionDAO $msqd){
                throw $msqd;
            }

        }

        public function SelectArticle($debut,$NbrArticleParPage){

            $articleDAO = new ArticleDAO();
            $data = $articleDAO->SelectArticle($debut,$NbrArticleParPage);

            return $data;
        }

        public function SelectAllArticle(){

            $articleDAO = new ArticleDAO();
            $data = $articleDAO->SelectAllArticle();

            return $data;
        }

        public function SelectIdArticle($get){
            
            $articleDAO = new ArticleDAO();

            $data = $articleDAO->SelectIdArticle($get);

            return $data;
        }
    }

?>