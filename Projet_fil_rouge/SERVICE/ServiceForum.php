<?php
include_once('..\DAO\ForumDAO.php');
include_once('..\MODEL\Topic.php');

Class ServiceForum{

    private $Topic;
     
    public function __construct(){

        $this->Topic = new ForumDAO();
    } 


    /* Ajout Modification Suppresion */

    Public function ajoutTopic($post){

        $Topic= new Topic();

        $Topic-> setNomTopic($post['NomTopic']);
        $Topic-> setContenuTopic($post['ContenuTopic']);   

        $this->Topic->ajoutTopic($Topic);
    }

    Public function modificationTopic($post){

        $Topic= new Topic();

        $Topic-> setNomTopic($post['NomTopic']);
        $Topic-> setContenuTopic($post['ContenuTopic']);   

        $this->Topic->modifyTopic($Topic);
    }

    public function deleteTopic($get){

        $this->Topic->deleteTopic($get);
    }

            /* Alimentation */

    public function SelectAlimentation(){

        $data = $this->Topic->SelectAlimentation();
        return $data;
    }


    public function SelectForumAlimentationRecherche($post){

        $count=0;

        foreach($post as $key=>$val){
            if($val != ""){
                $count++;
                $Tab[$key] = $post[$key];  
            }
        }

        if($count == 0){
            $data = $this->Topic->SelectAlimentation();
            return $data;
        }else{
            $data = $this->Topic->SelectForumAlimentationRecherche($Tab);
            return $data;        
        }
    }


                    /* Sante */

    public function SelectSante(){

        $data = $this->Topic->SelectSante();
        return $data;
    }


    public function SelectForumSanteRecherche($post){

        $count=0;

        foreach($post as $key=>$val){
            if($val != ""){
                $count++;
                $Tab[$key] = $post[$key];  
            }
        }

        if($count == 0){
            $data = $this->Topic->SelectSante();
            return $data;
        }else{
            $data = $this->Topic->SelectForumSanteRecherche($Tab);
            return $data; 
        }
    }


    /* Divertissement */

    public function SelectDivertissement(){

        $data = $this->Topic->SelectDivertissement();
        return $data;
    }


    public function SelectForumDivertissementRecherche($post){

        $count=0;

        foreach($post as $key=>$val){
            if($val != ""){
                $count++;
                $Tab[$key] = $post[$key];  
            }
        }

        if($count == 0){
            $data = $this->Topic->SelectDivertissement();
            return $data;
        }else{
            $data = $this->Topic->SelectForumDivertissementRecherche($Tab);
            return $data;  
        }
    }


    /* Autre */

    public function SelectAutre(){

        $data = $this->Topic->SelectAutre();
        return $data;
    }

    public function SelectForumAutreRecherche($post){

        $count=0;

        foreach($post as $key=>$val){
            if($val != ""){
                $count++;
                $Tab[$key] = $post[$key];  
            }
        }

        if($count == 0){
            $data = $this->Topic->SelectAutre();
            return $data;
        }else{
            $data = $this->Topic->SelectForumAutreRecherche($Tab);
            return $data;        
        }
    }


    /* ******************************************************************************************************** */
}

?>