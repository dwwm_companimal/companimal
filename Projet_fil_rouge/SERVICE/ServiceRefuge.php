<?php

    include_once("../DAO/RefugeDAO.php");
    include_once("../MODEL/Refuge.php");

    class ServiceRefuge{

        private $Refuge;
        
        public function __construct(){

            $this->Refuge = new RefugeDAO();    
        }

        public function SelectRefugeId($IdRefuge){

            $SelectRefugeId = new RefugeDAO();

            $data = $SelectRefugeId->SelectRefugeIdDAO($IdRefuge);
            return $data;
        }
        
        public function SelectAllRefuge(){

            $SelectAllServiceRefuge = new RefugeDAO();

            $data = $SelectAllServiceRefuge->SelectAllRefugeDAO();
            return $data;
        }
      
        public function AjoutRefuge($post){

            $Refuge = new Refuge();

            $Refuge->setNomRefuge($post["NomRefuge"]);
            $Refuge->setAdresseRefuge($post["AdresseRefuge"]);  
            $Refuge->setCodePostalRefuge($post["CodePostalRefuge"]);
            $Refuge->setVilleRefuge($post["VilleRefuge"]);
            $Refuge->setDescriptionRefuge($post["DescriptionRefuge"]);
            $Refuge->setEmailRefuge($post["AdresseMailRefuge"]);       
            $Refuge->setTelephoneRefuge($post["TelRefuge"]);

            $this->Refuge->AjoutRefuge($Refuge);
        }

        public function ModifRefuge($post){

            $Refuge = new Refuge();

            $Refuge->setIdRefuge($post["id_refuge"]);
            $Refuge->setNomRefuge($post["NomRefuge"]);
            $Refuge->setAdresseRefuge($post["AdresseRefuge"]);  
            $Refuge->setCodePostalRefuge($post["CodePostalRefuge"]);
            $Refuge->setVilleRefuge($post["VilleRefuge"]);
            $Refuge->setDescriptionRefuge($post["DescriptionRefuge"]);
            $Refuge->setEmailRefuge($post["AdresseMailRefuge"]);       
            $Refuge->setTelephoneRefuge($post["TelRefuge"]);

            $this->Refuge->ModifRefuge($Refuge);
        }

        public function SupRefuge($get){

            $this->Refuge->SupRefuge($get);
        }

        public function SelectRefuge($debut,$NbrRefugeParPage){

            $data = $this->Refuge->SelectRefuge($debut,$NbrRefugeParPage);
            return $data;
        }
    }

?>