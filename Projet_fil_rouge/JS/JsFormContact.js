
$("#formContact").validate({
    rules: {
    MessageContact: {
        required: true,
        minlength: 10,
        maxlength: 750
        },

    EmailContact: {
        required: true,
        maxlength: 50,
        email:"Votre adresse mail est invalide",
        },  
    },
    
    messages : {
    MessageContact: {
        required: "Veuillez saisir votre message",
        minlength: "Votre message doit contenir plus de 10 caractères" ,
        maxlength: "Votre message doit contenir moins de 750 caractères"
        },

    EmailContact: {
        required: "L'adresse mail est obligatoire",
        email: "L'adresse est incorrect",
        maxlength: "L'adresse mail doit contenir 50 caractères au maximum "
        },    

    }

  });

 

