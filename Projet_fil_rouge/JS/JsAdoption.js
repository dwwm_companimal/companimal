
/* Recherche Adoption */ 
$(".Recherche").change(function(){

    var espece = $("#Espece option:selected").val();
    var race = $("#Race option:selected").val();
    var sexe = $("#Sexe option:selected").val();
    var refuge = $("#Refuge option:selected").val();

    if($('#Age').on('input').val() === 0){
        var age = "";
    }else{
        var age = $('#Age').on('input').val();
    }

    $("#Affichage").load("../HTML/AdoptionAffichage.php",{
        nom_espece:espece,
        Nom_race:race,
        sexe_animal:sexe,
        id_refuge:refuge,
        naissance_animal:age,
    },
    function(e){
        $("#paginAdoption li").remove();
        loadPaginationAdoption();
    })
})

$('#Age').next().text('--');
$('#Age').on('input', function() {
    var espece = $("#Espece option:selected").val();
    var race = $("#Race option:selected").val();
    var sexe = $("#Sexe option:selected").val();
    var refuge = $("#Refuge option:selected").val();

    if($('#Age').on('input').val() === 0){
        var age = "";
    }else{
        var age = $('#Age').on('input').val();
    }

    $('#Age').next().text(age);
    $("#Affichage").load("../HTML/AdoptionAffichage.php",{
        nom_espece:espece,
        Nom_race:race,
        sexe_animal:sexe,
        id_refuge:refuge,
        naissance_animal:age,
    },
    function(e){
        $("#paginAdoption li").remove();
        loadPaginationAdoption();
    })
})



/* Pagination */

function loadPaginationAdoption(){
    var pageSize = 9;

    var pageCount =  $(".contentDisplayAdoption").length / pageSize;
    
    console.log($(".contentDisplayAdoption").length);
    console.log(pageCount);
    
    for(var i = 0 ; i<pageCount;i++){
    
    $("#paginAdoption").append('<li class="page-item"><a class="page-link" href="#">'+(i+1)+'</a></li> ');
    }
    $("#paginAdoption li").first().find("a").addClass("current");

    showPage = function(page) {
    $(".contentDisplayAdoption").hide();
    $(".contentDisplayAdoption").each(function(n) {
        if (n >= pageSize * (page - 1) && n < pageSize * page)
            $(this).show();
    });        
	}
    
	showPage(1);

	$("#paginAdoption li a").click(function() {
	    $("#paginAdoption li a").removeClass("current");
	    $(this).addClass("current");
	    showPage(parseInt($(this).text())) 
	});
}

/* Formulaire Adoption Race en fonction de l'espece */

$("#EspeceAnimal").change(function(){
    var espece= $("#EspeceAnimal option:selected").val();
    $("#RaceAnimal").load("../HTML/AdoptionAffichageRace.php",{
        nom_espece:espece,
    })
})

/* Formulaire adoption test assert js */

$("#formAdoption").validate({
    rules: {
        NomAnimal: {
            required: true,
            minlength: 2,
            maxlength: 30
        },

        EspeceAnimal: {
            required: true
           
        },

        RaceAnimal: {
            required: true
            
        },

        userImage: {
            required: true
            

        },

        SexeAnimal: {
            required: true
            

        },

        AgeAnimal: {
            required: true,
            max:30  

        },

        RefugeAnimal: {
            required: true

        },

        DescriptionAnimal: {
            required: true,
            minlength: 5,
            maxlength: 500
        }
    },



    messages: {
        NomAnimal: {
            required: "Le nom de l'animal est obligatoire.",
            minlength: "Le nom de l'animal doit contenir au minimum 2 caractères",
            maxlength: "Le nom de l'animal doit contenir au maximum 30 caractères"
        },

        EspeceAnimal: {
            required: "Veuillez de sélectionner l'espèce de l'animal"

        },

        RaceAnimal: {
            required: "Veuillez de sélectionner la race de l'animal"

        },

        userImage: {
            required: "Veuillez insérer une photo de l'animal"


        },

        SexeAnimal: {
            required: "Veuillez sélectionner le sexe de l'animal"


        },

        AgeAnimal: {
            required: "Age de l'animal obligatoire",
            max: "L'age maximal est de 30 ans "

        },

        RefugeAnimal: {
            required: "Veuillez sélectionner un refuge"

        },

        DescriptionAnimal: {
            required: "La description de l'animal est obligatoire",
            minlength: "La description doit comporter 5 caractères minimum",
            maxlength: "La description peut comporter au maximum 500 caractères"
        }
    },
    errorPlacement: function (error, element) {
        if (element.is(":radio")) {
            error.appendTo(element.parents('.puces'));
        }
        else { // This is the default behavior 
            error.insertAfter(element);
        }
    }
});
