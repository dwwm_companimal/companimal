/* Formulaire Inscription */
$.validator.addMethod(
    "regex2",
    function(value, element, reg) {
        if (reg.constructor != RegExp)
            reg = new RegExp(reg);
        else if (reg.global)
            reg.lastIndex = 0;
        return this.optional(element) || reg.test(value);
    },
    "Wrong input found, please check your input."
);

  
    $("#formArticle").validate({
      rules: {
        TitreArticle : {
          required: true,
          minlength: 5,
          maxlength: 50
        },

        userImage : {
            required: true
           
        },

        ContenuArticle : {
            required: true,
            minlength: 5,
            maxlength: 500
        }
    },
      
      
      
      messages : {
        TitreArticle : {
            required: "Titre obligatoire.",
            minlength: "La taille minimale du titre est de 5 caractères.",
            maxlength: "La taille minimale du titre est de 50 caractères."
          },
  
          userImage : {
              required: "Image obligatoire."
             
          },
  
          ContenuArticle : {
              required: "Contenu obligatoire.",
              minlength: "La taille minimale du contenu est de 5 caractères.",
              maxlength: "La taille maximale du contenu est de 500 caractères."
          }
      }
    });


    $(".suppArticle").submit(function(e){
        e.preventDefault();
        
        var formData = $(this).serialize();
         $.ajax({
             url : 'suppressionArticle.php',
             type:'POST',
             data:formData,
             success : function(data){
                 
                response = JSON.parse(data);
              
               
                if(response.error){
                    
                    if(response.error.code == 1045){
                        alert("La connexion à la base de donnée à échoué veuillez verifier votre connexion ou réessayer ulterieurement.");
                    } else {
                        alert(response.error.message);
                    }
                }
                else{
               
                    location.href="Accueil.php"
                }
            }, 
             error : function(xhr, message, status){
                 alert("Errer !!");
             }
         })
     })   