/* Formulaire Inscription */
$.validator.addMethod(
    "regex2",
    function(value, element, reg) {
        if (reg.constructor != RegExp)
            reg = new RegExp(reg);
        else if (reg.global)
            reg.lastIndex = 0;
        return this.optional(element) || reg.test(value);
    },
    "Wrong input found, please check your input."
);

  
    $("#formInscription").validate({
      rules: {
        nom : {
          required: true,
          minlength: 2,
          maxlength: 30
        },

        prenom : {
            required: true,
            minlength: 2,
            maxlength: 30
        },

        adresse : {
            required: false,
            minlength: 5,
            maxlength: 50
        },

        ville: {
            required: false,
            minlength: 2,
            maxlength: 40
        
        },

        codePostal: {
            required: false,
            regex2:("[0-9][0-9][0-9][0-9][0-9]")

        },

        tel: {
            required: true,
            regex2:("[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]")

        },

        email: {
            required: true,
            maxlength: 50
          

        },
          
        motDePasse: {
            required:true,
            regex2:("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[-!@#\$%\^&\*])(?=.{8,})")
        },

        motDePasse2: {
            required: true,
            equalTo: "#motDePasse"
        }
    },
      
      
      
      messages : {
        nom: {
          required: "Le nom est obligatoire",  
          minlength: "Votre nom doit contenir au minimum 2 caractères",
          maxlength: "Le nom doit contenir 30 caractères au maximum "
       
        },
        prenom: {
            required: "Le prenom est obligatoire",  
            minlength: "Votre prenom doit contenir au minimum 2 caractères",
            maxlength: "Le prénom doit contenir 30 caractères au maximum "
         
          },
        adresse: {
            minlength: "Votre adresse doit contenir au minimum 5 caractères",
            maxlength: "L'adresse' doit contenir 50 caractères au maximum "
        
        },
        ville: {
            minlength: "Votre ville doit contenir au minimum 2 caractères",
            maxlength: "La ville doit contenir 40 caractères au maximum "
        
        },
        codePostal: {
            regex2: "Votre code postal doit contenir 5 chiffres"
        
        },
        tel: {
            required: "Le numéro de téléphone est obligatoire",
            regex2: "Le numéro  doit contenir 10 chiffres"
        
        },

        email: {
            required: "L'adresse mail est obligatoire",
            email:"Votre adresse mail est invalide",
            maxlength: "L'email peut contenir 50 caractères au maximum "
   

        },

        motDePasse:{
            required: "Le mot de passe est obligatoire",
            regex2: "Votre mot de passe doit contenir au minimum:<br/>  8 caractères <br/>  1 lettre majuscule <br/> 1 lettre minuscule <br/> 1 chiffre <br/> 1 caractère spécial (-!@#\$%\^&\*) "
        
        },

        motDePasse2:{
            required: "La confirmation de mot de passe est obligatoire",
            equalTo: "Les deux mots de passe doivent être identiques"
        
        }
      }
    });
 

  

$("#formInscription").submit(function (e) {
    e.preventDefault();
    var formData = $(this).serialize();

    var email = $('#email').val();

    if (email != "") {
        $.post('testInscription.php', { email: email }, function (resp) {

            if (resp != "true") {
                $('.feedback').text(resp);
            } else {

                if ($("#formInscription").valid())
                $.ajax({
                    url: 'Inscription.php',
                    type: 'POST',
                    data: formData,
                    success: function (data2) {

                        response = JSON.parse(data2);


                        if (response.error) {

                            if (response.error.code == 891) {
                                $('.feedback').text("Cet email est déjà enregistré");
                            } else {
                                alert(response.error.message);
                            }
                        }
                        else {

                            location.href = "Accueil.php"
                        }
                    },
                    error: function (xhr, message, status) {
                        alert("Errer !!");
                    }
                })

            }
        });
    }
})

   

$("#formLogin").submit(function (e) {
    e.preventDefault();
    var formData = $(this).serialize();
    
    var email = $('#email2').val();

    if (email != "") {
        $.post('testLogin.php', { email: email }, function (resp) {
            
            if (resp != "true") {
                $('.feedback2').text(resp);
            } else {
              
                
                $.ajax({
                    url: 'Login.php',
                    type: 'POST',
                    data: formData,
                    success: function (data2) {
                        
                        response = JSON.parse(data2);


                        if (response.error) {

                            if (response.error.code == 888) {
                                $('.feedback4').text("Mot de passe incorrect");
                            } else {
                                alert(response.error.message);
                            }
                        }
                        else {

                            location.href = "Accueil.php"
                        }
                    },
                    error: function (xhr, message, status) {
                        alert("Errer !!");
                    }
                })
                
            }
        });
    }
})

$("#modifMDP").submit(function (e) {
    e.preventDefault();
    var formData = $(this).serialize();
        
    if ($("#modifMDP").valid())
        $.ajax({
            url: 'ModifMDP.php',
            type: 'POST',
            data: formData,
            success: function (data2) {
              
                response = JSON.parse(data2);
                

                if (response.error) {

                    if (response.error.code == 889) {
                        $('.feedback5').text(" Ancien mot de passe incorrect");
                    } else {
                        alert(response.error.message);
                    }
                }
                else {

                    location.href = "Accueil.php"
                }
            },
            error: function (xhr, message, status) {
                alert("Errer !!");
            }
        })
})

$("#MDPOublie").submit(function (e) {
    e.preventDefault();
    var formData = $(this).serialize();

    
        $.ajax({
            url: 'testMDpPerdu.php',
            type: 'POST',
            data: formData,
            success: function (data2) {
                
                response = JSON.parse(data2);


                if (response.error) {

                    if (response.error.code == 890) {
                        $('.feedback3').text("Email non enregistré");
                    } else {
                        alert(response.error.message);
                    }
                }
                else {

                    location.href = "Accueil.php"
                }
            },
            error: function (xhr, message, status) {
                alert("Errer !!");
            }
        })
})



    $(".suppUser").submit(function(e){
        e.preventDefault();
        
        var formData = $(this).serialize();
         $.ajax({
             url : 'suppressionUtilisateur.php',
             type:'POST',
             data:formData,
             success : function(data){
                
                response = JSON.parse(data);
              
               
                if(response.error){
                    
                    if(response.error.code == 1045){
                        alert("La connexion à la base de donnée à échoué veuillez verifier votre connexion ou réessayer ulterieurement.");
                    } else {
                        alert(response.error.message);
                    }
                }
                else{
               
                    location.href="Accueil.php?action=gestionUsers"
                }
            }, 
             error : function(xhr, message, status){
                 alert("Errer !!");
             }
         })
     })   

$("#formLogin").validate({
    rules: {
        email: {
            required: true,

        },

        motDePasse: {
            required: true
           
        }

    },



    messages: {
       
        email: {
            required: "L'adresse mais est obligatoire",

        },

        motDePasse: {
            required: "Le mot de passe est obligatoire",
           
        }
      
    }
});


$("#modifMDP").validate({
    rules: {

        oldPassword: {
            required: true
            
        },
        password1: {
            required: true,
            regex2: ("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[-!@#\$%\^&\*])(?=.{8,})")
        },
        password2: {
            equalTo: "#password1"
        }


    },



    messages: {


        oldPassword: {
            required: "L'ancien mot de passe est obligatoire"
            

        },
        password1: {
            required: "Le mot de passe est obligatoire",
            regex2: "Votre mot de passe doit contenir au minimum:<br/>  8 caractères <br/>  1 lettre majuscule <br/> 1 lettre minuscule <br/> 1 chiffre <br/> 1 caractère spécial (-!@#\$%\^&\*) "

        },
        password2: {
            equalTo: "Les deux mots de passe doivent être identiques"
        }

    }
});

// fonction on click select role gestion des employés
function onClickBtnComm() {
    const frm = $(this).parent();
    const td = frm.parent();
    const form = td.find('form.formHid');
    var substring = 'd-none'
    if (form.attr('class').indexOf(substring) !== -1) {
        form.removeClass('d-none');
    } else {
        form.addClass('d-none');
    }

};

document.querySelectorAll('a.btnRole').forEach(function (link) {
    link.addEventListener('click', onClickBtnComm);
});