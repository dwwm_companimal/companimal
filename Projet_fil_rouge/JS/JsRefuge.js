
$.validator.addMethod(
    "regex2",
    function(value, element, reg) {
        if (reg.constructor != RegExp)
            reg = new RegExp(reg);
        else if (reg.global)
            reg.lastIndex = 0;
        return this.optional(element) || reg.test(value);
    },
    "Ce champs ne doit contenir que des chiffres"
)


$("#formRefuge").validate({
    rules: {
    NomRefuge : {
        required: true,
        minlength: 5,
        maxlength: 50
        },

    AdresseRefuge : {
        required: true,
        minlength: 5,
        maxlength: 50
        },

    VilleRefuge : {
        required: true,
        minlength: 2,
        maxlength: 40

        },

    CodePostalRefuge: {
        required: true,
        maxlength: 5,
        regex2:("[0-9][0-9][0-9][0-9][0-9]")
        },

    AdresseMailRefuge: {
        required: true,
        maxlength: 50,
        email:"Votre adresse mail est invalide",
        },

    TelRefuge: {
        required: true,
        maxlength:10,
        minlength: 10,
        regex2:("[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]")
         },

    userImage: {
        required: true,
        },
        
    DescriptionRefuge: {
        required:true,
        minlength: 10,
        maxlength: 500        
        }
      
  },
    
    messages : {
    NomRefuge: {
        required: "Le nom du refuge est obligatoire",  
        minlength: "Le nom du refuge doit contenir au minimum 5 caractères",
        maxlength: "Le nom du refuge doit contenir 50 caractères au maximum "
     
        },
    AdresseRefuge: {
        required: "L'adresse du refuge est obligatoire",  
        minlength: "L'adresse du refuge doit contenir au minimum 5 caractères",
        maxlength: "L'adresse du refuge doit contenir 50 caractères au maximum "
       
        },
    VilleRefuge: {
        required: "La ville du refuge est obligatoire", 
        minlength: "La ville refuge doit contenir au minimum 5 caractères",
        maxlength: "La ville refuge doit contenir 40 caractères au maximum"      
      
        },
    CodePostalRefuge: {
        required: "Le code postal est obligatoire",
        maxlength: "Le code postal doit contenir moins de 5 caractères",
        regex2:"Le code postal doit contenir 5 chiffres"             
        },
    AdresseMailRefuge: {
        required: "L'adresse mail est obligatoire",
        email: "L'adresse est incorrect",
        maxlength: "L'adresse mail doit contenir 50 caractères au maximum "
        },

    TelRefuge: {
        required: "Le numéro de téléphone est obligatoire",
        maxlength: "Le numéro de téléphone doit contenir 10 caractères au maximum",
        minlength: "Le numéro de téléphone doit contenir 10 caractères au minimum" 
        },

    userImage: {
        required: "Vous devez obligatoirement choisir une image"
        },

    DescriptionRefuge:{
        required: "Ce champs est obligatoire",
        minlength: "Votre message doit contenir plus de 10 caractères",
        maxlength: "Votre message doit contenir moins de 500 caractères"        
        }

    },
    
    
  });