/* Recherche Pet Sitter */

$(".RecherchePetSitter").change(function(){
    var typeGarde = $("#TypeGarde option:selected").val();
    var typeDomicile = $("#TypeDomicile option:selected").val();
    var espece = $("#Espece option:selected").val();
    var codePostal  = $("#CodePostalPetSitter").val();
    $("#AffichagePetSitter").load("../HTML/PetSitterAffichage.php",{
        type_garde_pet_sitter:typeGarde,
        type_domicile_pet_sitter:typeDomicile,
        nom_espece:espece,
        distance_pet_sitter:codePostal,
    },
    function(e){
        $("#paginPetSitter li").remove();
        loadPaginationPetSitter();
    })
})

$("#CodePostalPetSitter").keyup(function() {
    var typeGarde = $("#TypeGarde option:selected").val();
    var typeDomicile = $("#TypeDomicile option:selected").val();
    var espece = $("#Espece option:selected").val();
    var codePostal  = $("#CodePostalPetSitter").val();
    $("#AffichagePetSitter").load("../HTML/PetSitterAffichage.php",{
        type_garde_pet_sitter:typeGarde,
        type_domicile_pet_sitter:typeDomicile,
        nom_espece:espece,
        distance_pet_sitter:codePostal,
    },
    function(e){
        $("#paginPetSitter li").remove();
        loadPaginationPetSitter();
    })
})

/* Pagination */
function loadPaginationPetSitter(){
    var pageSize = 5;

    var pageCount =  $(".contentDisplayPetSitter").length / pageSize;
    
    console.log($(".contentDisplayPetSitter").length);
    console.log(pageCount);
    
    for(var i = 0 ; i<pageCount;i++){
    
    $("#paginPetSitter").append('<li class="page-item"><a class="page-link" href="#">'+(i+1)+'</a></li> ');
    }
    $("#paginPetSitter li").first().find("a").addClass("current");

    showPage = function(page) {
    $(".contentDisplayPetSitter").hide();
    $(".contentDisplayPetSitter").each(function(n) {
        if (n >= pageSize * (page - 1) && n < pageSize * page)
            $(this).show();
    });        
	}
    
	showPage(1);

	$("#paginPetSitter li a").click(function() {
	    $("#paginPetSitter li a").removeClass("current");
	    $(this).addClass("current");
	    showPage(parseInt($(this).text())) 
	});
}

/* ****** */

$.validator.addMethod(
    "regex2",
    function(value, element, reg) {
        if (reg.constructor != RegExp)
            reg = new RegExp(reg);
        else if (reg.global)
            reg.lastIndex = 0;
        return this.optional(element) || reg.test(value);
    },
    "Wrong input found, please check your input."
)

$("#formPetSitter").validate({
    rules: {
    NomAnnonceFormPetSitter : {
        required: true,
        minlength: 5,
        maxlength: 100
        },

    CodePostalPetSitter : {
        required: true,
        minlength: 5,
        maxlength: 5
        },

    Garde : {
        required: true,

        },

    afftel: {
        required: true,
        },

    Domicile: {
        required: true,
        },

    userImage: {
        required: true,
         },

    EspecePetSitter: {
        required: true,
        },
        
    InfoCompPetSitter: {
        required:true,
        minlength: 10,
        maxlength: 500        
        }
      
  },
    
    messages : {
    NomAnnonceFormPetSitter: {
        required: "Le nom de l'annonce est obligatoire",  
        minlength: "Le nom de l'annonce doit contenir au minimum 5 caractères",
        maxlength: "Le nom doit contenir 100 caractères au maximum "
     
        },
    CodePostalPetSitter: {
        required: "Le code postal est obligatoire",  
        minlength: "Le code postal doit contenir au minimum 5 caractères",
        maxlength: "Le code postal doit contenir 5 caractères au maximum ",
        regex2:("[0-9][0-9][0-9][0-9][0-9]")
       
        },
    Garde: {
        required: "Ce champs est obligatoire"       
      
        },
    afftel: {
        required: "Ce champs est obligatoire"
              
        },
    Domicile: {
        required: "Ce champs est obligatoire"
        },

    userImage: {
        required: "Vous devez obligatoirement choisir une image pour votre annonce"
        },

    EspecePetSitter: {
        required: "Veuillez spécifier une espèce"
        },

    InfoCompPetSitter:{
        required: "Ce champs est obligatoire",
        minlength: "Votre message doit contenir plus de 10 caratères" ,
        maxlength: "Votre message doit contenir moins de 500 caratères"        
        }

    },

    errorPlacement: function (error, element) {
        if (element.is(":radio")) {
            error.appendTo(element.parents('.puces'));
        }
        else { // This is the default behavior 
            error.insertAfter(element);
        }
    }

  });

 