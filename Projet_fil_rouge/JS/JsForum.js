/* Recherche Forum Alimentation*/

$("#NomTopicAlimentation").keyup(function() {
    var nomTopic  = $("#NomTopicAlimentation").val();
    $("#AffichageForumAlimentation").load("../HTML/ForumAlimentationAffichage.php",{
        nom_topic:nomTopic,

    },
    function(e){
        $("#paginForumArticle li").remove();
        loadPaginationForumArticle();
    })
})

/* Recherche Forum Sante*/

$("#NomTopicSante").keyup(function() {
    var nomTopic  = $("#NomTopicSante").val();
    $("#AffichageForumSante").load("../HTML/ForumSanteAffichage.php",{
        nom_topic:nomTopic,

    },
    function(e){
        $("#paginForumArticle li").remove();
        loadPaginationForumArticle();
    })
})

/* Recherche Forum Divertissement*/

$("#NomTopicDivertissement").keyup(function() {
    var nomTopic  = $("#NomTopicDivertissement").val();
    $("#AffichageForumDivertissement").load("../HTML/ForumDivertissementAffichage.php",{
        nom_topic:nomTopic,

    },
    function(e){
        $("#paginForumArticle li").remove();
        loadPaginationForumArticle();
    })
})

/* Recherche Forum Autre*/

$("#NomTopicAutre").keyup(function() {
    var nomTopic  = $("#NomTopicAutre").val();
    $("#AffichageForumAutre").load("../HTML/ForumAutreAffichage.php",{
        nom_topic:nomTopic,

    },
    function(e){
        $("#paginForumArticle li").remove();
        loadPaginationForumArticle();
    })
})

/* Pagination Forum*/

function loadPaginationForumArticle(){
    var pageSize = 9;

    var pageCount =  $(".contentDisplayForumArticle").length / pageSize;
    
    console.log($(".contentDisplayForumArticle").length);
    console.log(pageCount);
    
    for(var i = 0 ; i<pageCount;i++){
    
    $("#paginForumArticle").append('<li class="page-item"><a class="page-link" href="#">'+(i+1)+'</a></li> ');
    }
    $("#paginForumArticle li").first().find("a").addClass("current");

    showPage = function(page) {
    $(".contentDisplayForumArticle").hide();
    $(".contentDisplayForumArticle").each(function(n) {
        if (n >= pageSize * (page - 1) && n < pageSize * page)
            $(this).show();
    });        
	}
    
	showPage(1);

	$("#paginForumArticle li a").click(function() {
	    $("#paginForumArticle li a").removeClass("current");
	    $(this).addClass("current");
	    showPage(parseInt($(this).text())) 
	});
}

/* Pagination Commentaire*/

function loadPaginationForumCommentaire(){
    var pageSize = 4;

    var pageCount =  $(".contentDisplayForumCommentaire").length / pageSize;
    
    console.log($(".contentDisplayForumCommentaire").length);
    console.log(pageCount);
    
    for(var i = 0 ; i<pageCount;i++){
    
    $("#paginForumCommentaire").append('<li class="page-item"><a class="page-link" href="#">'+(i+1)+'</a></li> ');
    }
    $("#paginForumCommentaire li").first().find("a").addClass("current");

    showPage = function(page) {
    $(".contentDisplayForumCommentaire").hide();
    $(".contentDisplayForumCommentaire").each(function(n) {
        if (n >= pageSize * (page - 1) && n < pageSize * page)
            $(this).show();
    });        
	}
    
	showPage(1);

	$("#paginForumCommentaire li a").click(function() {
	    $("#paginForumCommentaire li a").removeClass("current");
	    $(this).addClass("current");
	    showPage(parseInt($(this).text())) 
	});
}

$("#formForum").validate({
    rules: {
        NomTopic: {
            required: true,
            minlength: 10,
            maxlength: 100
        },

        ContenuTopic: {
            required: true,
            minlength: 50,
        },  
    },
    
    messages : {
        NomTopic: {
            required: "Veuillez saisir le titre du topic",
            minlength: "Votre message doit contenir plus de 10 caractères" ,
            maxlength: "Votre message doit contenir moins de 100 caractères",
        },

        ContenuTopic: {
            required: "Un contenu pour ce Topic est obligatoire",
            minlength: "Le contenu doit contenir au minimum 50 caratères",
        },    

    }

  });