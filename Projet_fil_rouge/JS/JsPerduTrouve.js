/* Recherche Perdu Trouver */

$(".RecherchePerduTrouver").change(function(){
    var espece = $("#Espece option:selected").val();
    var type = $("#Type option:selected").val();
    var codePostal  = $("#CodePostal").val();
    $("#AffichagePerduTrouver").load("../HTML/PerduTrouverAffichage.php",{
        nom_espece:espece,
        type_perdu_trouve:type,
        code_postale_perdu_trouve:codePostal
    },
    function(e){
        $("#paginPerduTrouve li").remove();
        loadPaginationPerduTrouve();
    })
})

$("#CodePostal").keyup(function() {
    var espece = $("#Espece option:selected").val();
    var type = $("#Type option:selected").val();
    var codePostal  = $("#CodePostal").val();
    $("#AffichagePerduTrouver").load("../HTML/PerduTrouverAffichage.php",{
        nom_espece:espece,
        type_perdu_trouve:type,
        code_postale_perdu_trouve:codePostal
    },
    function(e){
        $("#paginPerduTrouve li").remove();
        loadPaginationPerduTrouve();
    })
})

/* Pagination Perdu Trouve */

function loadPaginationPerduTrouve(){
    var pageSize = 9;

    var pageCount =  $(".contentDisplayPerduTrouve").length / pageSize;
    
    console.log($(".contentDisplayPerduTrouve").length);
    console.log(pageCount);
    
    for(var i = 0 ; i<pageCount;i++){
    
    $("#paginPerduTrouve").append('<li class="page-item"><a class="page-link" href="#">'+(i+1)+'</a></li> ');
    }
    $("#paginPerduTrouve li").first().find("a").addClass("current");

    showPage = function(page) {
    $(".contentDisplayPerduTrouve").hide();
    $(".contentDisplayPerduTrouve").each(function(n) {
        if (n >= pageSize * (page - 1) && n < pageSize * page)
            $(this).show();
    });        
	}
    
	showPage(1);

	$("#paginPerduTrouve li a").click(function() {
	    $("#paginPerduTrouve li a").removeClass("current");
	    $(this).addClass("current");
	    showPage(parseInt($(this).text())) 
	});
}


/* Formulaire Perdu Trouve test assert js */

$.validator.addMethod(
    "regex2",
    function (value, element, reg) {
        if (reg.constructor != RegExp)
            reg = new RegExp(reg);
        else if (reg.global)
            reg.lastIndex = 0;
        return this.optional(element) || reg.test(value);
    },
    "Wrong input found, please check your input."
);

$("#formPerduTrouve").validate({
    rules: {
        TypeAnnoncePT: {
            required: true
           
        },

        NomAnnoncePT: {
            required: true,
            minlength: 5,
            maxlength: 30

        },

        NomAnimalPT: {
            required: false,
            minlength: 2,
            maxlength: 50

        },

        userImage: {
            required: true


        },

        EspeceAnimalPT: {
            required: true


        },

        LieuPT: {
            required: true,
            minlength: 2,
            maxlength: 30

        },

        CodePostalPT: {
            required: true,
            regex2: ("[0-9][0-9][0-9][0-9][0-9]")

        },

        DescriptionPT: {
            required: true,
            minlength: 5,
            maxlength: 500
        }
    },



    messages: {
        TypeAnnoncePT: {
            required: "Veuillez sélectionner le type d'annonce"

        },

        NomAnnoncePT: {
            required: "veuillez saisir un titre",
            minlength: "Le titre doit comporter au moins 5 caractères",
            maxlength: "Le titre peut comporter au maximum 30 caractères"

        },

        NomAnimalPT: {
            
            minlength: "Le nom de l'animal doit comporter au moins 2 caractères",
            maxlength: "Le nom de l'animal peut comporter au maximum 50 caractères"


        },

        userImage: {
            required: "Veuillez insérer une photo de l'animal"


        },

        EspeceAnimalPT: {
            required: "Veuillez sélectionner une espèce"


        },

        LieuPT: {
            required: "Veuillez saisir une ville",
            minlength: "La ville doit comporter au minimum 2 caractères",
            maxlength: "La ville peut comporter au maximum 50 caractères"

        },

        CodePostalPT: {
            required: "Veuillez saisir un code postale",
            regex2: "Le code postale doit comporter 5 chiffres"

        },

        DescriptionPT: {
            required: "Veuillez saisir une description",
            minlength: "La description doit comporter au minimum 5 caractères",
            maxlength: 500
        }
    },
    errorPlacement: function (error, element) {
        if (element.is(":radio")) {
            error.appendTo(element.parents('.puces'));
        }
        else { // This is the default behavior 
            error.insertAfter(element);
        }
    }
});
