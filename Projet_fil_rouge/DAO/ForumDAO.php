<?php
include_once('..\DAO\BDDConnexionDAO.php');
include_once('..\MODEL\Topic.php');

Class ForumDAO extends BDDConnexionDAO{


    /* Ajout Modification Suppresion */

    Public function ajoutTopic($Topic){
            
        $mysqli = $this->connectionBdd();

        $iduser = $_SESSION['iduser'];            
        $categorieTopic = $_GET['categorie'];

        $titre = $Topic->getNomTopic();
        $contenu = $Topic->getContenuTopic();

        $stmt = $mysqli->prepare('INSERT INTO topic( nom_topic,categorie_topic,contenu_topic,date_topic, id_utilisateur) VALUES ( ?,?,?,Localtimestamp(),?)');
        $stmt -> bind_param("sisi", $titre,$categorieTopic,$contenu,$iduser);
        $stmt -> execute();  
    }

    public function modifyTopic($Topic){

        $mysqli = $this->connectionBdd();

        $idtopic = $_GET['idtopic'];

        $titre = $Topic -> getNomTopic();
        $contenu = $Topic -> getContenuTopic(); 

        $stmt = $mysqli->prepare('UPDATE topic SET nom_topic=? ,contenu_topic=? WHERE id_topic='.$idtopic.'');
        $stmt -> bind_param("ss", $titre,$contenu);
        $stmt -> execute();    
    }

    public function deleteTopic($get){
        
        $mysqli = $this->connectionBdd();

        $stmt = $mysqli->prepare("DELETE FROM topic WHERE id_topic=?");
        $stmt -> bind_param("i",$get);
        $stmt -> execute();
    }

                    /* Alimentation */

    public function SelectAlimentation(){

        $mysqli = $this->connectionBdd();

        $stmt = $mysqli->prepare("SELECT *, DATE_FORMAT(date_topic, '%d/%m/%Y à %H\h%i') as date_t FROM topic WHERE categorie_topic=2 ORDER BY id_topic DESC");
        $stmt->execute();
        $rs = $stmt->get_result();
        $data = $rs->fetch_all(MYSQLI_ASSOC);

        return $data;
    }



    public function SelectForumAlimentationRecherche($Tab){

        $mysqli = $this->connectionBdd();

        $rec = "";

        foreach($Tab as $key=>$val){   
            $rec .= $key .' LIKE "%'.$val.'%" AND ';
        }

        $rec = rtrim($rec, ' AND ');
        /*echo '<br>';
        echo "SELECT *, DATE_FORMAT(date_topic, '%d/%m/%Y à %H\h%i') as date_t FROM topic WHERE categorie_topic=2 AND ".$rec."";
        echo '<br>';
        echo '<br>';*/

        $stmt = $mysqli->prepare("SELECT *, DATE_FORMAT(date_topic, '%d/%m/%Y à %H\h%i') as date_t FROM topic WHERE categorie_topic=2 AND ".$rec." ORDER BY id_topic DESC");
        $stmt->execute();
        $rs = $stmt->get_result();
        $data = $rs->fetch_all(MYSQLI_ASSOC);

        return $data;
    }


            /* Sante */

    public function SelectSante(){

        $mysqli = $this->connectionBdd();

        $stmt = $mysqli->prepare("SELECT *, DATE_FORMAT(date_topic, '%d/%m/%Y à %H\h%i') as date_t FROM topic WHERE categorie_topic=1 ORDER BY id_topic DESC");
        $stmt->execute();
        $rs = $stmt->get_result();
        $data = $rs->fetch_all(MYSQLI_ASSOC);

        return $data;
    }


    public function SelectForumSanteRecherche($Tab){

        $mysqli = $this->connectionBdd();

        $rec = "";

        foreach($Tab as $key=>$val){   
            $rec .= $key .' LIKE "%'.$val.'%" AND ';
        }

        $rec = rtrim($rec, ' AND ');

        $stmt = $mysqli->prepare("SELECT *, DATE_FORMAT(date_topic, '%d/%m/%Y à %H\h%i') as date_t FROM topic WHERE categorie_topic=1 AND ".$rec." ORDER BY id_topic DESC");
        $stmt->execute();
        $rs = $stmt->get_result();
        $data = $rs->fetch_all(MYSQLI_ASSOC);

        return $data;
    }

        /* Divertissement */

        public function SelectDivertissement(){

            $mysqli = $this->connectionBdd();
    
            $stmt = $mysqli->prepare("SELECT *, DATE_FORMAT(date_topic, '%d/%m/%Y à %H\h%i') as date_t FROM topic WHERE categorie_topic=3 ORDER BY id_topic DESC");
            $stmt->execute();
            $rs = $stmt->get_result();
            $data = $rs->fetch_all(MYSQLI_ASSOC);
    
            return $data;
        }
    
    
        public function SelectForumDivertissementRecherche($Tab){
    
            $mysqli = $this->connectionBdd();
    
            $rec = "";
    
            foreach($Tab as $key=>$val){   
                $rec .= $key .' LIKE "%'.$val.'%" AND ';
            }
    
            $rec = rtrim($rec, ' AND ');
    
            $stmt = $mysqli->prepare("SELECT *, DATE_FORMAT(date_topic, '%d/%m/%Y à %H\h%i') as date_t FROM topic WHERE categorie_topic=3 AND ".$rec." ORDER BY id_topic DESC");
            $stmt->execute();
            $rs = $stmt->get_result();
            $data = $rs->fetch_all(MYSQLI_ASSOC);
    
            return $data;
        }


        /* Autre */

        public function SelectAutre(){

            $mysqli = $this->connectionBdd();
    
            $stmt = $mysqli->prepare("SELECT *, DATE_FORMAT(date_topic, '%d/%m/%Y à %H\h%i') as date_t FROM topic WHERE categorie_topic=4 ORDER BY id_topic DESC");
            $stmt->execute();
            $rs = $stmt->get_result();
            $data = $rs->fetch_all(MYSQLI_ASSOC);
    
            return $data;
        }
    
    
        public function SelectForumAutreRecherche($Tab){
    
            $mysqli = $this->connectionBdd();
    
            $rec = "";
    
            foreach($Tab as $key=>$val){   
                $rec .= $key .' LIKE "%'.$val.'%" AND ';
            }
    
            $rec = rtrim($rec, ' AND ');
    
            $stmt = $mysqli->prepare("SELECT *, DATE_FORMAT(date_topic, '%d/%m/%Y à %H\h%i') as date_t FROM topic WHERE categorie_topic=4 AND ".$rec." ORDER BY id_topic DESC");
            $stmt->execute();
            $rs = $stmt->get_result();
            $data = $rs->fetch_all(MYSQLI_ASSOC);
    
            return $data;
        }
 
     
}


?>