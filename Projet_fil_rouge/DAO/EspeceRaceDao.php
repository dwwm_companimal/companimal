<?php

    include_once('BDDConnexionDAO.php');

    class EspeceRaceDAO extends BDDConnexionDAO{

        public function SelectEspeceDAO(){

            $mysqli = $this->connectionBdd();

            $rs = mysqli_query($mysqli, 'select nom_espece from espece');
            $data = mysqli_fetch_all($rs,MYSQLI_ASSOC);
    
            return $data;
        }

        public function SelectRaceDAO(){

            $mysqli = $this->connectionBdd();

            $rs = mysqli_query($mysqli, "select Nom_race from race");
            $data = mysqli_fetch_all($rs,MYSQLI_ASSOC);
    
            return $data;
        }

        public function SelectRaceAdoptionForm($post){

            $mysqli = $this->connectionBdd();

            $stmt = $mysqli->prepare("Select Nom_race from race WHERE  nom_espece = ? ");
            $stmt-> bind_param("s",$post);
            $stmt->execute();
            $rs = $stmt->get_result();
            $data = $rs->fetch_all(MYSQLI_ASSOC);
    
            return $data;
        }
    }
?>