<?php
    include_once('../DAO/BDDConnexionDAO.php');

Class PetSitterDAO extends BDDConnexionDAO{

            /* Ajout */

    Public function ajoutAnnoncePetSitter($data){

        $mysqli = $this->connectionBdd();
            
        $iduser = $_SESSION['iduser'];   

        $titrePetSitter = $data -> getTitrePetSitter();
        $distance = $data -> getDistancePetSitter();
        $garde = $data -> getTypeGardePetSitter();
        $domicile = $data -> getTypeDomicilePetSitter();
        $afftel = $data -> getPresentationTelephone();
        $file = $_FILES['userImage']['tmp_name'];
        $photo = file_get_contents($file);
        /*$photo = $data -> getDowloadPhotoPetSitter();*/
        $description = $data -> getDescriptionPetSitter();
        $idEspece = $data -> getIdEspece();

        $stmt = $mysqli->prepare('INSERT INTO pet_sitter(titre_pet_sitter,distance_pet_sitter,type_garde_pet_sitter,type_domicile_pet_sitter,presentation_telephone,description_pet_sitter,id_utilisateur,photo_pet_sitter,nom_espece) 
        VALUES (?, ?, ?, ?,?, ?, ?, ?, ?)');
        $stmt -> bind_param("sissssiss",$titrePetSitter,$distance,$garde,$domicile,$afftel,$description,$iduser,$photo,$idEspece);
        $stmt -> execute();
            
    }
       
        /* Modification */

    public function modifyAnnonce($data){

        $mysqli = $this->connectionBdd();

        $idpet = $data -> getIdPetSitter();          
        $titrePetSitter = $data -> getTitrePetSitter();
        $distance = $data -> getDistancePetSitter();
        $garde = $data -> getTypeGardePetSitter();
        $domicile = $data -> getTypeDomicilePetSitter();
        $afftel = $data -> getPresentationTelephone();  
        $description = $data -> getDescriptionPetSitter();
        $idEspece = $data -> getIdEspece();

        $stmt = $mysqli->prepare('UPDATE pet_sitter SET titre_pet_sitter=? , distance_pet_sitter=? ,type_garde_pet_sitter=? ,type_domicile_pet_sitter=?, presentation_telephone=? ,description_pet_sitter=? , nom_espece=? WHERE id_pet_sitter = ?');/* echo $mysqli -> error;die ;*/
        $stmt -> bind_param("sissssss",$titrePetSitter,$distance,$garde,$domicile,$afftel,$description,$idEspece,$idpet);
        $stmt -> execute();
    }

        /* Sup */

    public function SupPetSitter($get){

        $mysqli = $this->connectionBdd(); 

        $stmt = $mysqli->prepare("DELETE FROM pet_sitter WHERE id_pet_sitter =?");
        $stmt -> bind_param("i",$get);
        $stmt -> execute();
    }

        /* Select toute les info PetSitter */

    public function SelectAllPetSitter(){

        $mysqli = $this->connectionBdd();

        $stmt = $mysqli->prepare('select * from pet_sitter ORDER BY id_pet_sitter DESC');
        $stmt->execute();
        $rs = $stmt->get_result();
        $data = $rs->fetch_all(MYSQLI_ASSOC);

        return $data;
    }

        /* Select Garde Pet Sitter */

    public function SelectGardePetSitter(){

        $mysqli = $this->connectionBdd();

        $stmt = $mysqli->prepare('select DISTINCT type_garde_pet_sitter from pet_sitter ORDER BY id_pet_sitter DESC');
        $stmt->execute();
        $rs = $stmt->get_result();
        $data = $rs->fetch_all(MYSQLI_ASSOC);

        return $data;
    }

        /* Select Domicile Pet Sitter */

    public function SelectDomicilePetSitter(){

        $mysqli = $this->connectionBdd();

        $stmt = $mysqli->prepare('select DISTINCT type_domicile_pet_sitter from pet_sitter ORDER BY id_pet_sitter DESC');
        $stmt->execute();
        $rs = $stmt->get_result();
        $data = $rs->fetch_all(MYSQLI_ASSOC);

        return $data;
    }

        /* Recherche / Pagination */


    public function SelectPetSitterRecherche($Tab){

        $mysqli = $this->connectionBdd();

        $rec = "";

        foreach($Tab as $key=>$val){   
                $rec .= $key .' LIKE "%'.$val.'%" AND ';
        }
        
        $rec = rtrim($rec, ' AND ');
        $rs = mysqli_query($mysqli,'select * from pet_sitter where  '.$rec.' ORDER BY id_pet_sitter DESC');
        $data = mysqli_fetch_all($rs,MYSQLI_ASSOC);

        return $data;
    }


}


?>