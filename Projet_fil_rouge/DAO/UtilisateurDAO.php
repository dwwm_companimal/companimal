<?php

include_once('BDDConnexionDAO.php');
include_once('../MODEL/Utilisateur.php');
include_once('../DAO/MysqliQueryExceptionDAO.php');
include_once('../DAO/MysqliExceptionDAO.php');
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);



class UtilisateurDAO extends BDDConnexionDAO {
    
    public function selectAll(){

        $mysqli=$this->connectionBdd();

        $stmt = $mysqli -> prepare("select * from utilisateur ");
        $stmt->execute();
        $rs = $stmt -> get_result();
        $data = $rs -> fetch_all(MYSQLI_BOTH);

        return $data;
    }

    public function selectRole(){

        $mysqli=$this->connectionBdd();

        $stmt = $mysqli -> prepare("select * from role ");
        $stmt->execute();
        $rs = $stmt -> get_result();
        $data = $rs -> fetch_all(MYSQLI_BOTH);

        return $data;
    }

    public function RandomString($length = 10){

        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';

        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }

    public function MDPEmail($post){

        $newMDP = $this->RandomString();
        $newMDPHash = password_hash($newMDP, PASSWORD_DEFAULT);

        $mysqli=$this->connectionBdd();

        $stmt = $mysqli -> prepare("update utilisateur set mot_de_passe=?  where email=?") ;/*echo $mysqli -> error;die ;*/
        $stmt -> bind_param("ss",$newMDPHash,$post);    
        $stmt->execute(); 
  
        return $newMDP;
    }

    public function selectWhereEgale($compar){

        $mysqli=$this->connectionBdd();

        $str=$compar;

        $stmt = $mysqli -> prepare("select * from utilisateur  where email=?");
        $stmt -> bind_param("s",$str);
        $stmt->execute();
        $rs = $stmt -> get_result();
        $data = $rs -> fetch_all(MYSQLI_BOTH);

        return $data;
    }

    public function selectWhereEgaleId($compar){

        $mysqli=$this->connectionBdd();

        $str=$compar;

        $stmt = $mysqli -> prepare("select * from utilisateur  where id_utilisateur=?");
        $stmt -> bind_param("s",$str);
        $stmt->execute();
        $rs = $stmt -> get_result();
        $data = $rs -> fetch_all(MYSQLI_BOTH);

        return $data;
    }

    function add($Ajout){
        
        $mysqli=$this->connectionBdd();
        
        $nomUser        =$Ajout -> getNomUtilisateur();
        $prenomUser     =$Ajout -> getPrenomUtilisateur(); 
        $adresseUser    =$Ajout -> getAdresseUtilisateur(); 
        $codePostalUser =$Ajout -> getCodePostalUtilisateur(); 
        $villeUser      =$Ajout -> getVilleUtilisateur(); 
        $telUser        =$Ajout -> getTelephoneUtilisateur(); 
        $email          =$Ajout -> getEmail(); 
        $motDePasse     =$Ajout -> getMotDePasse(); 
        
        $stmt = $mysqli -> prepare("INSERT into utilisateur (nom_utilisateur,prenom_utilisateur,adresse_utilisateur,code_postal_utilisateur,ville_utilisateur,telephone_utilisateur,email,mot_de_passe,nom_role) 
                                    values(?,?,?,?,?,?,?,?,'Membre');") ;/*echo $mysqli -> error;die ;*/
        $stmt -> bind_param("sssissss",$nomUser,$prenomUser,$adresseUser,$codePostalUser,$villeUser,$telUser,$email,$motDePasse); 
        $stmt->execute();     
    }
    
    function edit($Edit){

        $mysqli=$this->connectionBdd();
        
        $iduser         =$Edit -> getIdUtilisateur();
        $nomUser        =$Edit -> getNomUtilisateur();
        $prenomUser     =$Edit -> getPrenomUtilisateur(); 
        $adresseUser    =$Edit -> getAdresseUtilisateur(); 
        $codePostalUser =$Edit -> getCodePostalUtilisateur(); 
        $villeUser      =$Edit -> getVilleUtilisateur(); 
        $telUser        =$Edit -> getTelephoneUtilisateur(); 
        $email          =$Edit -> getEmail(); 
        
        $stmt = $mysqli -> prepare("update utilisateur set nom_utilisateur=?, prenom_utilisateur=?, adresse_utilisateur=? , code_postal_utilisateur=?, ville_utilisateur=?, telephone_utilisateur=?, email=? where id_utilisateur=?") ;/*echo $mysqli -> error;die ;*/
        $stmt -> bind_param("sssisssi",$nomUser,$prenomUser,$adresseUser,$codePostalUser,$villeUser,$telUser,$email,$iduser);    
        $stmt->execute();    
    }

    function editMDP($Edit2){

        $mysqli=$this->connectionBdd();
        
        $iduser    =$Edit2 -> getIdUtilisateur();
        $mdp       =$Edit2 -> getMotDePasse();
         
        $stmt = $mysqli -> prepare("update utilisateur set mot_de_passe=? where id_utilisateur=?") ;/*echo $mysqli -> error;die ;*/
        $stmt -> bind_param("si",$mdp,$iduser);    
        $stmt->execute();        
    }

    function editRole($Edit3){

        $mysqli=$this->connectionBdd();
        
        $iduser    =$Edit3 -> getIdUtilisateur();
        $role      =$Edit3 -> getNomRole();
               
        $stmt = $mysqli -> prepare("update utilisateur set nom_role=? where id_utilisateur=?") ;/*echo $mysqli -> error;die ;*/
        $stmt -> bind_param("si",$role,$iduser);    
        $stmt->execute();        
    }

    function del($del){
        try{
        $mysqli=$this->connectionBdd();

        $stmt = $mysqli -> prepare("delete from utilisateur where id_utilisateur=?") ;/*echo $mysqli -> error;die ;*/
        $stmt -> bind_param("s",$del);    
        $stmt->execute();
        }catch(MysqliExceptionDAO $msd){
            throw $msd;
        }catch(mysqli_sql_exception $mse){
            throw new MysqliQueryExceptionDAO($mse->getMessage(), $mse->getCode());
        }

    }

    public function selectAllPetSitter($id){

        $mysqli=$this->connectionBdd();
       
        $stmt = $mysqli -> prepare("select * from pet_sitter  where id_utilisateur=?");
        $stmt -> bind_param("i",$id);
        $stmt->execute();
        $rs = $stmt -> get_result();
        $data = $rs -> fetch_all(MYSQLI_BOTH);

        return $data;
    }

    public function selectAllPerdu($id){

        $mysqli=$this->connectionBdd();
       
        $stmt = $mysqli -> prepare("select * from perdu_trouve  where id_utilisateur=? and type_perdu_trouve='Perdu'");
        $stmt -> bind_param("i",$id);
        $stmt->execute();
        $rs = $stmt -> get_result();
        $data = $rs -> fetch_all(MYSQLI_BOTH);

        return $data;
    }

    public function selectAllTrouve($id){

        $mysqli=$this->connectionBdd();
       
        $stmt = $mysqli -> prepare("select * from perdu_trouve  where id_utilisateur=? and type_perdu_trouve='Trouve'");
        $stmt -> bind_param("i",$id);
        $stmt->execute();
        $rs = $stmt -> get_result();
        $data = $rs -> fetch_all(MYSQLI_BOTH);

        return $data;
    }

    public function selectAllTopic($id){

        $mysqli=$this->connectionBdd();
       
        $stmt = $mysqli -> prepare("select * from topic  where id_utilisateur=? ");
        $stmt -> bind_param("i",$id);
        $stmt->execute();
        $rs = $stmt -> get_result();
        $data = $rs -> fetch_all(MYSQLI_BOTH);

        return $data;
    }
}