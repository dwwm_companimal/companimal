<?php
    include_once('BDDConnexionDAO.php');

    class ArticleDAO extends BDDConnexionDAO{

        public function AjoutArticle($Article){
            
            $mysqli = $this->connectionBdd();

            $titreArticle = $Article->getTitreArticle();
            $imageArticle = $Article->getImageArticle();  
            $contenuArticle = $Article->getContenuArticle();
            $idUser = $_SESSION['iduser'];
            $file = $_FILES['userImage']['tmp_name'];
            $photo = file_get_contents($file);
            $stmt = $mysqli->prepare("INSERT INTO article(titre_article,image_article,contenu_article,id_utilisateur) VALUES (?,?,?,?)");
            $stmt-> bind_param("sssi",$titreArticle,$photo,$contenuArticle,$idUser);
            $stmt->execute();
        }

        public function ModifArticle($Article){

            $mysqli = $this->connectionBdd();

            $idArticle = $Article->getIdArticle();
            $titreArticle = $Article->getTitreArticle();
            $contenuArticle = $Article->getContenuArticle();
           
            $stmt = $mysqli->prepare("UPDATE article SET titre_article=?,contenu_article=? where id_article = ?");
            $stmt-> bind_param("ssi",$titreArticle,$contenuArticle,$idArticle);
            $stmt->execute(); 
        }

        public function SelectArticle($debut,$NbrArticleParPage){

            $mysqli = $this->connectionBdd();

            $stmt = $mysqli->prepare('select * from article order by id_article desc LIMIT ?,?');
            $stmt-> bind_param("ii",$debut,$NbrArticleParPage);
            $stmt->execute();
            $rs = $stmt->get_result();
            $data = $rs->fetch_all(MYSQLI_ASSOC);
    
            return $data;
        }

        public function SelectAllArticle(){

            $mysqli = $this->connectionBdd();

            $stmt = $mysqli->prepare('select * from article');
            $stmt->execute();
            $rs = $stmt->get_result();
            $data = $rs->fetch_all(MYSQLI_ASSOC);
    
            return $data;
        }

        public function SelectIdArticle($get){

            $mysqli = $this->connectionBdd();

            $stmt = $mysqli->prepare('select * from article where id_article = ?');
            $stmt-> bind_param("i",$get);
            $stmt->execute();
            $rs = $stmt->get_result();
            $data = $rs->fetch_all(MYSQLI_ASSOC);
    
            return $data;
        }

        public function SupArticle($get){
            try{
            $mysqli = $this->connectionBdd();

            $stmt = $mysqli->prepare("DELETE from article WHERE id_article = ? ");
            $stmt-> bind_param("i",$get);
            $stmt->execute();
            }catch(MysqliExceptionDAO $msd){
                throw $msd;
            }catch(mysqli_sql_exception $mse){
                throw new MysqliQueryExceptionDAO($mse->getMessage(), $mse->getCode());
            }
        }
        
    }


?>