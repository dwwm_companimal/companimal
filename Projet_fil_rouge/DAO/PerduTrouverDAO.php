<?php
    include_once('BDDConnexionDAO.php');

    class PerduTrouverDAO extends BDDConnexionDAO{
        
        /* Permet l'ajout d'un fiche */

        public function AjoutPerduTrouver($PerduTrouver){

            $mysqli = $this->connectionBdd();
            
            $typeAnnonce = $PerduTrouver->GetTypePerduTrouve();
            $titre = $PerduTrouver->GetTitrePerduTrouve();
            $nomEspece = $PerduTrouver->GetNomEspece();
            $nomAnimal = $PerduTrouver->GetNomAnimalPerduTrouve();
            $lieu = $PerduTrouver->GetLieuPerduTrouve();
            $codePostal = $PerduTrouver->GetCodePostalPerduTrouve();
            $description = $PerduTrouver->GetDescriptionPerduTrouver();

            $iduser = $_SESSION['iduser']; 
            $file = $_FILES['userImage']['tmp_name'];

            $photo = file_get_contents($file);
            $stmt = $mysqli->prepare("INSERT INTO perdu_trouve(titre_perdu_trouve,nom_animal_perdu_trouve,type_perdu_trouve,lieu_perdu_trouve,date_perdu_trouve,code_postale_perdu_trouve,description_perdu_trouver,id_utilisateur,nom_espece,photo_perdu) VALUES (?,?,?,?,Localtimestamp(),?,?,?,?,?)");
            $stmt-> bind_param("ssssisiss",$titre,$nomAnimal,$typeAnnonce,$lieu,$codePostal,$description,$iduser,$nomEspece,$photo);
            $stmt->execute();
        }

        /* Permet de modifier une fiche */

        public function ModifPerduTrouver($PerduTrouver){

            $mysqli = $this->connectionBdd();
            
            $typeAnnonce = $PerduTrouver->GetTypePerduTrouve();
            $titre = $PerduTrouver->GetTitrePerduTrouve();
            $nomEspece = $PerduTrouver->GetNomEspece();
            $nomAnimal = $PerduTrouver->GetNomAnimalPerduTrouve();
            $lieu = $PerduTrouver->GetLieuPerduTrouve();
            $codePostal = $PerduTrouver->GetCodePostalPerduTrouve();
            $description = $PerduTrouver->GetDescriptionPerduTrouver();
            $idPerduTrouver = $PerduTrouver->GetIdPerduTrouve();

            $stmt = $mysqli->prepare("UPDATE perdu_trouve SET titre_perdu_trouve= ? , nom_animal_perdu_trouve = ? , type_perdu_trouve = ? , lieu_perdu_trouve = ? , code_postale_perdu_trouve= ? , description_perdu_trouver= ? , nom_espece = ? where id_perdu_trouve = ?");
            $stmt-> bind_param("ssssissi",$titre,$nomAnimal,$typeAnnonce,$lieu,$codePostal,$description,$nomEspece,$idPerduTrouver);
            $stmt->execute();
        }

        /* Permet de recupere tous les information PerduTrouver */

        public function SelectAllPerduTrouver(){

            $mysqli = $this->connectionBdd();

            $stmt = $mysqli->prepare('select * from perdu_trouve ORDER BY id_perdu_trouve DESC');
            $stmt->execute();
            $rs = $stmt->get_result();
            $data = $rs->fetch_all(MYSQLI_ASSOC);
    
            return $data;
        }

        /* Permet de recupere les Différent Type PerduTrouve */

        public function SelectTypePerduTrouver(){

            $mysqli = $this->connectionBdd();

            $stmt = $mysqli->prepare('select DISTINCT type_perdu_trouve from perdu_trouve ORDER BY id_perdu_trouve DESC');
            $stmt->execute();
            $rs = $stmt->get_result();
            $data = $rs->fetch_all(MYSQLI_ASSOC);
    
            return $data;
        }

        /* Select toutes les information Perdu */

        public function SelectAllPerdu(){

            $mysqli = $this->connectionBdd();

            $stmt = $mysqli->prepare('select * from perdu_trouve where type_perdu_trouve="Perdu" order by id_perdu_trouve desc');
            $stmt->execute();
            $rs = $stmt->get_result();
            $data = $rs->fetch_all(MYSQLI_ASSOC);
    
            return $data;
        }

        /* Select toute les information PerduTrouve */

        public function SelectAllTrouve(){

            $mysqli = $this->connectionBdd();

            $stmt = $mysqli->prepare('select * from perdu_trouve where type_perdu_trouve="Trouve" order by id_perdu_trouve desc');
            $stmt->execute();
            $rs = $stmt->get_result();
            $data = $rs->fetch_all(MYSQLI_ASSOC);
    
            return $data;
        }

        /* Select toute les information en fonction de son Id_perdu_trouve */

        public function SelectIdPerduTrouver($get){

            $mysqli = $this->connectionBdd();

            $stmt = $mysqli->prepare('select * from perdu_trouve where id_perdu_trouve = ? ORDER BY id_perdu_trouve DESC');
            $stmt-> bind_param("i",$get);
            $stmt->execute();
            $rs = $stmt->get_result();
            $data = $rs->fetch_all(MYSQLI_ASSOC);
    
            return $data;
        }

        /* Permet de supprimer une fiche */

        public function SupPerduTrouver($get){

            $mysqli = $this->connectionBdd();
    
            $stmt = $mysqli->prepare("DELETE from perdu_trouve WHERE id_perdu_trouve = ? ORDER BY id_perdu_trouve DESC");
            $stmt-> bind_param("i",$get);
            $stmt->execute();
        }

        /* Recupere le telephone et l'email de l'utilisateur  */

        public function SelectIdUtilisateurPerduTrouver($get){

            $mysqli = $this->connectionBdd();
    
            $stmt = $mysqli->prepare("select u.telephone_utilisateur,u.email from perdu_trouve as pt inner join utilisateur as u on pt.id_utilisateur = u.id_utilisateur WHERE pt.id_utilisateur = ? ");
            $stmt-> bind_param("i",$get);
            $stmt->execute();
            $rs = $stmt->get_result();
            $data2 = $rs->fetch_all(MYSQLI_ASSOC);
    
            return $data2;
        }


        /* Permet de récupérer le nombre d'element par recherche */

        public function SelectPerduTrouveRecherche($Tab){

            $mysqli = $this->connectionBdd();
    
            $rec = "";
    
            foreach($Tab as $key=>$val){   
                $rec .= $key .' LIKE "%'.$val.'%" AND ';
            }
            
            $rec = rtrim($rec, ' AND ');
         
            $rs = mysqli_query($mysqli,'select * from perdu_trouve where  '.$rec.' ORDER BY id_perdu_trouve DESC');
            $data = mysqli_fetch_all($rs,MYSQLI_ASSOC);
    
            return $data;
        }

        
    }
?>