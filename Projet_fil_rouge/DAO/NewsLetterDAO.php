<?php
    include_once('BDDConnexionDAO.php');

    class NewsLetterDAO extends BDDConnexionDAO{

        public function SelectAllMailNws(){

            $mysqli = $this->connectionBdd();

            $stmt = $mysqli->prepare('select * from newsletter');
            $stmt->execute();
            $rs = $stmt->get_result();
            $data = $rs->fetch_all(MYSQLI_ASSOC);
    
            return $data;
        }

        public function AjoutMailNws($NewsLetter){

            $mysqli = $this->connectionBdd();

            $EmailNws = $NewsLetter->GetEmail();

            $stmt = $mysqli->prepare("INSERT INTO newsletter(email_newsletter) VALUES (?)");
            $stmt-> bind_param("s",$EmailNws);
            $stmt->execute();
        }

        public function SelectWhereAllMailNws($post){

            $mysqli = $this->connectionBdd();

            $stmt = $mysqli->prepare('select * from newsletter where email_newsletter = ?');
            $stmt-> bind_param("s",$post);
            $stmt->execute();
            $rs = $stmt->get_result();
            $data = $rs->fetch_all(MYSQLI_ASSOC);
    
            return $data;
        }

        public function DeletNws($MailNws){

            $mysqli = $this->connectionBdd();

            $stmt = $mysqli->prepare("DELETE from newsletter WHERE email_newsletter = ? ");
            $stmt-> bind_param("s",$MailNws);
            $stmt->execute();
        }
    }