<?php
    include_once('BDDConnexionDAO.php');

    class AdoptionDAO extends BDDConnexionDAO{

        public function AjoutAdoption($Adoption){

            $mysqli = $this->connectionBdd();

            $nomAnimal = $Adoption->GetNom_animal();
            $especeAnimal = $Adoption->GetNom_espece();  
            $raceAnimal = $Adoption->GetNom_race();
            $sexeAnimal = $Adoption->GetSexe_animal();
            $naissanceAnimal = $Adoption->GetNaissance_animal();
            $descriptionAnimal = $Adoption->GetDescription_animal();       
            $refugeAnimal = $Adoption->GetId_refuge();

            $file = $_FILES['userImage']['tmp_name'];
            $photo = file_get_contents($file);
            
            $iduser = $_SESSION['iduser'];   

            $stmt = $mysqli->prepare("INSERT INTO animal(nom_animal,naissance_animal,sexe_animal,description_animal,id_utilisateur,nom_espece,nom_race,id_refuge,photo_animal) VALUES (?,?,?,?,?,?,?,?,?)");
            $stmt-> bind_param("sississis",$nomAnimal,$naissanceAnimal,$sexeAnimal,$descriptionAnimal,$iduser,$especeAnimal,$raceAnimal,$refugeAnimal,$photo);
            $stmt->execute();
        }

        public function ModifAdoption($Adoption){

            $mysqli = $this->connectionBdd();

            $AdoptionId = $Adoption->GetId_adoption();
            $nomAnimal = $Adoption->GetNom_animal();
            $especeAnimal = $Adoption->GetNom_espece();  
            $raceAnimal = $Adoption->GetNom_race();
            $sexeAnimal = $Adoption->GetSexe_animal();
            $naissanceAnimal = $Adoption->GetNaissance_animal();
            $descriptionAnimal = $Adoption->GetDescription_animal();       
            $refugeAnimal = $Adoption->GetId_refuge();

            $stmt = $mysqli->prepare("UPDATE animal SET nom_animal=?,naissance_animal=?,sexe_animal=?,description_animal=?,nom_espece=?,nom_race=?,id_refuge=? where id_adoption = ?");
            $stmt-> bind_param("sissssii",$nomAnimal,$naissanceAnimal,$sexeAnimal,$descriptionAnimal,$especeAnimal,$raceAnimal,$refugeAnimal,$AdoptionId);
            $stmt->execute();   
        }


        public function SelectAllAdoption(){

            $mysqli = $this->connectionBdd();

            $stmt = $mysqli->prepare('select * from animal order by id_adoption desc');
            $stmt->execute();
            $rs = $stmt->get_result();
            $data = $rs->fetch_all(MYSQLI_ASSOC);
    
            return $data;
        }

        public function SelectSexeAdoption(){

            $mysqli = $this->connectionBdd();

            $stmt = $mysqli->prepare('select DISTINCT sexe_animal from animal');
            $stmt->execute();
            $rs = $stmt->get_result();
            $data = $rs->fetch_all(MYSQLI_ASSOC);
    
            return $data;
        }

        public function SelectIdAdoption($get){

            $mysqli = $this->connectionBdd();

            $stmt = $mysqli->prepare('select * from animal where id_adoption = ?');
            $stmt-> bind_param("i",$get);
            $stmt->execute();
            $rs = $stmt->get_result();
            $data = $rs->fetch_all(MYSQLI_ASSOC);
    
            return $data;
        }

        public function SupAdoption($get){

            $mysqli = $this->connectionBdd();

            $stmt = $mysqli->prepare("DELETE from animal WHERE id_adoption = ? ");
            $stmt-> bind_param("i",$get);
            $stmt->execute();
        }       

                /* Permet d'avoir le nombre d'element par recherche */

        public function SelectAdoptionRecherche($Tab){

            $mysqli = $this->connectionBdd();

            $rec = "";

            foreach($Tab as $key=>$val){   
                    $rec .= $key .' LIKE "%'.$val.'%" AND ';
            }
            
            $rec = rtrim($rec, ' AND ');

            $rs = mysqli_query($mysqli,'select * from animal where  '.$rec.' ORDER BY id_adoption DESC');
            $data = mysqli_fetch_all($rs,MYSQLI_ASSOC);

            return $data;
        }


    }
?>