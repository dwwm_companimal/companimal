<?php
include_once('..\DAO\BDDConnexionDAO.php');
include_once('..\MODEL\Commentaire.php');

Class CommentaireDAO extends BDDConnexionDAO{

    Public function ajoutCommentaire($post){
            
        $mysqli = $this->connectionBdd();

        $idtopic =$_GET['id_topic'];
        $iduser = $_SESSION['iduser'];

        $contenu = $post->getContenuCommentaire();

        $stmt = $mysqli->prepare('INSERT INTO commentaire( contenu_commentaire,date_commentaire,id_topic,id_utilisateur) VALUES ( ?,Localtimestamp(),?,?)');
        $stmt -> bind_param("sii", $contenu,$idtopic,$iduser);
        $stmt -> execute(); 
    }

    public function SupCommentaire($get){

        $mysqli = $this->connectionBdd();

        $stmt = $mysqli->prepare("DELETE FROM commentaire WHERE id_commentaire=?");
        $stmt -> bind_param("i",$get);
        $stmt -> execute();
    }
       
}

?>