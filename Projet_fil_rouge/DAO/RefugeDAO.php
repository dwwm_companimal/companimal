<?php

    include_once('BDDConnexionDAO.php');

    class RefugeDAO extends BDDConnexionDAO{


        public function SelectRefugeIdDAO($IdRefuge){

            $mysqli = $this->connectionBdd();

            $stmt = $mysqli->prepare("SELECT * from refuge where id_refuge = ?");
            $stmt-> bind_param("i",$IdRefuge);
            $stmt->execute();
            $rs = $stmt->get_result();
            $data = $rs->fetch_all(MYSQLI_ASSOC);
    
            return $data;
        }

        public function SelectAllRefugeDAO(){

            $mysqli = $this->connectionBdd();

            $stmt = $mysqli->prepare("SELECT * from refuge order by nom_refuge asc");
            $stmt->execute();
            $rs = $stmt->get_result();
            $data = $rs->fetch_all(MYSQLI_ASSOC);
    
            return $data;
        }

        public function AjoutRefuge($Refuge){

            $mysqli = $this->connectionBdd();

            $nomRefuge = $Refuge->GetNomRefuge();
            $adresseRefuge = $Refuge->GetAdresseRefuge();
            $codePostalRefuge = $Refuge->GetCodePostalRefuge();
            $villeRefuge = $Refuge->GetVilleRefuge();
            $descriptionRefuge = $Refuge->GetDescriptionRefuge();
            $emailRefuge = $Refuge->GetEmailRefuge();
            $telRefuge = $Refuge->GetTelephoneRefuge();
            $iduser = $_SESSION['iduser'];
            $file = $_FILES['userImage']['tmp_name'];
            $photo = file_get_contents($file);   

            $stmt = $mysqli->prepare("INSERT INTO refuge(nom_refuge,adresse_refuge,code_postal_refuge,ville_refuge,description_refuge,email_refuge,telephone_refuge,id_utilisateur,photo_refuge) VALUES (?,?,?,?,?,?,?,?,?)");
            $stmt-> bind_param("ssisssiis",$nomRefuge,$adresseRefuge,$codePostalRefuge,$villeRefuge,$descriptionRefuge,$emailRefuge,$telRefuge,$iduser,$photo);
            $stmt->execute();
        }

        public function ModifRefuge($Refuge){

            $mysqli = $this->connectionBdd();

            $idRefuge = $Refuge->GetIdRefuge();
            $nomRefuge = $Refuge->GetNomRefuge();
            $adresseRefuge = $Refuge->GetAdresseRefuge();
            $codePostalRefuge = $Refuge->GetCodePostalRefuge();
            $villeRefuge = $Refuge->GetVilleRefuge();
            $descriptionRefuge = $Refuge->GetDescriptionRefuge();
            $emailRefuge = $Refuge->GetEmailRefuge();
            $telRefuge = $Refuge->GetTelephoneRefuge();

            $stmt = $mysqli->prepare("UPDATE refuge SET nom_refuge=?,adresse_refuge=?,code_postal_refuge=?,ville_refuge=?,description_refuge=?,email_refuge=?,telephone_refuge=? where id_refuge = ?");
            $stmt-> bind_param("ssisssii",$nomRefuge,$adresseRefuge,$codePostalRefuge,$villeRefuge,$descriptionRefuge,$emailRefuge,$telRefuge,$idRefuge);
            $stmt->execute();
        }

        public function SupRefuge($get){

            $mysqli = $this->connectionBdd();

            $stmt = $mysqli->prepare("DELETE from refuge WHERE id_refuge = ? ");
            $stmt-> bind_param("i",$get);
            $stmt->execute();
        }

        public function SelectRefuge($debut,$NbrRefugeParPage){

            $mysqli = $this->connectionBdd();

            $stmt = $mysqli->prepare('select * from refuge order by nom_refuge LIMIT ?,?');
            $stmt-> bind_param("ii",$debut,$NbrRefugeParPage);
            $stmt->execute();
            $rs = $stmt->get_result();
            $data = $rs->fetch_all(MYSQLI_ASSOC);
    
            return $data;
        }
    }


?>