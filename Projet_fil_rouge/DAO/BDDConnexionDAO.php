<?php

include_once('MysqliExceptionDAO.php');
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

class BDDConnexionDAO{
    
    public function connectionBdd(){
        try{
        $mysqli= new mysqli('localhost','root','','companimal');
        return $mysqli;
        }catch(mysqli_sql_exception $mse){
        throw new MysqliExceptionDAO($mse->getMessage(), $mse->getCode());  
        }
    }

    public function DeconnexionBDD(){

        $mysqli = $this->connectionBdd();
        mysqli_close($mysqli);
    }

}
?>