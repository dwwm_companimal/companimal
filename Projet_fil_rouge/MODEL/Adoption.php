<?php

class Adoption{

    private $id_adoption;
    private $nom_animal;
    private $naissance_animal;
    private $sexe_animal;
    private $photo_animal;
    private $description_animal;
    private $utilisateur;
    private $nom_espece;
    private $nom_race;
    private $id_refuge;


    /**
     * Get the value of id_adoption
     */ 
    public function getId_adoption()
    {
        return $this->id_adoption;
    }

    /**
     * Set the value of id_adoption
     *
     * @return  self
     */ 
    public function setId_adoption($id_adoption)
    {
        $this->id_adoption = $id_adoption;

        return $this;
    }

    /**
     * Get the value of nom_animal
     */ 
    public function getNom_animal()
    {
        return $this->nom_animal;
    }

    /**
     * Set the value of nom_animal
     *
     * @return  self
     */ 
    public function setNom_animal($nom_animal)
    {
        $this->nom_animal = $nom_animal;

        return $this;
    }

    /**
     * Get the value of naissance_animal
     */ 
    public function getNaissance_animal()
    {
        return $this->naissance_animal;
    }

    /**
     * Set the value of naissance_animal
     *
     * @return  self
     */ 
    public function setNaissance_animal($naissance_animal)
    {
        $this->naissance_animal = $naissance_animal;

        return $this;
    }

    /**
     * Get the value of sexe_animal
     */ 
    public function getSexe_animal()
    {
        return $this->sexe_animal;
    }

    /**
     * Set the value of sexe_animal
     *
     * @return  self
     */ 
    public function setSexe_animal($sexe_animal)
    {
        $this->sexe_animal = $sexe_animal;

        return $this;
    }

    /**
     * Get the value of photo_animal
     */ 
    public function getPhoto_animal()
    {
        return $this->photo_animal;
    }

    /**
     * Set the value of photo_animal
     *
     * @return  self
     */ 
    public function setPhoto_animal($photo_animal)
    {
        $this->photo_animal = $photo_animal;

        return $this;
    }

    /**
     * Get the value of description_animal
     */ 
    public function getDescription_animal()
    {
        return $this->description_animal;
    }

    /**
     * Set the value of description_animal
     *
     * @return  self
     */ 
    public function setDescription_animal($description_animal)
    {
        $this->description_animal = $description_animal;

        return $this;
    }

    /**
     * Get the value of utilisateur
     */ 
    public function getUtilisateur()
    {
        return $this->utilisateur;
    }

    /**
     * Set the value of utilisateur
     *
     * @return  self
     */ 
    public function setUtilisateur($utilisateur)
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    /**
     * Get the value of nom_espece
     */ 
    public function getNom_espece()
    {
        return $this->nom_espece;
    }

    /**
     * Set the value of nom_espece
     *
     * @return  self
     */ 
    public function setNom_espece($nom_espece)
    {
        $this->nom_espece = $nom_espece;

        return $this;
    }


    /**
     * Get the value of nom_race
     */ 
    public function getNom_race()
    {
        return $this->nom_race;
    }

    /**
     * Set the value of nom_race
     *
     * @return  self
     */ 
    public function setNom_race($nom_race)
    {
        $this->nom_race = $nom_race;

        return $this;
    }

    /**
     * Get the value of id_refuge
     */ 
    public function getId_refuge()
    {
        return $this->id_refuge;
    }

    /**
     * Set the value of id_refuge
     *
     * @return  self
     */ 
    public function setId_refuge($id_refuge)
    {
        $this->id_refuge = $id_refuge;

        return $this;
    }
}