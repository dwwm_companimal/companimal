<?php

class PerduTrouve{

    private $idPerduTrouve;
    private $titrePerduTrouve;
    private $nomAnimalPerduTrouve;
    private $typePerduTrouve;
    private $lieuPerduTrouve;
    private $datePerduTrouve;
    private $codePostalPerduTrouve;
    private $photoPerduTrouver;
    private $descriptionPerduTrouver;
    private $idUtilisateur;
    private $nomEspece;
    

    /**
     * Get the value of idPerduTrouve
     */ 
    public function getIdPerduTrouve()
    {
        return $this->idPerduTrouve;
    }

    /**
     * Set the value of idPerduTrouve
     *
     * @return  self
     */ 
    public function setIdPerduTrouve($idPerduTrouve)
    {
        $this->idPerduTrouve = $idPerduTrouve;

        return $this;
    }

    /**
     * Get the value of titrePerduTrouve
     */ 
    public function getTitrePerduTrouve()
    {
        return $this->titrePerduTrouve;
    }

    /**
     * Set the value of titrePerduTrouve
     *
     * @return  self
     */ 
    public function setTitrePerduTrouve($titrePerduTrouve)
    {
        $this->titrePerduTrouve = $titrePerduTrouve;

        return $this;
    }

    /**
     * Get the value of typePerduTrouve
     */ 
    public function getTypePerduTrouve()
    {
        return $this->typePerduTrouve;
    }

    /**
     * Set the value of typePerduTrouve
     *
     * @return  self
     */ 
    public function setTypePerduTrouve($typePerduTrouve)
    {
        $this->typePerduTrouve = $typePerduTrouve;

        return $this;
    }

    /**
     * Get the value of lieuPerduTrouve
     */ 
    public function getLieuPerduTrouve()
    {
        return $this->lieuPerduTrouve;
    }

    /**
     * Set the value of lieuPerduTrouve
     *
     * @return  self
     */ 
    public function setLieuPerduTrouve($lieuPerduTrouve)
    {
        $this->lieuPerduTrouve = $lieuPerduTrouve;

        return $this;
    }

    /**
     * Get the value of datePerduTrouve
     */ 
    public function getDatePerduTrouve()
    {
        return $this->datePerduTrouve;
    }

    /**
     * Set the value of datePerduTrouve
     *
     * @return  self
     */ 
    public function setDatePerduTrouve($datePerduTrouve)
    {
        $this->datePerduTrouve = $datePerduTrouve;

        return $this;
    }

    /**
     * Get the value of codePostalPerduTrouve
     */ 
    public function getCodePostalPerduTrouve()
    {
        return $this->codePostalPerduTrouve;
    }

    /**
     * Set the value of codePostalPerduTrouve
     *
     * @return  self
     */ 
    public function setCodePostalPerduTrouve($codePostalPerduTrouve)
    {
        $this->codePostalPerduTrouve = $codePostalPerduTrouve;

        return $this;
    }

    /**
     * Get the value of idUtilisateur
     */ 
    public function getIdUtilisateur()
    {
        return $this->idUtilisateur;
    }

    /**
     * Set the value of idUtilisateur
     *
     * @return  self
     */ 
    public function setIdUtilisateur($idUtilisateur)
    {
        $this->idUtilisateur = $idUtilisateur;

        return $this;
    }


    /**
     * Get the value of photoPerduTrouver
     */ 
    public function getPhotoPerduTrouver()
    {
        return $this->photoPerduTrouver;
    }

    /**
     * Set the value of photoPerduTrouver
     *
     * @return  self
     */ 
    public function setPhotoPerduTrouver($photoPerduTrouver)
    {
        $this->photoPerduTrouver = $photoPerduTrouver;

        return $this;
    }

    /**
     * Get the value of descriptionPerduTrouver
     */ 
    public function getDescriptionPerduTrouver()
    {
        return $this->descriptionPerduTrouver;
    }

    /**
     * Set the value of descriptionPerduTrouver
     *
     * @return  self
     */ 
    public function setDescriptionPerduTrouver($descriptionPerduTrouver)
    {
        $this->descriptionPerduTrouver = $descriptionPerduTrouver;

        return $this;
    }

    /**
     * Get the value of nomAnimalPerduTrouve
     */ 
    public function getNomAnimalPerduTrouve()
    {
        return $this->nomAnimalPerduTrouve;
    }

    /**
     * Set the value of nomAnimalPerduTrouve
     *
     * @return  self
     */ 
    public function setNomAnimalPerduTrouve($nomAnimalPerduTrouve)
    {
        $this->nomAnimalPerduTrouve = $nomAnimalPerduTrouve;

        return $this;
    }

    /**
     * Get the value of nomEspece
     */ 
    public function getNomEspece()
    {
        return $this->nomEspece;
    }

    /**
     * Set the value of nomEspece
     *
     * @return  self
     */ 
    public function setNomEspece($nomEspece)
    {
        $this->nomEspece = $nomEspece;

        return $this;
    }
}


?>